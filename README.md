Go Gigs - Services Store API
----------------------------
@author Unai Garcia

Backend for managing services/gigs, featuring:
- PostgreSQL client connection and repositories interfaces
- HTTP controller interfaces
- API documentation thru apidoc.js
- API tests thru postman/newman collections configuration
- Role based authentication using encryption
