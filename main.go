/*
Go Gigs - Main Package.

Initializes database connection and http server.
 */
package main

import (
    "gitlab.com/fc_freelance/go_gigs/db"
    "gitlab.com/fc_freelance/go_gigs/http"
)

// Main execution thread
func main() {

    // Init database connection
    db.Init()

    // Init HTTP server
    http.Init()
}

/**
 * @apiDefine UserAuth Authenticated users access only
 * API endpoint only accessible for authenticated users, otherwise a 401 will be returned.
 */

/**
 * @apiDefine ExpertAuth Authenticated expert users access only
 * API endpoint only accessible for authenticated users with expert role, otherwise a 401 will be returned.
 */

/**
 * @apiDefine AdminAuth Authenticated admin users access only
 * API endpoint only accessible for authenticated users with admin privileges, otherwise a 401 will be returned.
 */

/**
 * @apiDefine BadRequestError
 *
 * @apiError (Errors 4xx) BadRequest 400 - The request is missing some needed data or data not properly formatted.
 *
 * @apiErrorExample {json} BadRequest response
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Bad Request"
 *     }
 */

/**
 * @apiDefine UnauthorizedError
 *
 * @apiError (Errors 4xx) Unauthorized 401 - Not authorized to perform the request.
 *
 * @apiErrorExample {json} Unauthorized response
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "error": "Unauthorized"
 *     }
 */

/**
 * @apiDefine NotFoundError
 *
 * @apiError (Errors 4xx) NotFound 404 - The resource was not found.
 *
 * @apiErrorExample {json} NotFound response
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Not Found"
 *     }
 */

/**
 * @apiDefine MethodNotAllowedError
 *
 * @apiError (Errors 4xx) MethodNotAllowed 405 - The request method is not allowed.
 *
 * @apiErrorExample {json} MethodNotAllowed response
 *     HTTP/1.1 405 Method Not Allowed
 *     {
 *       "error": "Method Not Allowed"
 *     }
 */

/**
 * @apiDefine InternalServerError
 *
 * @apiError (Errors 5xx) InternalServerError 500 - A server error occurred while performing the request.
 *
 * @apiErrorExample {json} InternalServerError response
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "error": "Internal Server Error"
 *     }
 */

/**
 * @apiDefine NotImplementedError
 *
 * @apiError (Errors 5xx) NotImplemented 501 - The request endpoint is not implemented yet.
 *
 * @apiErrorExample {json} NotImplemented response
 *     HTTP/1.1 501 Not Implemented
 *     {
 *       "error": "Not Implemented"
 *     }
 */

/**
 * @apiDefine JsonHeader
 *
 * @apiHeader (Headers) {String} Content-Type The request body content type (mandatory application/json).
 *
 * @apiHeaderExample {json} JSON
 *     "Content-Type": "application/json"
 */

/**
 * @apiDefine MultipartHeader
 *
 * @apiHeader (Headers) {String} Content-Type The request body content type (mandatory multipart/form-data).
 *
 * @apiHeaderExample {json} Multipart
 *     "Content-Type": "multipart/form-data"
 */

/**
 * @apiDefine ApiTokenHeader
 *
 * @apiHeader (Headers) {String} go_gigs_tkn The API authentication token, as provided in login response. If not provided, aPI will attempt to authenticate through cookie.
 *
 * @apiHeaderExample {json} API Token
 *     "go_gigs_tkn": "asdjs77aEWE1243fdsfDSF"
 */
