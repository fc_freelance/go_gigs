#!/bin/bash

# Check PostgreSQL existence
command -v psql >/dev/null 2>&1 || { echo >&2 "PostgreSQL not installed. Install it and re-run the script."; exit 1; }

# Get script folder, which should be relative to migrations one
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Get environment to migrate from either environment variables or input, crashing if not specified
if [ -z "$DB_ENV" ]; then
    DB_ENV=$1
    if [ -z "$DB_ENV" ]; then
        echo "No environment defined."
        echo "Please provide it as parameter or define it in DB_ENV environment variable."
        echo "Falling back to environment variables (if they exist)"
    fi
fi

# Try to find environment config for database migration, if file defined
if [ ! -z "$DB_ENV" ]; then
    DB_CFG_FILE=${SCRIPT_DIR}/${DB_ENV}.env
    if [ -f $DB_CFG_FILE ]; then
        . $DB_CFG_FILE
        if [[ "$?" -ne "0" ]]; then
            echo "Error parsing environment configuration file ${DB_CFG_FILE}"
            exit 1
        fi
    fi
fi

# If at this point there's no db name defined, just quit
if [ -z "$DB_NAME" ]; then
    echo "Could not get database name from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set DB_NAME environment variable"
    exit 1
fi

# If at this point there's no db user defined, just quit
if [ -z "$DB_USER" ]; then
    echo "Could not get database user from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set DB_USER environment variable"
    exit 1
fi

# If at this point there's no db user password defined, just quit
if [ -z "$DB_USER_PASSWORD" ]; then
    echo "Could not get database user password from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set DB_USER_PASSWORD environment variable"
    exit 1
fi

# Set database host to default 'localhost' if not set yet
if [ -z "$DB_HOST" ]; then
    echo "Could not get database host from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set DB_HOST environment variable"
    echo "Falling back to localhost"
    DB_HOST=127.0.0.1
fi

# Set database port to default '5432' if not set yet
if [ -z "$DB_PORT" ]; then
    echo "Could not get database port from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set DB_PORT environment variable"
    echo "Falling back to 5432"
    DB_PORT=5432
fi

# Set psql user to default 'postgres' if not set yet
if [ -z "$PSQL_USER" ]; then
    echo "Could not get psql user from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set PSQL_USER environment variable"
    echo "Falling back to 'postgres' user"
    PSQL_USER=postgres
fi

# Set psql user password to default 'postgres' if not set yet
if [ -z "$PSQL_USER_PASSWORD" ]; then
    echo "Could not get psql user password from environment."
    echo "Please create a {DB_ENV}.env file from default sample or set PSQL_USER_PASSWORD environment variable"
    echo "Falling back to input prompt"
fi

# Info
echo "Migrating database ..."

# Set postgresql password into env var
export PGPASSWORD=$PSQL_USER_PASSWORD

# Check user/role existence, attempt to create it if not
USER_EXISTS=$(psql -U ${PSQL_USER} -h ${DB_HOST} -p ${DB_PORT} -d postgres -qtc "SELECT 1 FROM pg_user WHERE usename = '${DB_USER}'" 2>&1)
if [[ "$?" -ne "0" ]]; then
    echo "Error checking User/Role: ${USER_EXISTS}"
    exit 1
fi
if [[ "${USER_EXISTS}" -ne "1" ]]; then
    echo "User/Role ${DB_USER} does not exist. Creating it."
    USER_CREATED=$(psql -U ${PSQL_USER} -h ${DB_HOST} -p ${DB_PORT} -d postgres -qtc "CREATE ROLE ${DB_USER} WITH LOGIN PASSWORD '${DB_USER_PASSWORD}'" 2>&1)
    if [[ "$?" -ne "0" ]]; then
        echo "Error creating User/Role ${DB_USER}: ${USER_CREATED}"
        exit 1
    fi
else
    echo "User/Role ${DB_USER} already exists."
fi

# Check database existence, attempt to create it if not
DB_EXISTS=$(psql -U ${PSQL_USER} -h ${DB_HOST} -p ${DB_PORT} -d postgres -qtc "SELECT 1 FROM pg_database WHERE datname = '${DB_NAME}'" 2>&1)
if [[ "$?" -ne "0" ]]; then
    echo "Error checking Database: ${DB_EXISTS}"
    exit 1
fi
if [[ "${DB_EXISTS}" -ne "1" ]]; then
    echo "Database ${DB_NAME} does not exist. Creating it."
    DB_CREATED=$(psql -U ${PSQL_USER} -h ${DB_HOST} -p ${DB_PORT} -d postgres -qtc "CREATE DATABASE ${DB_NAME} OWNER ${DB_USER}" 2>&1)
    if [[ "$?" -ne "0" ]]; then
        echo "Error creating database ${DB_NAME}: ${DB_CREATED}"
        exit 1
    fi
else
    echo "Database ${DB_NAME} already exists."
fi

# Set postgresql password into env var, this time for database user
export PGPASSWORD=$DB_USER_PASSWORD

# Create migrations table if it does not exist
MIGRATIONS_TABLE_CREATED=$(psql -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -v ON_ERROR_STOP=1 -qtf "${SCRIPT_DIR}/migrations.schema.sql" 2>&1)
if [[ "$?" -ne "0" ]]; then
    echo "Error creating migrations table for database ${DB_NAME}: ${MIGRATIONS_TABLE_CREATED}"
    exit 1
fi

# Check existing migrations
MIGRATED=($(psql -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -qtc "SELECT code FROM migrations ORDER BY code ASC" 2>&1))
if [[ "$?" -ne "0" ]]; then
    echo "Error getting existing migrations from database ${DB_NAME}: ${MIGRATED}"
    exit 1
fi

# Iterate over files and apply new migrations
SQL_DIR=${SCRIPT_DIR}/migrations
for filename in ${SQL_DIR}/*.${DB_NAME}.sql; do
    if [[ ! -e "$filename" ]]; then continue; fi
    MIGRATION_CODE=$(basename "$filename" .${DB_NAME}.sql)
    if [[ " ${MIGRATED[@]} " =~ " ${MIGRATION_CODE} " ]]; then
        echo "Already applied: ${MIGRATION_CODE}"
    else
        MIGRATING=$(psql -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -v ON_ERROR_STOP=1 -qtf "${filename}" 2>&1)
        if [[ "$?" -ne "0" ]]; then
            echo "Error creating migration from file ${filename}: ${MIGRATING}"
            exit 1
        fi
        MIGRATING=$(psql -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -qtc "INSERT INTO migrations (code) VALUES ('${MIGRATION_CODE}')" 2>&1)
        if [[ "$?" -ne "0" ]]; then
            echo "Error updating migrations table for file ${filename}: ${MIGRATING}"
            exit 1
        fi
        echo "Migration applied: ${MIGRATION_CODE}"
    fi
done

# FIXME: Remove this once we've a proper data fill for DB common stuff
POLYFILL_FILE=${SQL_DIR}/go_gigs.polyfills.sql
POLYFILL=$(psql -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -v ON_ERROR_STOP=1 -qtf "${POLYFILL_FILE}" 2>&1)
if [[ "$?" -ne "0" ]]; then
    echo "Error creating polyfill from file ${POLYFILL_FILE}"
    exit 1
fi
echo "Polyfills applied. Please remove this from migrations once proper common data is filled up."

# Clear env password
export PGPASSWORD=""

# Completeness
echo "Migration completed"
exit 0
