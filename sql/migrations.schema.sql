-- Create migrations sequence
DO $$ BEGIN
    IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'migrations_id_seq') THEN
        CREATE SEQUENCE migrations_id_seq;
    END IF;
END $$;

-- Create migrations table
CREATE TABLE IF NOT EXISTS migrations (
  id integer PRIMARY KEY DEFAULT nextval('migrations_id_seq'),
  code varchar(64) NOT NULL UNIQUE CHECK (code <> ''),
  created_at timestamp WITHOUT TIME ZONE DEFAULT NOW()
);
