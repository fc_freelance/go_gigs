-----------------------------------------------------
----------------------- COMMON ----------------------
-----------------------------------------------------

-- Create categories sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'categories_id_seq') THEN
    CREATE SEQUENCE categories_id_seq;
  END IF;
END $$;

-- Create categories table
CREATE TABLE IF NOT EXISTS categories (
  id integer PRIMARY KEY DEFAULT nextval('categories_id_seq'),
  name varchar(64) NOT NULL UNIQUE CHECK (name <> ''),
  description text DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  parent_id integer DEFAULT NULL REFERENCES categories (id) ON DELETE CASCADE
);

-- Create locations sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'locations_id_seq') THEN
    CREATE SEQUENCE locations_id_seq;
  END IF;
END $$;

-- Create locations table
CREATE TABLE IF NOT EXISTS locations (
  id integer PRIMARY KEY DEFAULT nextval('locations_id_seq'),
  name varchar(255) NOT NULL CHECK (name <> ''),
  code varchar(50) NOT NULL UNIQUE CHECK (code <> ''),
  type char(2) DEFAULT NULL,
  lat float DEFAULT NULL,
  lon float DEFAULT NULL,
  db_id varchar(50) DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  parent_id integer DEFAULT NULL REFERENCES locations (id) ON DELETE CASCADE
);

-- Create addresses sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'addresses_id_seq') THEN
    CREATE SEQUENCE addresses_id_seq;
  END IF;
END $$;

-- Create addresses table
CREATE TABLE IF NOT EXISTS addresses (
  id integer PRIMARY KEY DEFAULT nextval('addresses_id_seq'),
  number varchar(16) NOT NULL CHECK (number <> ''),
  street varchar(128) NOT NULL CHECK (street <> ''),
  postal_code varchar(20) NOT NULL CHECK (postal_code <> ''),
  extra_info varchar(255) DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  location_id integer NOT NULL REFERENCES locations (id) ON DELETE CASCADE
);

-- Create languages sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'languages_id_seq') THEN
    CREATE SEQUENCE languages_id_seq;
  END IF;
END $$;

-- Create languages table
CREATE TABLE IF NOT EXISTS languages (
  id integer PRIMARY KEY DEFAULT nextval('languages_id_seq'),
  code varchar(16) NOT NULL CHECK (code <> ''),
  name varchar(64) NOT NULL CHECK (name <> ''),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);

-- Create currencies sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'currencies_id_seq') THEN
    CREATE SEQUENCE currencies_id_seq;
  END IF;
END $$;

-- Create currencies table
CREATE TABLE IF NOT EXISTS currencies (
  id integer PRIMARY KEY DEFAULT nextval('currencies_id_seq'),
  code varchar(16) NOT NULL CHECK (code <> ''),
  name varchar(64) NOT NULL CHECK (name <> ''),
  symbol char(2) NOT NULL CHECK (symbol <> ''),
  conversion float NOT NULL CHECK (conversion > 0),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);

-- Create resources sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'resources_id_seq') THEN
    CREATE SEQUENCE resources_id_seq;
  END IF;
END $$;

-- Create resources type
DROP TYPE IF EXISTS resource_type;
CREATE TYPE resource_type AS ENUM ('image', 'video', 'audio', 'pdf');

-- Create resources table
CREATE TABLE IF NOT EXISTS resources (
  id integer PRIMARY KEY DEFAULT nextval('resources_id_seq'),
  type resource_type NOT NULL,
  name varchar(255) DEFAULT NULL,
  url varchar(255) NOT NULL CHECK (url <> ''),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);
