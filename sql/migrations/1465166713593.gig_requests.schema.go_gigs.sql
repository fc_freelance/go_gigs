-----------------------------------------------------
----------------------- GIGS ------------------------
-----------------------------------------------------

-- Create gigs sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_requests_id_seq') THEN
    CREATE SEQUENCE gig_requests_id_seq;
  END IF;
END $$;

-- Create gigs table
CREATE TABLE IF NOT EXISTS gig_requests (
  id integer PRIMARY KEY DEFAULT nextval('gig_requests_id_seq'),
  name varchar(64) NOT NULL CHECK (name <> ''),
  description text NOT NULL CHECK (description <> ''),
  date date NOT NULL check (date > NOW()),
  time time WITH TIME ZONE DEFAULT NULL,
  budget float DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  currency_id integer DEFAULT NULL REFERENCES currencies (id) ON DELETE CASCADE,
  category_id integer NOT NULL REFERENCES categories (id) ON DELETE CASCADE,
  location_id integer NOT NULL REFERENCES locations (id) ON DELETE CASCADE
);

-- Create gig_request_resources sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_request_resources_id_seq') THEN
    CREATE SEQUENCE gig_request_resources_id_seq;
  END IF;
END $$;

-- Create gig_resources table
CREATE TABLE IF NOT EXISTS gig_request_resources (
  id integer PRIMARY KEY DEFAULT nextval('gig_request_resources_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_request_id integer NOT NULL REFERENCES gig_requests (id) ON DELETE CASCADE,
  resource_id integer NOT NULL REFERENCES resources (id) ON DELETE CASCADE
);
