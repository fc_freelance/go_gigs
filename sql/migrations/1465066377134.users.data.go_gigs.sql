-----------------------------------------------------
-------------------- AUTH / USERS -------------------
-----------------------------------------------------

-- Fill basic roles
INSERT INTO roles (id, code, name, description) VALUES
  (1, 'user', 'User', 'Basic User role'),
  (2, 'expert', 'Expert', 'Expert role'),
  (3, 'admin', 'Admin', 'Administrator role') ON CONFLICT DO NOTHING;

-- Reset sequence
SELECT setval('roles_id_seq', COALESCE((SELECT MAX(id)+1 FROM roles), 1), false);

-- Fill basic permissions
INSERT INTO permissions (id, code, name, description) VALUES
  (1, 'list_users', 'List/Get Users', 'Allows to retrieve any user data'),
  (2, 'manage_gigs', 'Gigs Management', 'Allows to fully create/edit/delete Gigs, mainly for Experts use'),
  (3, 'manage_system', 'System Management', 'Administrator permissions for full system management') ON CONFLICT DO NOTHING;

-- Reset sequence
SELECT setval('permissions_id_seq', COALESCE((SELECT MAX(id)+1 FROM permissions), 1), false);

-- Fill basic role permissions
INSERT INTO role_permissions (role_id, permission_id) VALUES
  (1, 1),
  (2, 1),
  (2, 2),
  (3, 1),
  (3, 2),
  (3, 3) ON CONFLICT DO NOTHING;
