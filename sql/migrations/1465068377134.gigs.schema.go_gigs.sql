-----------------------------------------------------
----------------------- GIGS ------------------------
-----------------------------------------------------

-- Create gigs sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gigs_id_seq') THEN
    CREATE SEQUENCE gigs_id_seq;
  END IF;
END $$;

-- Create gigs table
CREATE TABLE IF NOT EXISTS gigs (
  id integer PRIMARY KEY DEFAULT nextval('gigs_id_seq'),
  name varchar(64) NOT NULL CHECK (name <> ''),
  description text NOT NULL CHECK (description <> ''),
  price float NOT NULL CHECK (price > 0),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  currency_id integer NOT NULL REFERENCES currencies (id) ON DELETE CASCADE,
  category_id integer NOT NULL REFERENCES categories (id) ON DELETE CASCADE
);

-- Create gig_items sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_items_id_seq') THEN
    CREATE SEQUENCE gig_items_id_seq;
  END IF;
END $$;

-- Create gig_items table
CREATE TABLE IF NOT EXISTS gig_items (
  id integer PRIMARY KEY DEFAULT nextval('gig_items_id_seq'),
  name varchar(64) NOT NULL CHECK (name <> ''),
  description text DEFAULT NULL,
  price float DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_id integer NOT NULL REFERENCES gigs (id) ON DELETE CASCADE
);

-- Create gig_resources sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_resources_id_seq') THEN
    CREATE SEQUENCE gig_resources_id_seq;
  END IF;
END $$;

-- Create gig_resources table
CREATE TABLE IF NOT EXISTS gig_resources (
  id integer PRIMARY KEY DEFAULT nextval('gig_resources_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_id integer NOT NULL REFERENCES gigs (id) ON DELETE CASCADE,
  resource_id integer NOT NULL REFERENCES resources (id) ON DELETE CASCADE
);

-- Create gig_schedules sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedules_id_seq') THEN
    CREATE SEQUENCE gig_schedules_id_seq;
  END IF;
END $$;

-- Create gig_schedules table
CREATE TABLE IF NOT EXISTS gig_schedules (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedules_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_id integer NOT NULL REFERENCES gigs (id) ON DELETE CASCADE
);

-- Create gig_schedule_locations sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedule_locations_id_seq') THEN
    CREATE SEQUENCE gig_schedule_locations_id_seq;
  END IF;
END $$;

-- Create gig_schedule_locations table
CREATE TABLE IF NOT EXISTS gig_schedule_locations (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedule_locations_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_schedule_id integer NOT NULL REFERENCES gig_schedules (id) ON DELETE CASCADE,
  location_id integer NOT NULL REFERENCES locations (id) ON DELETE CASCADE
);

-- Create gig_schedule_availabilities sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedule_availabilities_id_seq') THEN
    CREATE SEQUENCE gig_schedule_availabilities_id_seq;
  END IF;
END $$;

-- Create gig_schedule_availabilities table
CREATE TABLE IF NOT EXISTS gig_schedule_availabilities (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedule_availabilities_id_seq'),
  date_from date DEFAULT NULL,
  date_to date DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_schedule_id integer NOT NULL REFERENCES gig_schedules (id) ON DELETE CASCADE
);

-- Create gig_schedule_times sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedule_times_id_seq') THEN
    CREATE SEQUENCE gig_schedule_times_id_seq;
  END IF;
END $$;

-- Create gig_schedule_times table
CREATE TABLE IF NOT EXISTS gig_schedule_times (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedule_times_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_schedule_id integer NOT NULL REFERENCES gig_schedules (id) ON DELETE CASCADE
);

-- Create gig_schedule_time_days sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedule_time_days_id_seq') THEN
    CREATE SEQUENCE gig_schedule_time_days_id_seq;
  END IF;
END $$;

-- Create weekday type
DROP TYPE IF EXISTS weekday;
CREATE TYPE weekday AS ENUM ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

-- Create gig_schedule_time_days table
CREATE TABLE IF NOT EXISTS gig_schedule_time_days (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedule_time_days_id_seq'),
  weekday weekday NOT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_schedule_time_id integer NOT NULL REFERENCES gig_schedule_times (id) ON DELETE CASCADE
);

-- Create gig_schedule_time_ranges sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'gig_schedule_time_ranges_id_seq') THEN
    CREATE SEQUENCE gig_schedule_time_ranges_id_seq;
  END IF;
END $$;

-- Create gig_schedule_time_ranges table
CREATE TABLE IF NOT EXISTS gig_schedule_time_ranges (
  id integer PRIMARY KEY DEFAULT nextval('gig_schedule_time_ranges_id_seq'),
  time_from time WITH TIME ZONE DEFAULT NULL,
  time_to time WITH TIME ZONE DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  gig_schedule_time_id integer NOT NULL REFERENCES gig_schedule_times (id) ON DELETE CASCADE
);
