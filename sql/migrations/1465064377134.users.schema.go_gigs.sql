-----------------------------------------------------
-------------------- AUTH / USERS -------------------
-----------------------------------------------------

-- Create users sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'users_id_seq') THEN
    CREATE SEQUENCE users_id_seq;
  END IF;
END $$;

-- Create users table
CREATE TABLE IF NOT EXISTS users (
  id integer PRIMARY KEY DEFAULT nextval('users_id_seq'),
  username varchar(32) NOT NULL UNIQUE CHECK (username <> ''),
  email varchar(128) NOT NULL UNIQUE CHECK (email <> ''),
  password varchar(128) NOT NULL CHECK (password <> ''),
  first_name varchar(32) DEFAULT NULL,
  last_name varchar(32) DEFAULT NULL,
  mobile varchar(16) DEFAULT NULL,
  in_vacation boolean DEFAULT false,
  active boolean DEFAULT false,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  language_id integer DEFAULT NULL REFERENCES languages (id) ON DELETE CASCADE,
  address_id integer DEFAULT NULL REFERENCES addresses (id) ON DELETE CASCADE,
  avatar_id integer DEFAULT NULL REFERENCES resources (id) ON DELETE CASCADE,
  currency_id integer DEFAULT NULL REFERENCES currencies (id) ON DELETE CASCADE
);

-- Create user spoken languages sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_spoken_languages_id_seq') THEN
    CREATE SEQUENCE user_spoken_languages_id_seq;
  END IF;
END $$;

-- Create user spoken languages table
CREATE TABLE IF NOT EXISTS user_spoken_languages (
  id integer PRIMARY KEY DEFAULT nextval('user_spoken_languages_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  language_id integer NOT NULL REFERENCES languages (id) ON DELETE CASCADE
);

-- Create user billing addresses sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_billing_addresses_id_seq') THEN
    CREATE SEQUENCE user_billing_addresses_id_seq;
  END IF;
END $$;

-- Create user billing addresses table
CREATE TABLE IF NOT EXISTS user_billing_addresses (
  id integer PRIMARY KEY DEFAULT nextval('user_billing_addresses_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  address_id integer NOT NULL REFERENCES addresses (id) ON DELETE CASCADE
);

-- Create groups sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'groups_id_seq') THEN
    CREATE SEQUENCE groups_id_seq;
  END IF;
END $$;

-- Create groups table
CREATE TABLE IF NOT EXISTS groups (
  id integer PRIMARY KEY DEFAULT nextval('groups_id_seq'),
  name varchar(64) NOT NULL UNIQUE CHECK (name <> ''),
  description text DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);

-- Create roles sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'roles_id_seq') THEN
    CREATE SEQUENCE roles_id_seq;
  END IF;
END $$;

-- Create roles table
CREATE TABLE IF NOT EXISTS roles (
  id integer PRIMARY KEY DEFAULT nextval('roles_id_seq'),
  code varchar(32) NOT NULL UNIQUE CHECK (code <> ''),
  name varchar(64) DEFAULT NULL,
  description text DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);

-- Create permissions sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'permissions_id_seq') THEN
    CREATE SEQUENCE permissions_id_seq;
  END IF;
END $$;

-- Create permissions table
CREATE TABLE IF NOT EXISTS permissions (
  id integer PRIMARY KEY DEFAULT nextval('permissions_id_seq'),
  code varchar(32) NOT NULL UNIQUE CHECK (code <> ''),
  name varchar(64) DEFAULT NULL,
  description text DEFAULT NULL,
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL
);

-- Create user_verification_tokens sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_verification_tokens_id_seq') THEN
    CREATE SEQUENCE user_verification_tokens_id_seq;
  END IF;
END $$;

-- Create user_verification_tokens table
CREATE TABLE IF NOT EXISTS user_verification_tokens (
  id integer PRIMARY KEY DEFAULT nextval('user_verification_tokens_id_seq'),
  token varchar(256) NOT NULL UNIQUE CHECK (token <> ''),
  expires_at timestamp WITH TIME ZONE DEFAULT NOW() + '5 minutes',
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE
);

DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_api_tokens_id_seq') THEN
    CREATE SEQUENCE user_api_tokens_id_seq;
  END IF;
END $$;

-- Create user_verification_tokens table
CREATE TABLE IF NOT EXISTS user_api_tokens (
  id integer PRIMARY KEY DEFAULT nextval('user_api_tokens_id_seq'),
  token varchar(256) NOT NULL UNIQUE CHECK (token <> ''),
  expires_at timestamp WITH TIME ZONE DEFAULT NOW() + '12 hours',
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE
);

-- Create user_groups sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_groups_id_seq') THEN
    CREATE SEQUENCE user_groups_id_seq;
  END IF;
END $$;

-- Create user_groups table
CREATE TABLE IF NOT EXISTS user_groups (
  id integer PRIMARY KEY DEFAULT nextval('user_groups_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  group_id integer NOT NULL REFERENCES groups (id) ON DELETE CASCADE
);

-- Create user_roles sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'user_roles_id_seq') THEN
    CREATE SEQUENCE user_roles_id_seq;
  END IF;
END $$;

-- Create user_roles table
CREATE TABLE IF NOT EXISTS user_roles (
  id integer PRIMARY KEY DEFAULT nextval('user_roles_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  user_id integer NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  role_id integer NOT NULL REFERENCES roles (id) ON DELETE CASCADE
);

-- Create group_roles sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'group_roles_id_seq') THEN
    CREATE SEQUENCE group_roles_id_seq;
  END IF;
END $$;

-- Create group_roles table
CREATE TABLE IF NOT EXISTS group_roles (
  id integer PRIMARY KEY DEFAULT nextval('group_roles_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  group_id integer NOT NULL REFERENCES groups (id) ON DELETE CASCADE,
  role_id integer NOT NULL REFERENCES roles (id) ON DELETE CASCADE
);

-- Create role_permissions sequence
DO $$ BEGIN
  IF NOT EXISTS (SELECT 1 FROM information_schema.sequences  WHERE sequence_name = 'role_permissions_id_seq') THEN
    CREATE SEQUENCE role_permissions_id_seq;
  END IF;
END $$;

-- Create role_permissions table
CREATE TABLE IF NOT EXISTS role_permissions (
  id integer PRIMARY KEY DEFAULT nextval('role_permissions_id_seq'),
  created_at timestamp WITH TIME ZONE DEFAULT NOW(),
  updated_at timestamp WITH TIME ZONE DEFAULT NOW(),
  deleted_at timestamp WITH TIME ZONE DEFAULT NULL,
  permission_id integer NOT NULL REFERENCES permissions (id) ON DELETE CASCADE,
  role_id integer NOT NULL REFERENCES roles (id) ON DELETE CASCADE
);
