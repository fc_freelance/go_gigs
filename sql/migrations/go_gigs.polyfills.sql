-----------------------------------------------------
----------------------- COMMON ----------------------
-----------------------------------------------------

-- Fill categories
INSERT INTO categories (id, name, description, parent_id) VALUES
  (1, 'Cleaning', 'Cleaning related services', NULL),
  (2, 'Art', 'Art related services', NULL),
  (3, 'Transport', 'Transport related services', NULL),
  (101, 'Garden', 'Garden cleaning related services', 1),
  (102, 'House', 'House cleaning related services', 1),
  (103, 'Car', 'Car cleaning related services', 1) ON CONFLICT DO NOTHING;

-- Fill currencies
INSERT INTO currencies (id, code, name, symbol, conversion) VALUES
  (1, 'DOL', 'Dollar', '$', 1.000),
  (2, 'EUR', 'Euro', '€', 1.210),
  (3, 'GBP', 'Sterling Pound', '£', 1.420) ON CONFLICT DO NOTHING;

-- Fill cities
INSERT INTO languages (id, code, name) VALUES
  (1, 'EN', 'English'),
  (2, 'DE', 'Deutsch'),
  (3, 'ES', 'Spanish') ON CONFLICT DO NOTHING;
