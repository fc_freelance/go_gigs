package util

import (
    "bytes"
    "encoding/hex"
    "crypto/des"
    "crypto/cipher"
    "crypto/md5"
    "crypto/rand"
    "fmt"

    . "github.com/jinzhu/copier"

    cfg "gitlab.com/fc_freelance/go_gigs/config"
    utilModels "gitlab.com/fc_freelance/go_gigs/util/models"
    "log"
)

// crypt: Private struct definition
type crypt utilModels.CryptConfig

// Crypt: Global crypt variable (sort of singleton)
var Crypt crypt

/*
init: Initializes Crypto config
 */
func init() {

    // Try to copy config to model
    if err := Copy(&Crypt, &cfg.Config.Crypto); err != nil {
        log.Panic(err)
    }
}

/*
GetMD5Hash: Gets the MD5 hash for a string
 */
func (c *crypt) GetMD5Hash(text string) string {
    hasher := md5.New()
    hasher.Write([]byte(text))
    return hex.EncodeToString(hasher.Sum(nil))
}

/*
RandomToken: Generates a random Token with a specified length
 */
func (c *crypt) RandomToken(size uint) string {
    b := make([]byte, size)
    rand.Read(b)
    return fmt.Sprintf("%x", b)
}

/*
addPKCS5Padding: Adds padding to text bytes in order to match block size
 */
func (c *crypt) addPKCS5Padding(src []byte, blockSize int) []byte {
    padding := blockSize - len(src) % blockSize
    padtext := bytes.Repeat([]byte{byte(padding)}, padding)
    return append(src, padtext...)
}

/*
removePKCS5Padding: Removes padding from text bytes in order to match block size
 */
func (c *crypt) removePKCS5Padding(src []byte) []byte {
    length := len(src)
    unpadding := int(src[length-1])
    return src[:(length - unpadding)]
}

/*
Encrypt: Encrypts a plain text using Triple DES algorithm
 */
func (c *crypt) Encrypt(text string) (error, string) {

    // Parse the current key to bytes
    key := []byte(c.Key)

    // Parse/Shorten current iv to bytes
    iv := []byte(c.IV)[:des.BlockSize]

    // Create cipher
    block, err := des.NewTripleDESCipher(key)
    if err != nil {
        return err, ""
    }

    // Add padding to plain text if necessary
    plaintext := c.addPKCS5Padding([]byte(text), block.BlockSize())

    // Create encrypter
    encrypter := cipher.NewCBCEncrypter(block, iv)

    // Declare encrypted bytes
    encrypted := make([]byte, len(plaintext))

    // Encrypt plaintext into declared encrypted one
    encrypter.CryptBlocks(encrypted, plaintext)

    // Hex-encode bytes into string
    encryptedText := hex.EncodeToString(encrypted)

    // Return final encrypted text
    return nil, encryptedText
}

/*
Decrypt: Decrypts an encrypted text using Triple DES algorithm
 */
func (c *crypt) Decrypt(text string) (error, string) {

    // Parse the current key to bytes
    key := []byte(c.Key)

    // Parse/Shorten current iv to bytes
    iv := []byte(c.IV)[:des.BlockSize]

    // Hex-decode bytes from string
    encrypted, err := hex.DecodeString(text)
    if err != nil {
        return err, ""
    }

    // Create cipher
    block, err := des.NewTripleDESCipher(key)
    if err != nil {
        return err, ""
    }

    // Create decrypter
    decrypter := cipher.NewCBCDecrypter(block, iv)

    // Declare decrypted bytes
    decrypted := make([]byte, len(encrypted))

    // Decrypt encrypted bytes into declared decrypted one
    decrypter.CryptBlocks(decrypted, encrypted)

    // Remove padding if any
    decrypted = c.removePKCS5Padding(decrypted)

    // Return string from decrypted bytes
    return nil, string(decrypted)
}