package util

import(
    "strconv"
    "net/smtp"
    "log"

    . "github.com/jinzhu/copier"

    cfg "gitlab.com/fc_freelance/go_gigs/config"
    utilModels "gitlab.com/fc_freelance/go_gigs/util/models"
)

// mailer: Private struct definition
type mailer utilModels.MailerConfig

// Mailer: Global mailer variable (sort of singleton)
var Mailer mailer

/*
init: Initializes Mailer config
 */
func init() {

    // Try to copy config to model
    if err := Copy(&Mailer, &cfg.Config.Smtp); err != nil {
        log.Panic(err)
    }
}

/*
SendMail: Sends an email
 */
func (m *mailer) SendMail(sender string, recipients []string, body string) error {

    // Set up authentication information.
    auth := smtp.PlainAuth("", m.User, m.Password, m.Host)

    // Connect to the server, authenticate, set sender, recipient, and send the email.
    err := smtp.SendMail(m.Host + ":" + strconv.Itoa(int(m.Port)), auth, sender, recipients, []byte(body))

    // Return error if any
    return err
}
