/*
Package util: Utilities for Go Gigs App
 */
package util

// util: Private struct definition
type util struct {}

// Util: Global util variable (sort of singleton)
var Util = util{}

/*
StringSliceToInterfaceSlice: Converts string slice to interface slice
 */
func (u *util) StringSliceToInterfaceSlice(sSlice []string) []interface{} {
    iSlice := make([]interface{}, len(sSlice))
    for i, v := range sSlice {
        iSlice[i] = v
    }
    return iSlice
}

/*
StringSliceContainsSlice: Checks if a string slice is contained in other
 */
func (u *util) StringSliceContainsSlice(contained []string, container []string) bool {
    return u.SliceContainsSlice(u.StringSliceToInterfaceSlice(contained), u.StringSliceToInterfaceSlice(container));
}

/*
SliceContainsSlice: Checks if a slice is contained in other
 */
func (u *util) SliceContainsSlice(contained []interface{}, container []interface{}) bool {
    for _, v := range contained {
        if !u.SliceContainsValue(v, container) {
            return false
        }
    }
    return true
}

/*
SliceContainsValue: Checks if a slice contains a value
 */
func (u *util) SliceContainsValue(value interface{}, container []interface{}) bool {
    for _, v := range container {
        if v == value {
            return true
        }
    }
    return false
}