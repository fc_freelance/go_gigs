package models

/*
MailerConfig: Global mailer config model
 */
type MailerConfig struct {
    Host string `json:"host" default:"127.0.0.1"`
    Port uint `json:"port" default:"25"`
    User string `json:"user" default:"admin@localhost"`
    Password string `json:"password" required:"true"`
    Senders struct {
        Admin string `json:"admin" default:"admin@localhost"`
        Support string `json:"support" default:"admin@localhost"`
    } `json:"senders"`
}
