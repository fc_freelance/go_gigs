package models

/*
AssetsConfig: Global assets config model
 */
type AssetsConfig struct {
    Local struct {
        Enabled bool `json:"enabled" default:"true"`
        Host string `json:"host" default:"127.0.0.1"`
        User string `json:"user" default:""`
        Password string `json:"password" default:""`
        Path string `json:"path" default:"/tmp"`
    } `json:"local"`
    Remote struct {
        S3 struct {
            AccessKey string `json:"access_key" default:""`
            Bucket string `json:"bucket" default:"gogigs"`
            Prefix string `json:"prefix" default:"assets/"`
            SecretAccessKey string `json:"secret_access_key" default:""`
            Region string `json:"region" default:""`
        } `json:"s3"`
    } `json:"remote"`
    Cdn struct {
        Url string `json:"url" default:"http://127.0.0.1/assets"`
    } `json:"cdn"`
}
