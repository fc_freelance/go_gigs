/*
Package models - Util models
 */
package models

/*
CryptConfig: Global crypt config model
 */
type CryptConfig struct {
    Key string `json:"key" default:"123456781234567812345678"`
    IV string `json:"iv" default:"12345678"`
}
