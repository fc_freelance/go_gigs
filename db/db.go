/*
Package db - Handles Go Gigs database connection
 */
package db

import (
    "log"
    "os"
    "strconv"

    _ "github.com/lib/pq"
    "github.com/jinzhu/gorm"

    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
    cfg "gitlab.com/fc_freelance/go_gigs/config"
)

// DB: Global variable for DB handle
var DB *gorm.DB

// conf: Global (private) variable for DB config
var conf dbModels.Config

/*
init: Initializes database config
 */
func init() {

    // Set config
    conf = cfg.Config.Db
}

/*
Init: Initializes database connection
 */
func Init() {

    // Parse some DB parameters from config
    dbPort := strconv.Itoa(int(conf.Port))

    // Build connection string
    dbHash := conf.Type + "://" + conf.User + ":" + conf.Password + "@" +
                conf.Host + ":" + dbPort + "/" + conf.Database + "?sslmode=disable"

    // Error variable needed
    var err error

    // Connect to DB
    DB, err = gorm.Open(conf.Type, dbHash)
    if  err != nil {
        log.Fatal(err.Error())
        os.Exit(1)
    }

    // Init underlying DB, then we could invoke `*sql.DB`'s functions with it
    DB.DB()

    // Enable Logger, show detailed log
    DB.LogMode(true)

    // Check DB with a ping
    if err = DB.DB().Ping(); err != nil {
        log.Panic(err.Error())
        os.Exit(1)
    }

    // Just in case set to default OS/DB max connections
    DB.DB().SetMaxOpenConns(0)
}
