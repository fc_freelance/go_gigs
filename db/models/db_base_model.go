/*
Package models - Database models
 */
package models

import(
    "time"
)

/*
DbBaseModel: Override default gorm model to include proper snake_case json fields
 */
type DbBaseModel struct {
    ID uint `gorm:"primary_key" json:"id"`
    CreatedAt time.Time `json:"-"`
    UpdatedAt time.Time `json:"-"`
    DeletedAt *time.Time `sql:"index" json:"-"`
}
