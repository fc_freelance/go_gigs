/*
Package models - Database models
 */
package models

/*
Config: Database configuration struct
 */
type Config struct {
    Type string `json:"type" default:"postgres"`
    Host string `json:"host" default:"127.0.0.1"`
    Port uint `json:"port" default:"5432"`
    Database string `json:"database" default:"go_gigs"`
    User string `json:"user" default:"go_gigs"`
    Password string `json:"password" required:"true"`
}
