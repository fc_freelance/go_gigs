# The commands in this file will now be run with `bash` (not `sh`)
SHELL := /bin/bash

# Source directory
SRC_DIR := $(shell pwd)

# Config directory
CFG_DIR := $(SRC_DIR)/conf

# Source directory
TESTS_DIR := $(SRC_DIR)/test/postman

# Database migrations directory
MIGRATIONS_DIR := $(SRC_DIR)/sql

# Deployment directory
DEPLOY_DIR := $(GOPATH)/bin

# Declare targets
.PHONY: install service deploy test migrate doc

# @process install
# Installs binary and copies environment configuration file
install:
	@echo 'Installing app ...'
	@cd $(SRC_DIR)
	@go install >/dev/null
	@mkdir -p $(DEPLOY_DIR)/conf >/dev/null
	@cp $(CFG_DIR)/config.json $(DEPLOY_DIR)/conf/config.json >/dev/null
	@echo 'App Installed'

# @process service
# Installs service into system
service:
	@echo "Installing service ..."
	@sudo cp $(CFG_DIR)/init.script /etc/init.d/go_gigs >/dev/null
	@sudo chmod +x /etc/init.d/go_gigs >/dev/null
	@sudo update-rc.d go_gigs defaults >/dev/null
	@echo "Service installed"

# @process deploy
# Deploys service into remote system
deploy:
	@echo "Deploying to production server ..."
	@ssh gogigs@46.101.20.221 'source .profile;cd go/src/gitlab.com/fc_freelance/go_gigs; git fetch --all; git reset --hard origin/master; make migrate; make install; make doc; make service; sudo service go_gigs restart >/dev/null; make test'
	@echo "Deployment completed"

# @process test
# Run tests
test:
	@echo "Running all tests ..."
	@newman -c $(TESTS_DIR)/Go_Gigs_Tests.postman_collection.json -e $(CFG_DIR)/postman.environment.json -d $(TESTS_DIR)/Go_Gigs_Tests.input_data_01.postman.json
	@echo "Tests completed"

# @process migrate
# Migrate database
migrate:
	@$(MIGRATIONS_DIR)/migrate.sh go_gigs

# @process doc
# Generates documentation with apidocjs
doc:
	@echo 'Generating documentation ...'
	@cd $(SRC_DIR)
	@apidoc -c conf/ -f '\.go$ ' -i ./ -o doc/
	@echo 'Documentation generated'
