package http

import (
    "net/http"

    "github.com/gin-gonic/gin"

    cfg "gitlab.com/fc_freelance/go_gigs/config"
    authServices "gitlab.com/fc_freelance/go_gigs/modules/auth/services"
)

/*
Permission Middleware: Checks proper permissions for request
 */
func Permission(permissions []string) gin.HandlerFunc {

    // Return gin handler
    return func(c *gin.Context) {

        // If no permissions set, just continue, it's publicly open
        if (permissions == nil) {
            c.Next()
            return
        }

        // Try to get cookie from request
        cookie, err := c.Request.Cookie(cfg.Config.Http.AuthCookie)

        // Try to get token from request
        token := c.Request.Header.Get((cfg.Config.Http.AuthToken))

        // Checks if error getting the cookie, so 401 then
        if err != nil && token == "" {
            c.JSON(http.StatusUnauthorized, gin.H{
                "error": "Unauthorized",
            })
            c.Abort()
            return
        }

        // Call service and return error if any
        httpErr, session := authServices.AuthService.CheckSession(cookie, token)
        if httpErr != nil {
            c.JSON(httpErr.StatusCode, gin.H{
                "error": httpErr.Message,
            })
            c.Abort()
            return
        }

        // Call service and return error if any
        httpErr = authServices.AuthService.CheckPermission(session, permissions)
        if httpErr != nil {
            c.JSON(httpErr.StatusCode, gin.H{
                "error": httpErr.Message,
            })
            c.Abort()
            return
        }

        // Auth/Permissions passed, continue
        c.Next()
    }
}