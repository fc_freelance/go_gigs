package models

import "github.com/gin-gonic/gin"

/*
Route: HTTP Route struct
 */
type Route struct {
    Method string `json:"status"`
    Path string `json:"version"`
    Permissions []string `json:"permissions"`
    Handler func(c *gin.Context) `json:"handler"`
}
