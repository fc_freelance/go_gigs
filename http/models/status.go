package models

/*
Status: HTTP Server API status payload struct
 */
type Status struct {
    Status string `json:"status"`
    Version string `json:"version"`
}
