/*
Package models - HTTP models
 */
package models

/*
Config: HTTP configuration struct
 */
type Config struct {
    Port uint `json:"port" default:"8181"`
    AuthCookie string `json:"auth_cookie" default:"go_gigs_ck"`
    AuthToken string `json:"auth_token" default:"go_gigs_tkn"`
    AuthTime uint `json:"auth_time" default:"60000"`
    AuthTimeRememberMultiplier uint `json:"auth_time_remember_multiplier" default:"7"`
}
