/*
Package http - Manages Go Gigs HTTP server
*/
package http

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"

	cfg "gitlab.com/fc_freelance/go_gigs/config"
	httpModels "gitlab.com/fc_freelance/go_gigs/http/models"

	"gitlab.com/fc_freelance/go_gigs/modules/auth"
	"gitlab.com/fc_freelance/go_gigs/modules/common"
	"gitlab.com/fc_freelance/go_gigs/modules/gig_requests"
	"gitlab.com/fc_freelance/go_gigs/modules/gigs"
	"gitlab.com/fc_freelance/go_gigs/modules/users"
)

/*
Init : Server initialization
*/
func Init() {

	// Get new router instance
	router := gin.Default()

	// Status endpoint
	router.GET("/", status)

	// Set variable holding handler function for a route
	var routeMethodFunc func(string, ...gin.HandlerFunc) gin.IRoutes

	// Create routes slice
	routes := []httpModels.Route{}

	// Append all imported routes here
	routes = append(routes, common.Routes...)
	routes = append(routes, auth.Routes...)
	routes = append(routes, users.Routes...)
	routes = append(routes, gigs.Routes...)
	routes = append(routes, gig_requests.Routes...)

	// Loop over all routes
	for _, route := range routes {

		// Select proper handler for the route method
		switch route.Method {
		case "GET":
			routeMethodFunc = router.GET
		case "POST":
			routeMethodFunc = router.POST
		case "PUT":
			routeMethodFunc = router.PUT
		case "DELETE":
			routeMethodFunc = router.DELETE
		}

		// Check if supported method and define the route if so
		if routeMethodFunc != nil {
			routeMethodFunc(route.Path, Permission(route.Permissions), route.Handler)
		}
	}

	// Listen and exit/log error if any
	if err := router.Run(":" + strconv.Itoa(int(cfg.Config.Http.Port))); err != nil {
		log.Fatal(err.Error())
		os.Exit(2)
	}
}

/**
 * @api {get} / Get API status
 * @apiName GetStatus
 * @apiGroup General
 * @apiVersion 1.0.0
 *
 * @apiSuccess (Success response payload) {String} status Server API status.
 * @apiSuccess (Success response payload) {string} version Server API version.
 *
 * @apiSuccessExample {json} Success response example
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "OK",
 *       "version": "1.0.0"
 *     }
 *
 * @apiUse InternalServerError
 */

/*
status: Status Endpoint
 GET /
*/
func status(c *gin.Context) {

	// Build status model instance from config
	status := httpModels.Status{
		Status:  "OK",
		Version: cfg.Config.Version,
	}

	// Send OK JSON response
	c.JSON(http.StatusOK, status)
}
