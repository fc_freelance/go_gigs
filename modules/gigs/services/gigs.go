/*
Package services: Gigs module services
*/
package services

import (
	"net/http"

	httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
	commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
	commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
	gigModels "gitlab.com/fc_freelance/go_gigs/modules/gigs/models"
	gigRepositories "gitlab.com/fc_freelance/go_gigs/modules/gigs/repositories"
)

// gigService: Service private struct
type gigService struct{}

// GigService: Service global variable (sort of singleton)
var GigService = gigService{}

/*
All: Gets all gigs
*/
func (s *gigService) All() (*httpModels.HttpError, *[]gigModels.Gig) {

	// Return collection or error in repository
	return gigRepositories.GigRepository.All()
}

/*
Get: Gets a gig
*/
func (s *gigService) Get(id int) (*httpModels.HttpError, *gigModels.Gig) {

	// Find model or error in repository
	return gigRepositories.GigRepository.Get(id)
}

/*
Find: Finds a gig
*/
func (s *gigService) Find(gig gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

	// Find model or error in repository
	return gigRepositories.GigRepository.Find(gig)
}

/*
Create: Creates a Gig
*/
func (s *gigService) Create(gig gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

	// Handle relations first
	if err, _ := s.handleRelations(&gig); err != nil {
		return err, nil
	}

	// Create model or error in repository
	err, createdGig := gigRepositories.GigRepository.Create(gig)
	if err != nil {
		return err, nil
	}

	// Finally return created gig from DB
	return s.Get(int(createdGig.ID))
}

/*
Update: Updates a Gig
*/
func (s *gigService) Update(id int, gig gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

	// Handle relations first
	if err, _ := s.handleRelations(&gig); err != nil {
		return err, nil
	}

	// Update model or error in repository
	if err, _ := gigRepositories.GigRepository.Update(id, gig); err != nil {
		return err, nil
	}

	// Finally return updated gig from DB
	return s.Get(id)
}

/*
Delete: Deletes a Gig by id
*/
func (s *gigService) Delete(id int) (*httpModels.HttpError, *gigModels.Gig) {

	// Delete model or error in repository
	return gigRepositories.GigRepository.Delete(id)
}

/*
HandleRelations: Handles model relations (validate, assigns, etc.) and returns the modified? model
*/
func (s *gigService) handleRelations(gig *gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

	// Ensure no category object
	gig.Category = nil

	// Check proper category ID
	if gig.CategoryID > 0 {

		// Get category data
		err, category := commonServices.CategoryService.Get(int(gig.CategoryID))

		// If error, throw it
		if err != nil {
			return err, nil
		}

		// Properly retrieved, set it to model
		gig.Category = category

		// No proper category id, 400
	} else {
		return &httpModels.HttpError{
			Message:    "Invalid category ID",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// Ensure no currency object
	gig.Currency = nil

	// Check proper currency ID
	if gig.CurrencyID > 0 {

		// Get currency data
		err, currency := commonServices.CurrencyService.Get(int(gig.CurrencyID))

		// If error, throw it
		if err != nil {
			return err, nil
		}

		// Properly retrieved, set it to model
		gig.Currency = currency

		// No proper currency id, 400
	} else {
		return &httpModels.HttpError{
			Message:    "Invalid currency ID",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// FIXME: There's an issue in GORM, so it ALWAYS updates many2many related tables before insrting the relation
	// FIXME: Github issue (closed but issue still exists): https://github.com/jinzhu/gorm/issues/329
	// FIXME: The workaround would be to set this to empty array, save gig, then validate/assign them one by one with 'Assign'?
	// FIXME: Not sure if it works, but this is the code:
	// FIXME: db.Model(&gig).Association("GigSchedules.GigScheduleLocations").Append(GigScheduleLocation{LocationId = 123}) ... mmmm
	// Ok, we don't allow to create locations here, just pass the IDs, get ref to current schedules
	var schedules []gigModels.GigSchedule

	// Now iterate over received schedules array
	for _, s := range gig.GigSchedules {

		// For this schedule, create a locations collection
		var locations []commonModels.Location

		// Now iterate over received locations (cities) array for this schedule
		for _, l := range s.GigScheduleLocations {

			// Only process it if got ID, otherwise ignore
			if l.ID != 0 {

				// Get category data
				err, location := commonServices.LocationService.Get(int(l.ID))

				// If got data, append to collection, otherwise skip
				if err == nil && location != nil {
					locations = append(locations, *location)
				}
			}
		}

		// Reassign existing locations full data
		s.GigScheduleLocations = locations

		// And add to schedules
		schedules = append(schedules, s)
	}

	// And assign back to schedules
	gig.GigSchedules = schedules

	// Return success with modified model
	return nil, gig
}
