package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
GigScheduleTime: Public GigScheduleTime model struct
 */
type GigScheduleTime struct {
    dbModels.DbBaseModel
    GigScheduleID uint `form:"gig_schedule_id" json:"-" binding:"required"`
    GigSchedule *GigSchedule `json:"-"`
    GigScheduleTimeDays []GigScheduleTimeDay `json:"gig_schedule_time_days,omitempty"`
    GigScheduleTimeRanges []GigScheduleTimeRange `json:"gig_schedule_time_ranges,omitempty"`
}
