package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

/*
GigSchedule: Public GigSchedule model struct
 */
type GigSchedule struct {
    dbModels.DbBaseModel
    GigID uint `form:"gig_id" json:"-" binding:"required"`
    Gig *Gig `json:"-"`
    GigScheduleTimes []GigScheduleTime `json:"gig_schedule_times,omitempty"`
    GigScheduleAvailabilities []GigScheduleAvailability `json:"gig_schedule_availabilities,omitempty"`
    GigScheduleLocations []commonModels.Location `json:"gig_schedule_locations,omitempty" gorm:"many2many:gig_schedule_locations"`
}
