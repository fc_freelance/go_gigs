package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
GigScheduleTimeRange: Public GigScheduleTimeRange model struct
 */
type GigScheduleTimeRange struct {
    dbModels.DbBaseModel
    TimeFrom *string `form:"time_from" json:"time_from,omitempty"`
    TimeTo *string `form:"time_to" json:"time_to,omitempty"`
    GigScheduleTimeID uint `form:"gig_schedule_time_id" json:"-" binding:"required"`
    GigScheduleTime *GigScheduleTime `json:"-"`
}
