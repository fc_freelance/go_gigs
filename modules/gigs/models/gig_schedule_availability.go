package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
GigScheduleAvailability: Public GigScheduleAvailability model struct
 */
type GigScheduleAvailability struct {
    dbModels.DbBaseModel
    DateFrom *string `form:"date_from" json:"date_from,omitempty"`
    DateTo *string `form:"date_to" json:"date_to,omitempty"`
    GigScheduleID uint `form:"gig_schedule_id" json:"-" binding:"required"`
    GigSchedule *GigSchedule `json:"-"`
}
