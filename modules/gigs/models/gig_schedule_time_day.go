package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
GigScheduleTimeDay: Public GigScheduleTimeDay model struct
 */
type GigScheduleTimeDay struct {
    dbModels.DbBaseModel
    Weekday string `form:"weekday" json:"weekday" binding:"required"`
    GigScheduleTimeID uint `form:"gig_schedule_time_id" json:"-" binding:"required"`
    GigScheduleTime *GigScheduleTime `json:"-"`
}
