package models

import (
    "github.com/jinzhu/gorm"
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
GigItem Public GigItem model struct
 */
type GigItem struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Description *string `form:"description" json:"description,omitempty"`
    Price *float64 `form:"price" json:"price,omitempty"`
    GigID uint `form:"gig_id" json:"-" binding:"required"`
    Gig *Gig `json:"-"`
}

// TODO: IS this a proper handling? It's an additional query etc ... maybe get full gig and copy new values over with copier???
// BeforeUpdate callback for preventing updating created_at column
func (g *GigItem) BeforeUpdate(scope *gorm.Scope) (err error) {
    
    // Set var holding the existing GigItem if any
    var gg GigItem
    
    // Try to get it and check if found
    if !scope.DB().Where("id = ?", g.ID).First(&gg).RecordNotFound() {
        
        // If found, set the CreatedAt column to existing value
        scope.SetColumn("CreatedAt", gg.CreatedAt)
    }
    
    // Don't return error at all
    return nil
}