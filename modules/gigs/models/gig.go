/*
Package models - Gigs related models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

/*
Gig: Public Gig model struct
 */
type Gig struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Description string `form:"description" json:"description" binding:"required"`
    Price float64 `form:"price" json:"price" binding:"required"`
    UserID uint `json:"user_id"`
    User *userModels.User `json:"-"`
    CurrencyID uint `form:"currency_id" json:"currency_id" binding:"required"`
    Currency *commonModels.Currency `json:"currency,omitempty"`
    CategoryID uint `form:"category_id" json:"category_id" binding:"required"`
    Category *commonModels.Category `json:"category,omitempty"`
    GigItems []GigItem `json:"gig_items,omitempty"`
    GigSchedules []GigSchedule `json:"gig_schedules,omitempty"`
    GigResources []commonModels.Resource `json:"gig_resources,omitempty" gorm:"many2many:gig_resources"`
}
