/*
Package gigs - Manages everything related to Gigs
 */
package gigs

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
    gigControllers "gitlab.com/fc_freelance/go_gigs/modules/gigs/controllers"
)

/*
Routes Global routes variable
 */
var Routes = []httpModels.Route{

    /******************************************************************************************/
    /****************************************** GIGS *****************************************/
    /******************************************************************************************/

    /**
     * @apidefine GigCreateRequestBody
     *
     * @apiParam (Request body) {String} name Gig name.
     * @apiParam (Request body) {String} description Gig description.
     * @apiParam (Request body) {Number} price Gig price.
     * @apiParam (Request body) {Number} category_id The id of the category. It should be taken from existing ones.
     * @apiParam (Request body) {Number} currency_id The id of the currency. It should be taken from existing ones.
     * @apiParam (Request body) {Object[]} [gig_items] Items included in Gig.
     * @apiParam (Request body) {String} gig_items.name Item name.
     * @apiParam (Request body) {String} [gig_items.description] Item description.
     * @apiParam (Request body) {Number} [gig_items.price] Item price, if any.
     * @apiParam (Request body) {Object[]} [gig_resources] Media resources associated to Gig (videos, images, etc.)
     * @apiParam (Request body) {String="image","video","audio"} gig_resources.type Type of resource (video, audio, etc.).
     * @apiParam (Request body) {String} [gig_resources.name] Name for the resource.
     * @apiParam (Request body) {String} gig_resources.url Url where resource is located.
     * @apiParam (Request body) {Object[]} [gig_schedules] Combination of locations, availabilities and schedule times for the Gig.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times Time schedules for a Gig schedule entry.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_ranges Daily time ranges Gig can be delivered on.
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_from Earlier day time Gig can be delivered on. Must be formatted like 18:00
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_to Later day time Gig can be delivered on. Must be formatted like 18:00
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_days Week days Gig can be delivered on.
     * @apiParam (Request body) {String="Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"} gig_schedules.gig_schedule_times.gig_schedule_time_days.weekday Week day Gig can be delivered on.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_availabilities Calendar availability for a Gig schedule entry.
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_availabilities.date_from Day Availability date from, must be in date format like 2016-08-01.
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_availabilities.date_to Day Availability date to, must be in date format like 2016-08-01.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_locations Available locations (cities) for a Gig schedule entry.
     * @apiParam (Request body) {Number} gig_schedules.gig_schedule_locations.id Id of the location (city) the Gig can be delivered in. It should be taken from existing ones.
     *
     * @apiParamExample {json} Request body example
     * {
     *     "name": "Some Gig",
     *     "description": "My fancy Gig",
     *     "price": 45.32,
     *     "category_id": 101,
     *     "currency_id": 7,
     *     "gig_items": [
     *         {
     *             "name": "My Item 1",
     *             "description": "this fancy item 1",
     *             "price": 10
     *         },
     *         {
     *             "name": "My Item 2",
     *             "description": "this fancy item 2",
     *             "price": 99.9
     *         }
     *     ],
     *     "gig_resources": [
     *         {
     *             "type": "video",
     *             "name": "my show video",
     *             "url": "http://myvideourl.ftw"
     *         }
     *     ],
     *     "gig_schedules": [
     *         {
     *             "gig_schedule_times": [
     *                 {
     *                     "gig_schedule_time_ranges": [
     *                         {
     *                             "time_from": "16:00:00",
     *                             "time_to": "19:00:00"
     *                         }
     *                     ],
     *                     "gig_schedule_time_days": [
     *                         {
     *                             "weekday": "Tuesday"
     *                         },
     *                         {
     *                             "weekday": "Thursday"
     *                         }
     *                     ]
     *                 }
     *             ],
     *             "gig_schedule_availabilities": [
     *                 {
     *                     "date_from": "2016-04-21",
     *                     "date_to": "2016-07-15"
     *                 }
     *             ],
     *             "gig_schedule_locations": [
     *                 {
     *                     "id": 1
     *                 }
     *             ]
     *         }
     *     ]
     * }
     */

    /**
     * @apidefine GigUpdateRequestBody
     *
     * @apiParam (Request body) {String} name Gig name.
     * @apiParam (Request body) {String} description Gig description.
     * @apiParam (Request body) {Number} price Gig price.
     * @apiParam (Request body) {Number} category_id The id of the category. It should be taken from existing ones.
     * @apiParam (Request body) {Number} currency_id The id of the currency. It should be taken from existing ones.
     * @apiParam (Request body) {Object[]} [gig_items] Items included in Gig.
     * @apiParam (Request body) {Number} [gig_items.id] Existing item id for updating it. If not provided, a new item will be created.     
     * @apiParam (Request body) {String} gig_items.name Item name.
     * @apiParam (Request body) {String} [gig_items.description] Item description.
     * @apiParam (Request body) {Number} [gig_items.price] Item price, if any.
     * @apiParam (Request body) {Object[]} [gig_resources] Media resources associated to Gig (videos, images, etc.)
     * @apiParam (Request body) {Number} [gig_resources.id] Existing resource id for updating it. If not provided, a new resource will be created.     
     * @apiParam (Request body) {String="image","video","audio"} gig_resources.type Type of resource (video, audio, etc.).
     * @apiParam (Request body) {String} [gig_resources.name] Name for the resource.
     * @apiParam (Request body) {String} gig_resources.url Url where resource is located.
     * @apiParam (Request body) {Object[]} [gig_schedules] Combination of locations, availabilities and schedule times for the Gig.
     * @apiParam (Request body) {Number} [gig_schedules.id] Existing schedule id for updating it. If not provided, a new schedule will be created.     
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times Time schedules for a Gig schedule entry.
     * @apiParam (Request body) {Number} [gig_schedules.gig_schedule_times.id] Existing schedule time id for updating it. If not provided, a new schedule time will be created.     
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_ranges Day time ranges Gig can be delivered on.
     * @apiParam (Request body) {Number} [gig_schedules.gig_schedule_times.gig_schedule_time_ranges.id] Existing schedule time range id for updating it. If not provided, a new schedule time range will be created.     
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_from Earlier day time Gig can be delivered on. Must be formatted like 18:00
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_to Later day time Gig can be delivered on. Must be formatted like 18:00
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_days Week days Gig can be delivered on.
     * @apiParam (Request body) {Number} [gig_schedules.gig_schedule_times.gig_schedule_time_days.id] Existing schedule time days id for updating it. If not provided, a new schedule time days will be created.     
     * @apiParam (Request body) {String="Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"} gig_schedules.gig_schedule_times.gig_schedule_time_days.weekday Week day Gig can be delivered on.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_availabilities Calendar availability for a Gig schedule entry.
     * @apiParam (Request body) {Number} [gig_schedules.gig_schedule_availabilities.id] Existing availability id for updating it. If not provided, a new availability will be created.     
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_availabilities.date_from Day Availability date from, must be in date format like 2016-08-01.
     * @apiParam (Request body) {String} gig_schedules.gig_schedule_availabilities.date_to Day Availability date to, must be in date format like 2016-08-01.
     * @apiParam (Request body) {Object[]} gig_schedules.gig_schedule_locations Available locations (cities) for a Gig schedule entry.
     * @apiParam (Request body) {Number} gig_schedules.gig_schedule_locations.id Id of the location (city) the Gig can be delivered in. It should be taken from existing ones.
     *
     * @apiParamExample {json} Success response payload example
     * {
     *     "name": "My Gig new name",
     *     "description": "faaancy gig again!",
     *     "price": 88.22,
     *     "category_id": 101,
     *     "currency_id": 7,
     *     "gig_items": [
     *         {
     *             "id": 1,
     *             "name": "new Item spec",
     *             "description": "this fancy item is now cheaper!",
     *             "price": 5.9
     *         }
     *     ],
     *     "gig_resources": [
     *         {
     *             "id": 1,
     *             "type": "video",
     *             "name": "my show video modified",
     *             "url": "http://myvideourl2.ftw"
     *         }
     *     ],
     *     "gig_schedules": [
     *         {
     *             "id": 1,
     *             "gig_schedule_times": [
     *                 {
     *                     "id": 1,    
     *                     "gig_schedule_time_ranges": [
     *                         {
     *                             "id": 1,
     *                             "time_from": "18:00:00",
     *                             "time_to": "21:00:00"
     *                         }
     *                     ],
     *                     "gig_schedule_time_days": [
     *                         {
     *                             "id": 1,
     *                             "weekday": "Tuesday"
     *                         },
     *                         {
     *                             "id": 2,
     *                             "weekday": "Friday"
     *                         }
     *                     ]
     *                 }
     *             ],
     *             "gig_schedule_availabilities": [
     *                 {
     *                     "id": 1,
     *                     "date_from": "2016-05-01",
     *                     "date_to": "2016-07-31"
     *                 }
     *             ],
     *             "gig_schedule_locations": [
     *                 {
     *                     "id": 2
     *                 }
     *             ]
     *         }
     *     ]
     * }
     */

    /**
     * @apidefine GigPayload
     *
     * @apiSuccess (Success response payload) {Number} id Gig id.
     * @apiSuccess (Success response payload) {String} name Gig name.
     * @apiSuccess (Success response payload) {String} description Gig description.
     * @apiSuccess (Success response payload) {Number} price Gig price.
     * @apiSuccess (Success response payload) {Number} user_id User id (expert) this gig belongs to.
     * @apiSuccess (Success response payload) {Object} category Category Gig belongs to.
     * @apiSuccess (Success response payload) {Object} currency Currency Gig uses.
     * @apiSuccess (Success response payload) {Object[]} gig_items Items included in Gig.
     * @apiSuccess (Success response payload) {Number} gig_items.id Item id.     
     * @apiSuccess (Success response payload) {String} gig_items.name Item name.
     * @apiSuccess (Success response payload) {String} gig_items.description Item description.
     * @apiSuccess (Success response payload) {Number} gig_items.price Item price, if any.
     * @apiSuccess (Success response payload) {Object[]} gig_resources Media resources associated to Gig (videos, images, etc.)
     * @apiSuccess (Success response payload) {Object[]} gig_schedules Combination of locations, availabilities and schedule times for the Gig.
     * @apiSuccess (Success response payload) {Number} gig_schedules.id Schedule id.     
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times Time schedules for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.id Schedule time id.     
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_ranges Day time ranges Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.id Schedule time range id.     
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_from Earlier day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_to Later day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_days Week days Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.gig_schedule_time_days.id Schedule time days id.     
     * @apiSuccess (Success response payload) {String="Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"} gig_schedules.gig_schedule_times.gig_schedule_time_days.weekday Week day Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_availabilities Calendar availability for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_availabilities.id Availability id.     
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_availabilities.date_from Day Availability date from.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_availabilities.date_to Day Availability date to.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_locations Available locations (cities) for a Gig schedule entry.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome gig",
     *       "description": "my fancy new gig",
     *       "price": 35.56,
     *       "user_id": 2,
     *       "category": {
     *           "id": 101,
     *           "name": "Garden",
     *           "description": "Garden cleaning related services",
     *           "parent": {
     *               "id": 1,
     *               "name": "Cleaning",
     *               "description": "Cleaning related services"
     *           }
     *       },
     *       "currency": {
     *           "id": 7,
     *           "code": "DL",
     *           "symbol": "$",
     *           "conversion": 1.000
     *       },
     *       "gig_items": [
     *           {
     *               "id": 1,
     *               "name": "new Item spec",
     *               "description": "this fancy item is now cheaper!",
     *               "price": 5.9
     *           }
     *       ],
     *       "gig_resources": [
     *           {
     *               "id": 1,
     *               "type": "video",
     *               "name": "my video",
     *               "url": "http://myvideourl2.ftw"
     *           }
     *       ],
     *       "gig_schedules": [
     *           {
     *               "id": 1,
     *               "gig_schedule_times": [
     *                   {
     *                       "id": 1,    
     *                       "gig_schedule_time_ranges": [
     *                           {
     *                               "id": 1,
     *                               "time_from": "18:00:00",
     *                               "time_to": "21:00:00"
     *                           }
     *                       ],
     *                       "gig_schedule_time_days": [
     *                           {
     *                               "id": 1,
     *                               "weekday": "Tuesday"
     *                           },
     *                           {
     *                               "id": 2,
     *                               "weekday": "Friday"
     *                           }
     *                       ]
     *                   }
     *               ],
     *               "gig_schedule_availabilities": [
     *                   {
     *                       "id": 1,
     *                       "date_from": "2016-05-01",
     *                       "date_to": "2016-07-31"
     *                   }
     *               ],
     *               "gig_schedule_locations": [
     *                   {
     *                       "id": 2,
     *                       "name": "Berlin",
     *                       "code": "DE-BE-01",
     *                       "lat": 52.5244,
     *                       "lon": 13.4105,
     *                       "parent": {
     *                           "id": 1,
     *                           "name": "Deutschland",
     *                           "code": "DE",
     *                           "lat": 51.5,
     *                           "lon": 10.5
     *                       }
     *                   }
     *               ]
     *           }
     *       ]
     *     }
     */

    /**
     * @apidefine GigCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Gig id.
     * @apiSuccess (Success response payload) {String} name Gig name.
     * @apiSuccess (Success response payload) {String} description Gig description.
     * @apiSuccess (Success response payload) {Number} price Gig price.
     * @apiSuccess (Success response payload) {Number} user_id User id (expert) this gig belongs to.
     * @apiSuccess (Success response payload) {Object} category Category Gig belongs to.
     * @apiSuccess (Success response payload) {Object} currency Currency Gig uses.
     * @apiSuccess (Success response payload) {Object[]} gig_items Items included in Gig.
     * @apiSuccess (Success response payload) {Number} gig_items.id Item id.
     * @apiSuccess (Success response payload) {String} gig_items.name Item name.
     * @apiSuccess (Success response payload) {String} gig_items.description Item description.
     * @apiSuccess (Success response payload) {Number} gig_items.price Item price, if any.
     * @apiSuccess (Success response payload) {Object[]} gig_resources Media resources associated to Gig (videos, images, etc.)
     * @apiSuccess (Success response payload) {Object[]} gig_schedules Combination of locations, availabilities and schedule times for the Gig.
     * @apiSuccess (Success response payload) {Number} gig_schedules.id Schedule id.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times Time schedules for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.id Schedule time id.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_ranges Day time ranges Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.id Schedule time range id.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_from Earlier day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_to Later day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_times.gig_schedule_time_days Week days Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_times.gig_schedule_time_days.id Schedule time days id.
     * @apiSuccess (Success response payload) {String="Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"} gig_schedules.gig_schedule_times.gig_schedule_time_days.weekday Week day Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_availabilities Calendar availability for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} gig_schedules.gig_schedule_availabilities.id Availability id.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_availabilities.date_from Day Availability date from.
     * @apiSuccess (Success response payload) {String} gig_schedules.gig_schedule_availabilities.date_to Day Availability date to.
     * @apiSuccess (Success response payload) {Object[]} gig_schedules.gig_schedule_locations Available locations (cities) for a Gig schedule entry.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome gig",
     *       "description": "my fancy new gig",
     *       "price": 35.56,
     *       "user_id": 2,
     *       "category": {
     *           "id": 101,
     *           "name": "Garden",
     *           "description": "Garden cleaning related services",
     *           "parent": {
     *               "id": 1,
     *               "name": "Cleaning",
     *               "description": "Cleaning related services"
     *           }
     *       },
     *       "currency": {
     *           "id": 7,
     *           "code": "DL",
     *           "symbol": "$",
     *           "conversion": 1.000
     *       },
     *       "gig_items": [
     *           {
     *               "id": 1,
     *               "name": "new Item spec",
     *               "description": "this fancy item is now cheaper!",
     *               "price": 5.9
     *           }
     *       ],
     *       "gig_resources": [
     *           {
     *               "id": 1,
     *               "type": "video",
     *               "name": "my video",
     *               "url": "http://myvideourl2.ftw"
     *           }
     *       ],
     *       "gig_schedules": [
     *           {
     *               "id": 1,
     *               "gig_schedule_times": [
     *                   {
     *                       "id": 1,
     *                       "gig_schedule_time_ranges": [
     *                           {
     *                               "id": 1,
     *                               "time_from": "18:00:00",
     *                               "time_to": "21:00:00"
     *                           }
     *                       ],
     *                       "gig_schedule_time_days": [
     *                           {
     *                               "id": 1,
     *                               "weekday": "Tuesday"
     *                           },
     *                           {
     *                               "id": 2,
     *                               "weekday": "Friday"
     *                           }
     *                       ]
     *                   }
     *               ],
     *               "gig_schedule_availabilities": [
     *                   {
     *                       "id": 1,
     *                       "date_from": "2016-05-01",
     *                       "date_to": "2016-07-31"
     *                   }
     *               ],
     *               "gig_schedule_locations": [
     *                   {
     *                       "id": 2,
     *                       "name": "Berlin",
     *                       "code": "DE-BE-01",
     *                       "lat": 52.5244,
     *                       "lon": 13.4105,
     *                       "parent": {
     *                           "id": 1,
     *                           "name": "Deutschland",
     *                           "code": "DE",
     *                           "lat": 51.5,
     *                           "lon": 10.5
     *                       }
     *                   }
     *               ]
     *           }
     *       ]
     *     }
     */

    /**
     * @apidefine GigsPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Gig objects.
     * @apiSuccess (Success response payload) {Number} _.id Gig id.
     * @apiSuccess (Success response payload) {String} _.name Gig name.
     * @apiSuccess (Success response payload) {String} _.description Gig description.
     * @apiSuccess (Success response payload) {Number} _.price Gig price.
     * @apiSuccess (Success response payload) {Number} _.user_id User id (expert) this gig belongs to.
     * @apiSuccess (Success response payload) {Object} _.category Category Gig belongs to.
     * @apiSuccess (Success response payload) {Object} _.currency Currency Gig uses.
     * @apiSuccess (Success response payload) {Object[]} _.gig_items Items included in Gig.
     * @apiSuccess (Success response payload) {Number} _.gig_items.id Item id.     
     * @apiSuccess (Success response payload) {String} _.gig_items.name Item name.
     * @apiSuccess (Success response payload) {String} _.gig_items.description Item description.
     * @apiSuccess (Success response payload) {Number} _.gig_items.price Item price, if any.
     * @apiSuccess (Success response payload) {Object[]} _.gig_resources Media resources associated to Gig (videos, images, etc.)
     * @apiSuccess (Success response payload) {Number} _.gig_resources.id Resource id.     
     * @apiSuccess (Success response payload) {String="image","video","audio"} _.gig_resources.type Type of resource (video, audio, etc.).
     * @apiSuccess (Success response payload) {String} _.gig_resources.name Name for the resource.
     * @apiSuccess (Success response payload) {String} _.gig_resources.url Url where resource is located.
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules Combination of locations, availabilities and schedule times for the Gig.
     * @apiSuccess (Success response payload) {Number} _.gig_schedules.id Schedule id.     
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules.gig_schedule_times Time schedules for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} _.gig_schedules.gig_schedule_times.id Schedule time id.     
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules.gig_schedule_times.gig_schedule_time_ranges Day time ranges Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} _.gig_schedules.gig_schedule_times.gig_schedule_time_ranges.id Schedule time range id.     
     * @apiSuccess (Success response payload) {String} _.gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_from Earlier day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {String} _.gig_schedules.gig_schedule_times.gig_schedule_time_ranges.time_to Later day time Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules.gig_schedule_times.gig_schedule_time_days Week days Gig can be delivered on.
     * @apiSuccess (Success response payload) {Number} _.gig_schedules.gig_schedule_times.gig_schedule_time_days.id Schedule time days id.     
     * @apiSuccess (Success response payload) {String="Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"} _.gig_schedules.gig_schedule_times.gig_schedule_time_days.weekday Week day Gig can be delivered on.
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules.gig_schedule_availabilities Calendar availability for a Gig schedule entry.
     * @apiSuccess (Success response payload) {Number} _.gig_schedules.gig_schedule_availabilities.id Availability id.     
     * @apiSuccess (Success response payload) {String} _.gig_schedules.gig_schedule_availabilities.date_from Day Availability date from.
     * @apiSuccess (Success response payload) {String} _.gig_schedules.gig_schedule_availabilities.date_to Day Availability date to.
     * @apiSuccess (Success response payload) {Object[]} _.gig_schedules.gig_schedule_locations Available locations (cities) for a Gig schedule entry.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "My awesome gig",
     *         "description": "my fancy new gig",
     *         "price": 35.56,
     *         "user_id": 2,
     *         "category": {
     *             "id": 101,
     *             "name": "Garden",
     *             "description": "Garden cleaning related services",
     *             "parent": {
     *                 "id": 1,
     *                 "name": "Cleaning",
     *                 "description": "Cleaning related services"
     *             }
     *         },
     *         "currency": {
     *             "id": 7,
     *             "code": "DL",
     *             "symbol": "$",
     *             "conversion": 1.000
     *         },
     *         "gig_items": [
     *             {
     *                 "id": 1,
     *                 "name": "new Item spec",
     *                 "description": "this fancy item is now cheaper!",
     *                 "price": 5.9
     *             }
     *         ],
     *         "gig_resources": [
     *             {
     *                 "id": 1,
     *                 "type": "video",
     *                 "name": "fancy video",
     *                 "url": "http://myvideourl2.ftw"
     *             }
     *         ],
     *         "gig_schedules": [
     *             {
     *                 "id": 1,
     *                 "gig_schedule_times": [
     *                     {
     *                         "id": 1,    
     *                         "gig_schedule_time_ranges": [
     *                             {
     *                                 "id": 1,
     *                                 "time_from": "18:00:00",
     *                                 "time_to": "21:00:00"
     *                             }
     *                         ],
     *                         "gig_schedule_time_days": [
     *                             {
     *                                 "id": 1,
     *                                 "weekday": "Tuesday"
     *                             },
     *                             {
     *                                 "id": 2,
     *                                 "weekday": "Friday"
     *                             }
     *                         ]
     *                     }
     *                 ],
     *                 "gig_schedule_availabilities": [
     *                     {
     *                         "id": 1,
     *                         "date_from": "2016-05-01",
     *                         "date_to": "2016-07-31"
     *                     }
     *                 ],
     *                 "gig_schedule_locations": [
     *                     {
     *                         "id": 1,
     *                         "name": "Berlin",
     *                         "code": "DE-BE-01",
     *                         "lat": 52.5244,
     *                         "lon": 13.4105,
     *                         "parent": {
     *                             "id": 1,
     *                             "name": "Deutschland",
     *                             "code": "DE",
     *                             "lat": 51.5,
     *                             "lon": 10.5
     *                         }
     *                     }
     *                 ]
     *             }
     *         ]
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /gigs Get All Gigs
     * @apiName GetGigs
     * @apiGroup Gigs
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiUse GigsPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/gigs",
        Permissions: nil,
        Handler: gigControllers.GigController.All,
    },

    /**
     * @api {get} /gigs/:id Get Gig
     * @apiName GetGig
     * @apiGroup Gigs
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Gig unique id.
     *
     * @apiUse GigPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/gigs/:id",
        Permissions: nil,
        Handler: gigControllers.GigController.Get,
    },

    /**
     * @api {post} /gigs Create Gig
     * @apiName PostGig
     * @apiGroup Gigs
     * @apiVersion 1.0.0
     * @apiPermission ExpertAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse GigCreateRequestBody
     *
     * @apiUse GigCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/gigs",
        Permissions: []string{authModels.Permissions.ManageGigs},
        Handler: gigControllers.GigController.Create,
    },

    /**
     * @api {put} /gigs/:id Update Gig
     * @apiName PutGig
     * @apiGroup Gigs
     * @apiVersion 1.0.0
     * @apiPermission ExpertAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Gig unique id.
     *
     * @apiUse GigUpdateRequestBody
     *
     * @apiUse GigPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/gigs/:id",
        Permissions: []string{authModels.Permissions.ManageGigs},
        Handler: gigControllers.GigController.Update,
    },

    /**
     * @api {delete} /gigs/:id Delete Gig
     * @apiName DeleteGig
     * @apiGroup Gigs
     * @apiVersion 1.0.0
     * @apiPermission ExpertAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Gig unique id.
     *
     * @apiUse GigPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/gigs/:id",
        Permissions: []string{authModels.Permissions.ManageGigs},
        Handler: gigControllers.GigController.Delete,
    },
}
