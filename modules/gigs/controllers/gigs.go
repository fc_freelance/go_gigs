/*
Package controllers: Gigs module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    //commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    gigModels "gitlab.com/fc_freelance/go_gigs/modules/gigs/models"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"

    //commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
    gigServices "gitlab.com/fc_freelance/go_gigs/modules/gigs/services"
    authServices "gitlab.com/fc_freelance/go_gigs/modules/auth/services"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
    "log"
)

// gigController: Controller private struct
type gigController struct {}

// GigController: Controller global variable (sort of singleton)
var GigController = gigController{}

/*
All: Gets all gigs
 GET /gigs
 */
func (ct *gigController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, gigs := gigServices.GigService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with gigs list
    c.JSON(http.StatusOK, gigs)
}

/*
Get: Gets a gig
 GET /gigs/:id
 */
func (ct *gigController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing gig id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, gig := gigServices.GigService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with gigs list
    c.JSON(http.StatusOK, gig)
}

/*
Create: Creates a new gig
 POST /gigs
 */
func (ct *gigController) Create(c *gin.Context) {

    // Declare Gig model
    var gig gigModels.Gig

    // Error binding request data to model, 400
    if err := c.BindJSON(&gig); err != nil {
        log.Println(err)
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service for session and return error if any
    httpErr, session := authServices.AuthService.GetSession(c)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Get user (expert) for gig
    err, user := userServices.UserService.Get(int(session.UserId), false)
    if err != nil || user == nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Could not get user info",
        })
        return
    }

    // Assign user to gig
    gig.UserID = session.UserId

    // Call service and throw error if any
    httpErr, createdGig := gigServices.GigService.Create(gig)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added gig
    c.JSON(http.StatusCreated, createdGig)
}

/*
Update: Updates a gig
 PUT /gigs/:id
 */
func (ct *gigController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing gig id",
        })
        return
    }

    // Call service to get gig and throw error if any
    err1, gigRead := gigServices.GigService.Get(int(id))
    if err1 != nil {
        c.JSON(err1.StatusCode, gin.H{
            "error": err1.Message,
        })
        return
    }
    
    // Check it is same user or an admin one
    err2, usr := authServices.AuthService.GetSessionUser(c)
    if err2 != nil || (usr.ID != gigRead.UserID && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Unauthorized",
        })
        return
    }

    // Declare gig model
    var gig gigModels.Gig

    // Error binding request data to model, 400
    if err := c.BindJSON(&gig); err != nil {
        log.Println(err, gig)
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedGig := gigServices.GigService.Update(int(id), gig)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified gig
    c.JSON(http.StatusOK, updatedGig)
}

/*
Delete: Deletes a gig
 DELETE /gigs/:id
 */
func (ct *gigController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing gig id",
        })
        return
    }

    // Call service to get gig and throw error if any
    err1, gigRead := gigServices.GigService.Get(int(id))
    if err1 != nil {
        c.JSON(err1.StatusCode, gin.H{
            "error": err1.Message,
        })
        return
    }
    
    // Check it is same user or an admin one
    err2, usr := authServices.AuthService.GetSessionUser(c)
    if err2 != nil || (usr.ID != gigRead.UserID && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Unauthorized",
        })
        return
    }

    // Call service and throw error if any
    httpErr, gig := gigServices.GigService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, gig)
}
