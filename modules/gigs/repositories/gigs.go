/*
Package repositories: Gigs module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    gigModels "gitlab.com/fc_freelance/go_gigs/modules/gigs/models"
)

// gigRepository: Repository private struct
type gigRepository struct {}

// GigRepository: Repository global variable (sort of singleton)
var GigRepository = gigRepository{}

/*
All: Gets all gigs
 */
func (r *gigRepository) All() (*httpModels.HttpError, *[]gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    gigs := []gigModels.Gig{}

    // Run query and handle error
    err := db.DB.
        Preload("GigItems").
        Preload("GigResources").
        Preload("GigSchedules.GigScheduleAvailabilities").
        Preload("GigSchedules.GigScheduleLocations.Parent").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeRanges").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeDays").
        Preload("Category.Parent").
        Preload("Currency").
        Find(&gigs).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &gigs
}

/*
Get: Gets a gig
 */
func (r *gigRepository) Get(id int) (*httpModels.HttpError, *gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    gig := gigModels.Gig{}

    // Run query and handle error
    err := db.DB.
        Preload("GigItems").
        Preload("GigResources").
        Preload("GigSchedules.GigScheduleAvailabilities").
        Preload("GigSchedules.GigScheduleLocations.Parent").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeRanges").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeDays").
        Preload("Category.Parent").
        Preload("Currency").
        Where("id = ?", id).
        Find(&gig).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return model
    return nil, &gig
}

/*
Find: Finds a gig
 */
func (r *gigRepository) Find(gg gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold gig
    var gig gigModels.Gig

    // Run query and handle error
    err := db.DB.
        Preload("GigItems").
        Preload("GigResources").
        Preload("GigSchedules.GigScheduleAvailabilities").
        Preload("GigSchedules.GigScheduleLocations.Parent").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeRanges").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeDays").
        Preload("Category.Parent").
        Preload("Currency").
        Where(&gg).
        First(&gig).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &gig
}

/*
Create: Creates a Gig
 */
func (r *gigRepository) Create(gig gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create gig",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(gig)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Gig already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&gig).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created model
    return nil, &gig
}

/*
Update: Updates a gig
 */
func (r *gigRepository) Update(id int, gig gigModels.Gig) (*httpModels.HttpError, *gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update gig",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched gig
    var gg gigModels.Gig

    // Update gig in DB, handle error
    err := db.DB.
        Preload("GigItems").
        Preload("GigResources").
        Preload("GigSchedules.GigScheduleAvailabilities").
        Preload("GigSchedules.GigScheduleLocations.Parent").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeRanges").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeDays").
        Preload("Category.Parent").
        Preload("Currency").
        First(&gg, id).
        Updates(gig).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &gg
}

/*
Delete: Deletes a gig
 */
func (r *gigRepository) Delete(id int) (*httpModels.HttpError, *gigModels.Gig) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete gig",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold gig
    var gig gigModels.Gig

    // Delete gig from DB, handle error
    err := db.DB.
        Preload("GigItems").
        Preload("GigResources").
        Preload("GigSchedules.GigScheduleAvailabilities").
        Preload("GigSchedules.GigScheduleLocations.Parent").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeRanges").
        Preload("GigSchedules.GigScheduleTimes.GigScheduleTimeDays").
        Preload("Category.Parent").
        Preload("Currency").
        First(&gig, id).
        Delete(&gig).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &gig
}
