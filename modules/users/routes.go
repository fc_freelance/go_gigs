/*
Package users - Users module.

Handles all endpoints related to Users management.
It includes Groups, Permissions and Roles.
 */
package users

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userControllers "gitlab.com/fc_freelance/go_gigs/modules/users/controllers"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
)

/*
Routes Module API routes global variable.
 */
var Routes = []httpModels.Route{

    /******************************************************************************************/
    /****************************************** USERS *****************************************/
    /******************************************************************************************/

    /**
     * @apidefine UserCreateRequestBody
     *
     * @apiParam (Request body) {String} [username] User nick.
     * @apiParam (Request body) {String} email User email.
     * @apiParam (Request body) {String{8..}} password User password.
     * @apiParam (Request body) {String="user","expert"} [role=user] User role.
     * @apiParam (Request body) {File} [avatar] User avatar image file.
     *
     * @apiParamExample {form-data} Request body example
     *       username=myusernick
     *       email=unai.gg77@gmail.com
     *       password=mypass
     *       role=user
     *       --------------------------
     *       avatar=my_file.png
     */

    /**
     * @apidefine UserUpdateRequestBody
     *
     * @apiParam (Request body) {String} [username] User nick.
     * @apiParam (Request body) {String} [email] User email.
     * @apiParam (Request body) {String{8..}} [password] User password.
     * @apiParam (Request body) {String} [first_name] User first name.
     * @apiParam (Request body) {String} [last_name] User last name.
     * @apiParam (Request body) {String} [mobile] User mobile phone number.
     * @apiParam (Request body) {String="user","expert"}} [role=user] User role.
     * @apiParam (Request body) {File} [avatar] User avatar image file.
     *
     * @apiParamExample {multipart/form-data} Request body example
     *       username=mynewnick
     *       email=newmail@gmail.com
     *       password=mynewpass
     *       first_name=Meeeee
     *       first_name=Moo
     *       mobile=+441534784597
     *       role=user
     *       --------------------------
     *       avatar=my_file.png
     */

    /**
     * @apidefine UserPayload
     *
     * @apiSuccess (Success response payload) {Number} id User id.
     * @apiSuccess (Success response payload) {String} username User nick.
     * @apiSuccess (Success response payload) {String} email User email.
     * @apiSuccess (Success response payload) {String} first_name User first name.
     * @apiSuccess (Success response payload) {String} last_name User last name.
     * @apiSuccess (Success response payload) {String} mobile User mobile phone number.
     * @apiSuccess (Success response payload) {Object} address User address.
     * @apiSuccess (Success response payload) {Object} language User language.
     * @apiSuccess (Success response payload) {Object} currency User currency.
     * @apiSuccess (Success response payload) {Object} avatar User avatar.
     * @apiSuccess (Success response payload) {Object[]} groups Groups user belongs to.
     * @apiSuccess (Success response payload) {Object[]} roles User roles.
     * @apiSuccess (Success response payload) {Object[]} spoken_languages Languages user speaks.
     * @apiSuccess (Success response payload) {Object[]} billing_addresses User has.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "username": "myusernick",
     *       "email": "unai.gg77@gmail.com",
     *       "first_name": "Meeeee",
     *       "last_name": "moooo",
     *       "mobile": "123456789",
     *       "address": {...},
     *       "language": {...},
     *       "currency": {...},
     *       "avatar": {...},
     *       "groups": [...],
     *       "roles": [...],
     *       "spoken_languages": [...],
     *       "billing_addresses": [...]
     *     }
     */

    /**
     * @apidefine UserCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id User id.
     * @apiSuccess (Success response payload) {String} username User nick.
     * @apiSuccess (Success response payload) {String} email User email.
     * @apiSuccess (Success response payload) {Object[]} roles User roles.
     * @apiSuccess (Success response payload) {Object} avatar User avatar resource Object.
     * @apiSuccess (Success response payload) {Object} verification_token User verification token object.
     * @apiSuccess (Success response payload) {String} verification_token.expires_at Date string of the token expiration date.
     * @apiSuccess (Success response payload) {String} verification_token.token Verification token.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "username": "myusernick",
     *       "email": "unai.gg77@gmail.com",
     *       "roles": [...],
     *       "avatar": {...},
     *       "verification_token": {
     *          "expires_at": "2016-03-28T11:38:30.281155888+02:00",
     *          "token": "d185bc0d8c599e3a"
     *       }
     *     }
     */

    /**
     * @apidefine UsersPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of User objects.
     * @apiSuccess (Success response payload) {Number} _.id User id.
     * @apiSuccess (Success response payload) {String} _.username User nick.
     * @apiSuccess (Success response payload) {String} _.email User email.
     * @apiSuccess (Success response payload) {String} _.first_name User first name.
     * @apiSuccess (Success response payload) {String} _.last_name User last name.
     * @apiSuccess (Success response payload) {String} _.mobile User mobile phone number.
     * @apiSuccess (Success response payload) {Object} _.address User address.
     * @apiSuccess (Success response payload) {Object} _.language User language.
     * @apiSuccess (Success response payload) {Object} _.currency User currency.
     * @apiSuccess (Success response payload) {Object} _.avatar User avatar.
     * @apiSuccess (Success response payload) {Object[]} _.groups Groups user belongs to.
     * @apiSuccess (Success response payload) {Object[]} _.roles User roles.
     * @apiSuccess (Success response payload) {Object[]} _.spoken_languages Languages user speaks.
     * @apiSuccess (Success response payload) {Object[]} _.billing_addresses User has.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "username": "myusernick",
     *         "email": "unai.gg77@gmail.com",
     *         "first_name": "Meeeee",
     *         "last_name": "",
     *         "mobile": "",
     *         "address": {...},
     *         "language": {...},
     *         "currency": {...},
     *         "avatar": {...},
     *         "groups": [...],
     *         "roles": [...],
     *         "spoken_languages": [...],
     *         "billing_addresses": [...]
     *       },
     *       ...
     *     ]
     */


    /**
     * @api {get} /users Get All Users
     * @apiName GetUsers
     * @apiGroup Users
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiUse UsersPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/users",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.UserController.All,
    },

    /**
     * @api {get} /user/verify/:token Verify User
     * @apiName GetVerifyUser
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {String} token User verification token.
     *
     * @apiUse UserPayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/user/verify/:token",
        Permissions: nil,
        Handler: userControllers.UserController.Verify,
    },

    /**
     * @api {get} /users/:id Get User
     * @apiName GetUser
     * @apiGroup Users
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id User unique id.
     *
     * @apiUse UserPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/users/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.UserController.Get,
    },

    /**
     * @api {post} /users Create User
     * @apiName PostUser
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiUse MultipartHeader
     *
     * @apiUse UserCreateRequestBody
     *
     * @apiUse UserCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/users",
        Permissions: nil,
        Handler: userControllers.UserController.Create,
    },

    /**
     * @api {put} /users/:id Update User
     * @apiName PutUser
     * @apiGroup Users
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse MultipartHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id User unique id.
     *
     * @apiUse UserUpdateRequestBody
     *
     * @apiUse UserPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/users/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.UserController.Update,
    },

    /**
     * @api {delete} /users/:id Delete User
     * @apiName DeleteUser
     * @apiGroup Users
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id User unique id.
     *
     * @apiUse UserPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/users/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.UserController.Delete,
    },


    /******************************************************************************************/
    /***************************************** GROUPS *****************************************/
    /******************************************************************************************/

    /**
     * @apidefine GroupRequestBody
     *
     * @apiParam (Request body) {String} name Group name.
     * @apiParam (Request body) {String} description Group description.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "name": "MyGroup",
     *       "description": "my fancy new group"
     *     }
     */

    /**
     * @apidefine GroupPayload
     *
     * @apiSuccess (Success response payload) {Number} id Group id.
     * @apiSuccess (Success response payload) {String} name Group name.
     * @apiSuccess (Success response payload) {String} description Group description.
     * @apiSuccess (Success response payload) {Object[]} users Users in the group.
     * @apiSuccess (Success response payload) {Object[]} roles Group roles.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "MyGroup",
     *       "description": "my fancy new group",
     *       "users": [...],
     *       "roles": [...]
     *     }
     */

    /**
     * @apidefine GroupCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Group id.
     * @apiSuccess (Success response payload) {String} name Group name.
     * @apiSuccess (Success response payload) {String} description Group description.
     * @apiSuccess (Success response payload) {Object[]} users Users in the group.
     * @apiSuccess (Success response payload) {Object[]} roles Group roles.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "MyGroup",
     *       "description": "my fancy new group",
     *       "users": [...],
     *       "roles": [...]
     *     }
     */

    /**
     * @apidefine GroupsPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Group objects.
     * @apiSuccess (Success response payload) {Number} _.id Group id.
     * @apiSuccess (Success response payload) {String} _.name Group name.
     * @apiSuccess (Success response payload) {String} _.description Group description.
     * @apiSuccess (Success response payload) {Object[]} _.users Users in the group.
     * @apiSuccess (Success response payload) {Object[]} _.roles Group roles.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "MyGroup",
     *         "description": "my fancy new group",
     *         "users": [...],
     *         "roles": [...]
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /groups Get All Groups
     * @apiName GetGroups
     * @apiGroup Groups
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse GroupsPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/groups",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.GroupController.All,
    },

    /**
     * @api {get} /groups/:id Get Group
     * @apiName GetGroup
     * @apiGroup Groups
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiParam (Parameters) {Number} id Group unique id.
     *
     * @apiUse GroupPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/groups/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.GroupController.Get,
    },

    /**
     * @api {post} /groups Create Group
     * @apiName PostGroup
     * @apiGroup Groups
     * @apiVersion 1.0.0
     *
     * @apiUse JsonHeader
     *
     * @apiUse GroupRequestBody
     *
     * @apiUse GroupCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/groups",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.GroupController.Create,
    },

    /**
     * @api {put} /groups/:id Update Group
     * @apiName PutGroup
     * @apiGroup Groups
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiUse JsonHeader

     * @apiParam (Parameters) {Number} id Group unique id.
     *
     * @apiUse GroupRequestBody
     *
     * @apiUse GroupPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/groups/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.GroupController.Update,
    },

    /**
     * @api {delete} /groups/:id Delete Group
     * @apiName DeleteGroup
     * @apiGroup Groups
     * @apiVersion 1.0.0
     * @apiPermission UserAuth
     *
     * @apiParam (Parameters) {Number} id Group unique id.
     *
     * @apiUse GroupPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/groups/:id",
        Permissions: []string{authModels.Permissions.ListUsers},
        Handler: userControllers.GroupController.Delete,
    },



    /******************************************************************************************/
    /****************************************** ROLES *****************************************/
    /******************************************************************************************/

    /**
     * @apidefine RoleRequestBody
     *
     * @apiParam (Request body) {String} code Role code.
     * @apiParam (Request body) {String} name Role name.
     * @apiParam (Request body) {String} description Role description.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "code": "my_new_role",
     *       "name": "MyNewRole",
     *       "description": "my fancy new role"
     *     }
     */

    /**
     * @apidefine RolePayload
     *
     * @apiSuccess (Success response payload) {Number} id Role id.
     * @apiSuccess (Success response payload) {String} code Role code.
     * @apiSuccess (Success response payload) {String} name Role name.
     * @apiSuccess (Success response payload) {String} description Role description.
     * @apiSuccess (Success response payload) {Object[]} permissions Role permissions.
     * @apiSuccess (Success response payload) {Object[]} groups Groups with this role.
     * @apiSuccess (Success response payload) {Object[]} users Users with this role.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 4,
     *       "code": "my_new_role",
     *       "name": "MyNewRole",
     *       "description": "my fancy new role",
     *       "permissions": [...],
     *       "groups": [...],
     *       "users": [...]
     *     }
     */

    /**
     * @apidefine RoleCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Role id.
     * @apiSuccess (Success response payload) {String} code Role code.
     * @apiSuccess (Success response payload) {String} name Role name.
     * @apiSuccess (Success response payload) {String} description Role description.
     * @apiSuccess (Success response payload) {Object[]} permissions Role permissions.
     * @apiSuccess (Success response payload) {Object[]} groups Groups with this role.
     * @apiSuccess (Success response payload) {Object[]} users Users with this role.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 4,
     *       "code": "my_new_role",
     *       "name": "MyNewRole",
     *       "description": "my fancy new role",
     *       "permissions": [...],
     *       "groups": [...],
     *       "users": [...]
     *     }
     */

    /**
     * @apidefine RolesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Role objects.
     * @apiSuccess (Success response payload) {Number} _.id Role id.
     * @apiSuccess (Success response payload) {String} _.code Role code.
     * @apiSuccess (Success response payload) {String} _.name Role name.
     * @apiSuccess (Success response payload) {String} _.description Role description.
     * @apiSuccess (Success response payload) {Object[]} _.permissions Role permissions.
     * @apiSuccess (Success response payload) {Object[]} _.groups Groups with this role.
     * @apiSuccess (Success response payload) {Object[]} _.users Users with this role.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 4,
     *         "code": "my_new_role",
     *         "name": "MyNewRole",
     *         "description": "my fancy new role",
     *         "permissions": [...],
     *         "groups": [...],
     *         "users": [...]
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /roles Get All Roles
     * @apiName GetRoles
     * @apiGroup Roles
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse RolesPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/roles",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.RoleController.All,
    },

    /**
     * @api {get} /roles/:id Get Role
     * @apiName GetRole
     * @apiGroup Roles
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id Role unique id.
     *
     * @apiUse RolePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/roles/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.RoleController.Get,
    },

    /**
     * @api {post} /roles Create Role
     * @apiName PostRole
     * @apiGroup Roles
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiUse RoleRequestBody
     *
     * @apiUse RoleCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/roles",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.RoleController.Create,
    },

    /**
     * @api {put} /roles/:id Update Role
     * @apiName PutRole
     * @apiGroup Roles
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader

     * @apiParam (Parameters) {Number} id Role unique id.
     *
     * @apiUse RoleRequestBody
     *
     * @apiUse RolePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/roles/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.RoleController.Update,
    },

    /**
     * @api {delete} /roles/:id Delete Role
     * @apiName DeleteRole
     * @apiGroup Roles
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id Role unique id.
     *
     * @apiUse RolePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/roles/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.RoleController.Delete,
    },


    /******************************************************************************************/
    /*************************************** PERMISSIONS **************************************/
    /******************************************************************************************/

    /**
     * @apidefine PermissionRequestBody
     *
     * @apiParam (Request body) {String} code Permission code.
     * @apiParam (Request body) {String} name Permission name.
     * @apiParam (Request body) {String} description Permission description.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "code": "my_new_permission",
     *       "name": "MyNewPermission",
     *       "description": "my fancy new permission"
     *     }
     */

    /**
     * @apidefine PermissionPayload
     *
     * @apiSuccess (Success response payload) {Number} id Permission id.
     * @apiSuccess (Success response payload) {String} code Permission code.
     * @apiSuccess (Success response payload) {String} name Permission name.
     * @apiSuccess (Success response payload) {String} description Permission description.
     * @apiSuccess (Success response payload) {Object[]} roles Roles with this Permission.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 6,
     *       "code": "my_new_permission",
     *       "name": "MyNewPermission",
     *       "description": "my fancy new permission",
     *       "roles": [...]
     *     }
     */

    /**
     * @apidefine PermissionCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Permission id.
     * @apiSuccess (Success response payload) {String} code Permission code.
     * @apiSuccess (Success response payload) {String} name Permission name.
     * @apiSuccess (Success response payload) {String} description Permission description.
     * @apiSuccess (Success response payload) {Object[]} roles Roles with this Permission.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 6,
     *       "code": "my_new_permission",
     *       "name": "MyNewPermission",
     *       "description": "my fancy new permission",
     *       "roles": [...]
     *     }
     */

    /**
     * @apidefine PermissionsPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Permission objects.
     * @apiSuccess (Success response payload) {Number} _.id Permission id.
     * @apiSuccess (Success response payload) {String} _.code Permission code.
     * @apiSuccess (Success response payload) {String} _.name Permission name.
     * @apiSuccess (Success response payload) {String} _.description Permission description.
     * @apiSuccess (Success response payload) {Object[]} _.roles Roles with this Permission.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 6,
     *         "code": "my_new_Permission",
     *         "name": "MyNewPermission",
     *         "description": "my fancy new Permission",
     *         "roles": [...]
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /permissions Get All Permissions
     * @apiName GetPermissions
     * @apiGroup Permissions
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse PermissionsPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/permissions",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.PermissionController.All,
    },

    /**
     * @api {get} /permissions/:id Get Permission
     * @apiName GetPermission
     * @apiGroup Permissions
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id Permission unique id.
     *
     * @apiUse PermissionPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/permissions/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.PermissionController.Get,
    },

    /**
     * @api {post} /permissions Create Permission
     * @apiName PostPermission
     * @apiGroup Permissions
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiUse PermissionRequestBody
     *
     * @apiUse PermissionCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/permissions",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.PermissionController.Create,
    },

    /**
     * @api {put} /permissions/:id Update Permission
     * @apiName PutPermission
     * @apiGroup Permissions
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader

     * @apiParam (Parameters) {Number} id Permission unique id.
     *
     * @apiUse PermissionRequestBody
     *
     * @apiUse PermissionPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/permissions/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.PermissionController.Update,
    },

    /**
     * @api {delete} /permissions/:id Delete Permission
     * @apiName DeletePermission
     * @apiGroup Permissions
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id Permission unique id.
     *
     * @apiUse PermissionPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/permissions/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.PermissionController.Delete,
    },



    /******************************************************************************************/
    /******************************** USER VERIFICATION TOKENS ********************************/
    /******************************************************************************************/

    /**
     * @apidefine UserVerificationTokenRequestBody
     *
     * @apiParam (Request body) {String} token The token.
     * @apiParam (Request body) {Number} user_id id of the user the token is associated to.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "token": "large_unreadable_token",
     *       "user_id": 1
     *     }
     */


    /**
     * @api {get} /user_verification_tokens Get All User verification tokens
     * @apiName GetUserVerificationTokens
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/user_verification_tokens",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserVerificationTokenController.All,
    },

    /**
     * @api {get} /permissions/:id Get User verification token
     * @apiName GetUserVerificationToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/user_verification_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserVerificationTokenController.Get,
    },

    /**
     * @api {post} /permissions Create User verification tokens
     * @apiName PostUserVerificationToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiUse UserVerificationTokenRequestBody
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/user_verification_tokens",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserVerificationTokenController.Create,
    },

    /**
     * @api {put} /permissions/:id Update User verification tokens
     * @apiName PutUserVerificationToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse UserVerificationTokenRequestBody
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/user_verification_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserVerificationTokenController.Update,
    },

    /**
     * @api {delete} /permissions/:id Delete User verification tokens
     * @apiName DeleteUserVerificationToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/user_verification_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserVerificationTokenController.Delete,
    },

    /******************************************************************************************/
    /************************************ USER API TOKENS *************************************/
    /******************************************************************************************/

    /**
     * @apidefine UserApiTokenRequestBody
     *
     * @apiParam (Request body) {String} token The token.
     * @apiParam (Request body) {Number} user_id id of the user the token is associated to.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "token": "large_unreadable_token",
     *       "user_id": 1
     *     }
     */


    /**
     * @api {get} /user_api_tokens Get All User verification tokens
     * @apiName GetUserApiTokens
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/user_api_tokens",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserApiTokenController.All,
    },

    /**
     * @api {get} /permissions/:id Get User verification token
     * @apiName GetUserApiToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/user_api_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserApiTokenController.Get,
    },

    /**
     * @api {post} /permissions Create User verification tokens
     * @apiName PostUserApiToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiUse UserApiTokenRequestBody
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/user_api_tokens",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserApiTokenController.Create,
    },

    /**
     * @api {put} /permissions/:id Update User verification tokens
     * @apiName PutUserApiToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse UserApiTokenRequestBody
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/user_api_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserApiTokenController.Update,
    },

    /**
     * @api {delete} /permissions/:id Delete User verification tokens
     * @apiName DeleteUserApiToken
     * @apiGroup User Verification Tokens
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiParam (Parameters) {Number} id User verification token unique id.
     *
     * @apiUse MethodNotAllowedError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/user_api_tokens/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: userControllers.UserApiTokenController.Delete,
    },
}
