package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"
    . "github.com/jinzhu/copier"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
    authServices "gitlab.com/fc_freelance/go_gigs/modules/auth/services"
    "time"
    cfg "gitlab.com/fc_freelance/go_gigs/config"
    "gitlab.com/fc_freelance/go_gigs/util"
    // "log"
    "github.com/gin-gonic/gin/binding"
    "os"
    "log"
    "io"
)

// userController: Private controler struct
type userController struct {}

// UserController: Controller global variable (sort of singleton)
var UserController = userController{}

/*
All: Gets all users
 GET /users
 */
func (ct *userController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, users := userServices.UserService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with users list
    c.JSON(http.StatusOK, users)
}

/*
Get: Gets a user
 GET /users/:id
 */
func (ct *userController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing user id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, user := userServices.UserService.Get(int(id), false)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with user payload
    c.JSON(http.StatusOK, user)
}

/*
Create: Creates a new user
 POST /users
 */
func (ct *userController) Create(c *gin.Context) {

    // Declare form model
    var form userModels.SignUp

    // Error binding request data to model, 400
    if err := c.BindWith(&form, binding.FormMultipart); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Convert form data into user model
    user := userModels.User{}
    err := Copy(&user, &form)
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{
            "error": "Could not set user data from form values.",
        })
        return
    }

    // Try to get avatar file from Form, check if got it with no errors
    file, header , err := c.Request.FormFile("avatar")
    if err == nil && file != nil {

        // Get name from data header
        filename := header.Filename

        // TODO: Check if remote ... etc

        // Get local path for file
        filePath := cfg.Config.Assets.Local.Path + "/avatars/" + filename

        // TODO: Check folder exists, create it otherwise
        // Create local file, defer stream release and check for errors
        out, err := os.Create(filePath)
        defer out.Close()
        if err != nil {
            log.Fatal(err)
            c.JSON(http.StatusInternalServerError, gin.H{
                "error": err.Error(),
            })
            return
        }

        // Copy stream to file, check for errors
        _, err = io.Copy(out, file)
        if err != nil {
            log.Fatal(err)
            c.JSON(http.StatusInternalServerError, gin.H{
                "error": err.Error(),
            })
            return
        }

        // Bind created file resource to User Avatar, it should be created by GORM
        user.Avatar = &commonModels.Resource{
            // TODO: this name should come from somewhere else or just remove it
            Name: &filename,
            Type: "image",
            Url: cfg.Config.Assets.Cdn.Url + "/avatars/" + filename,
        }
    }

    // Call service and throw error if any
    httpErr, createdUser := userServices.UserService.Create(user, form.Role)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Generate a token for user verification
    httpErr, token := userServices.UserVerificationTokenService.Create(userModels.UserVerificationToken{User: createdUser})
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Set payload variable
    var resp gin.H

    // Check if we're to send email or not
    if cfg.Config.Options.Users.SendSignupVerifyMail {

        // Build full email body, including verification link
        body := "From: " + util.Mailer.Senders.Admin + "\n" +
                "Reply-To: " + user.Email + "\n" +
                "To: " + util.Mailer.Senders.Admin + "\n" +
                "Subject: GoGigs - Account verification\n\n" +
                "Welcome to GO GIGS!!! Please verify your account by clicking the link below:\n\n" +
                cfg.Config.Options.Users.SignupVerifyUrl + token.Token + "\n\n" +
                "Regards,\nGoGigs Team"

        // Send it
        if mailErr := util.Mailer.SendMail(util.Mailer.Senders.Admin, []string{user.Email}, body); mailErr != nil {
            c.JSON(http.StatusInternalServerError, gin.H{
                "error": mailErr.Error(),
            })
            return
        }

        // Send payload without token
        resp = gin.H{"user": token.User}

    // No mail, send token in payload
    } else {
        resp = gin.H{
            "user": token.User,
            "verification_token": gin.H{
                "token": token.Token,
                "expires_at": token.ExpiresAt,
            },
        }
    }

    // Send response with added user payload
    c.JSON(http.StatusCreated, resp)
}

/*
Update: Updates a user
 PUT /users/:id
 */
func (ct *userController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing user id",
        })
        return
    }

    // Check it is same user or an admin one
    err2, usr := authServices.AuthService.GetSessionUser(c)
    if err2 != nil || (int(usr.ID) != int(id) && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Unauthorized",
        })
        return
    }

    // Declare user model
    var user userModels.User

    // Error binding request data to model, 400
    if err := c.BindWith(&user, binding.FormMultipart); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": err.Error(),// "Bad form data",
        })
        return
    }

    // Try to get avatar file from Form, check if got it with no errors
    file, header , err := c.Request.FormFile("avatar")
    if err == nil && file != nil {

        // Get name from data header
        filename := header.Filename

        // TODO: Check if remote ... etc

        // Get local path for file
        filePath := cfg.Config.Assets.Local.Path + "/avatars/" + filename

        // TODO: Check folder exists, create it otherwise
        // Create local file, defer stream release and check for errors
        out, err := os.Create(filePath)
        defer out.Close()
        if err != nil {
            log.Fatal(err)
            c.JSON(http.StatusInternalServerError, gin.H{
                "error": err.Error(),
            })
            return
        }

        // Copy stream to file, check for errors
        _, err = io.Copy(out, file)
        if err != nil {
            log.Fatal(err)
            c.JSON(http.StatusInternalServerError, gin.H{
                "error": err.Error(),
            })
            return
        }

        // Bind created file resource to User Avatar, it should be created by GORM
        user.Avatar = &commonModels.Resource{
            // TODO: this name should come from somewhere else or just remove it
            Name: &filename,
            Type: "image",
            Url: cfg.Config.Assets.Cdn.Url + "/avatars/" + filename,
        }
    }

    // Check if we got password and it's not empty
    updatePassword := user.Password != ""

    // Call service and throw error if any
    httpErr, updatedUser := userServices.UserService.Update(int(id), user, updatePassword)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added user
    c.JSON(http.StatusOK, updatedUser)
}

/*
Delete: Deletes a user
 DELETE /users/:id
 */
func (ct *userController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing user id",
        })
        return
    }

    // Check it is same user or an admin one
    err2, usr := authServices.AuthService.GetSessionUser(c)
    if err2 != nil || (int(usr.ID) != int(id) && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Unauthorized",
        })
        return
    }

    // Call service and throw error if any
    httpErr, user := userServices.UserService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted user
    c.JSON(http.StatusOK, user)
}

/*
Verify: Verifies a user
 GET /user/verify/:token
 */
func (ct *userController) Verify(c *gin.Context) {

    // Get token from parameters
    token := c.Param("token")
    if token == "" || token == "/" {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing user verification token",
        })
        return
    }

    // Get Token from service
    httpErr, tkn := userServices.UserVerificationTokenService.Find(userModels.UserVerificationToken{Token: token})
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Check if already active
    if tkn.User.Active {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "User already activated",
        })
        return
    }

    // Check if token expired, deleting it if any
    if tkn.ExpiresAt != nil && tkn.ExpiresAt.Before(time.Now()) {
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Token expired",
        })
        userServices.UserVerificationTokenService.Delete(int(tkn.ID))
        return
    }

    // Set user as active
    tkn.User.Active = true

    // Call service and throw error if any
    httpErr, user := userServices.UserService.Update(int(tkn.User.ID), *tkn.User, false)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Delete Token from service
    httpErr, _ = userServices.UserVerificationTokenService.Delete(int(tkn.ID))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with verified user
    c.JSON(http.StatusOK, user)
}
