package controllers

import (
    "net/http"

    "github.com/gin-gonic/gin"
)

// userApiTokenController: Private controller struct
type userApiTokenController struct {}

// UserApiTokenController: Controller global variable (sort of singleton)
var UserApiTokenController = userApiTokenController{}

/*
All: Gets all user_verification_tokens
 GET /user_verification_tokens
 */
func (ct *userApiTokenController) All(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Get: Gets a user_verification_token
 GET /user_verification_tokens/:id
 */
func (ct *userApiTokenController) Get(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Create: Creates a new user_verification_token
 POST /user_verification_tokens
 */
func (ct *userApiTokenController) Create(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Update: Updates a user_verification_token
 PUT /user_verification_tokens/:id
 */
func (ct *userApiTokenController) Update(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Delete: mDeletes a user_verification_token
 DELETE /user_verification_tokens/:id
 */
func (ct *userApiTokenController) Delete(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}