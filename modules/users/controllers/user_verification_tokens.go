package controllers

import (
    "net/http"

    "github.com/gin-gonic/gin"
)

// userVerificationTokenController: Private controller struct
type userVerificationTokenController struct {}

// UserVerificationTokenController: Controller global variable (sort of singleton)
var UserVerificationTokenController = userVerificationTokenController{}

/*
All: Gets all user_verification_tokens
 GET /user_verification_tokens
 */
func (ct *userVerificationTokenController) All(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Get: Gets a user_verification_token
 GET /user_verification_tokens/:id
 */
func (ct *userVerificationTokenController) Get(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Create: Creates a new user_verification_token
 POST /user_verification_tokens
 */
func (ct *userVerificationTokenController) Create(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Update: Updates a user_verification_token
 PUT /user_verification_tokens/:id
 */
func (ct *userVerificationTokenController) Update(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}

/*
Delete: mDeletes a user_verification_token
 DELETE /user_verification_tokens/:id
 */
func (ct *userVerificationTokenController) Delete(c *gin.Context) {

    // Return response
    c.JSON(http.StatusMethodNotAllowed, gin.H{
        "error": "Method not allowed",
    })
}