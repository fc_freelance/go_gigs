package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
)

// permissionController: Private controller struct
type permissionController struct {}

// PermissionController: Controller global variable (sort of singleton)
var PermissionController = permissionController{}

/*
All: Gets all permissions
 GET /permissions
 */
func (ct *permissionController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, permissions := userServices.PermissionService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with collection
    c.JSON(http.StatusOK, gin.H{
        "permissions": permissions,
    })
}

/*
Get: Gets a permission
 GET /permissions/:id
 */
func (ct *permissionController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing permission id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, permission := userServices.PermissionService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response
    c.JSON(http.StatusOK, permission)
}

/*
Create: Creates a new permission
 POST /permissions
 */
func (ct *permissionController) Create(c *gin.Context) {

    // Declare role model
    var permission userModels.Permission

    // Error binding request data to model, 400
    if err := c.BindJSON(&permission); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdPermission := userServices.PermissionService.Create(permission)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added model
    c.JSON(http.StatusCreated, createdPermission)
}

/*
Update: Updates a permission
 PUT /permissions/:id
 */
func (ct *permissionController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing permission id",
        })
        return
    }

    // Declare role model
    var permission userModels.Permission

    // Error binding request data to model, 400
    if err := c.BindJSON(&permission); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedPermission := userServices.PermissionService.Update(int(id), permission)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified model
    c.JSON(http.StatusOK, updatedPermission)
}

/*
Delete: Deletes a permission
 DELETE /permissions/:id
 */
func (ct *permissionController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing permission id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, permission := userServices.PermissionService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, permission)
}