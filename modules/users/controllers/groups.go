/*
Package controllers - User module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
)

// groupController: Private controller struct
type groupController struct {}

// GroupController: Controller global variable (sort of singleton)
var GroupController = groupController{}

/*
All: Gets all groups
 GET /groups
 */
func (ct *groupController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, groups := userServices.GroupService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with collection
    c.JSON(http.StatusOK, gin.H{
        "groups": groups,
    })
}

/*
Get: Gets a group
 GET /groups/:id
 */
func (ct *groupController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing group id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, group := userServices.GroupService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response
    c.JSON(http.StatusOK, group)
}

/*
Create: Creates a new group
 POST /groups
 */
func (ct *groupController) Create(c *gin.Context) {

    // Declare role model
    var group userModels.Group

    // Error binding request data to model, 400
    if err := c.BindJSON(&group); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdGroup := userServices.GroupService.Create(group)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added model
    c.JSON(http.StatusCreated, createdGroup)
}

/*
Update:Updates a group
 PUT /groups/:id
 */
func (ct *groupController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing group id",
        })
        return
    }

    // Declare role model
    var group userModels.Group

    // Error binding request data to model, 400
    if err := c.BindJSON(&group); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedGroup := userServices.GroupService.Update(int(id), group)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified model
    c.JSON(http.StatusOK, updatedGroup)
}

/*
Delete: Deletes a group
 DELETE /groups/:id
 */
func (ct *groupController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing group id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, group := userServices.GroupService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, group)
}