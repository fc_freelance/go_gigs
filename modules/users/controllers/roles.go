package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
)

// roleController: Private controller struct
type roleController struct {}

// RoleController: Controller global variable (sort of singleton)
var RoleController = roleController{}

/*
All: Gets all roles
 GET /roles
 */
func (ct *roleController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, roles := userServices.RoleService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with gigs list
    c.JSON(http.StatusOK, gin.H{
        "roles": roles,
    })
}

/*
Get: Gets a role
 GET /roles/:id
 */
func (ct *roleController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing role id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, role := userServices.RoleService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response
    c.JSON(http.StatusOK, role)
}

/*
Create: Creates a new role
 POST /roles
 */
func (ct *roleController) Create(c *gin.Context) {

    // Declare role model
    var role userModels.Role

    // Error binding request data to model, 400
    if err := c.BindJSON(&role); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdRole := userServices.RoleService.Create(role)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added role
    c.JSON(http.StatusCreated, createdRole)
}

/*
Update: Updates a role
 PUT /roles/:id
 */
func (ct *roleController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing role id",
        })
        return
    }

    // Declare role model
    var role userModels.Role

    // Error binding request data to model, 400
    if err := c.BindJSON(&role); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedRole := userServices.RoleService.Update(int(id), role)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified role
    c.JSON(http.StatusOK, updatedRole)
}

/*
Delete: Deletes a role
 DELETE /roles/:id
 */
func (ct *roleController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing role id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, role := userServices.RoleService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, role)
}