package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
)

// roleService: Service private struct
type roleService struct {}

// RoleService: Service global variable (sort of singleton)
var RoleService = roleService{}

/*
All: Gets all models
 */
func (s *roleService) All() (*httpModels.HttpError, *[]userModels.Role) {

    // Return collection or error in repository
    return userRepositories.RoleRepository.All()
}

/*
Get: Gets a model by id
 */
func (s *roleService) Get(id int) (*httpModels.HttpError, *userModels.Role) {

    // Find model or error in repository
    return userRepositories.RoleRepository.Get(id)
}

/*
Find: Finds a model
 */
func (s *roleService) Find(role userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Find model or error in repository
    return userRepositories.RoleRepository.Find(role)
}

/*
Create: Creates a model
 */
func (s *roleService) Create(role userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Return model creation in repo
    return userRepositories.RoleRepository.Create(role)
}

/*
Update: Updates a model
 */
func (s *roleService) Update(id int, role userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Update model or error in repository
    return userRepositories.RoleRepository.Update(id, role)
}

/*
Delete: Deletes a model by id
 */
func (s *roleService) Delete(id int) (*httpModels.HttpError, *userModels.Role) {

    // Delete model or error in repository
    return userRepositories.RoleRepository.Delete(id)
}