package services

import (
    "net/http"

    "gitlab.com/fc_freelance/go_gigs/util"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
    //"log"
)

// userService: Service private struct
type userService struct {}

// UserService: Service global variable (sort of singleton)
var UserService = userService{}

/*
All: Gets all users
 */
func (s *userService) All() (*httpModels.HttpError, *[]userModels.User) {

    // Return collection or error in repository
    return userRepositories.UserRepository.All()
}

/*
Get: Gets a user by id, specifying if we need its permissions or not
 */
func (s *userService) Get(id int, withPermissions bool) (*httpModels.HttpError, *userModels.User) {

    // Find user or error in repository
    return userRepositories.UserRepository.Get(id, withPermissions)
}

/*
Find: Finds a user
 */
func (s *userService) Find(user userModels.User) (*httpModels.HttpError, *userModels.User) {

    // Find user or error in repository
    return userRepositories.UserRepository.Find(user)
}

/*
Create: Creates a User
 */
func (s *userService) Create(user userModels.User, role string) (*httpModels.HttpError, *userModels.User) {

    // If not role, set 'user' as default
    if role == "" || &role == nil {
        role = userModels.Roles.User
    }

    // Ensure not an attempt to create admin role etc.
    if role != userModels.Roles.User && role != userModels.Roles.Expert {
        return &httpModels.HttpError{
            Message: "Unsupported role",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Assign hashed password to user
    user.Password = util.Crypt.GetMD5Hash(user.Password)

    // Get proper role
    err, rol := RoleService.Find(userModels.Role { Code: role })
    if err != nil {
        return &httpModels.HttpError{
            Message: "Unexisting role",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Assign role to user
    user.Roles = []userModels.Role{*rol}

    // Create model or error in repository
    err, createdUser := userRepositories.UserRepository.Create(user)
    if err != nil {
        return err, nil
    }

    // Finally return created user from DB
    return s.Get(int(createdUser.ID), false)
}

/*
Update: Updates a user
 */
func (s *userService) Update(id int, user userModels.User, includePassword bool) (*httpModels.HttpError, *userModels.User) {

    // Assign hashed password to user if included
    if includePassword {
        user.Password = util.Crypt.GetMD5Hash(user.Password)
    }

    // Update model or error in repository
    if  err, _ := userRepositories.UserRepository.Update(id, user); err != nil {
        return err, nil
    }

    // Finally return updated model from DB
    return s.Get(id, false)
}

/*
Delete: Deletes a user by id
 */
func (s *userService) Delete(id int) (*httpModels.HttpError, *userModels.User) {

    // Delete user or error in repository
    return userRepositories.UserRepository.Delete(id)
}

/*
HasPermissions: Checks if a User contains certain permissions
 */
func (s *userService) HasPermissions(user userModels.User, permissionCodes []string) bool {

    // Map permission codes
    userPermissionsCodes := []string{}
    for _, r := range user.Roles {
        for _, p := range r.Permissions {
            userPermissionsCodes = append(userPermissionsCodes, p.Code)
        }
    }

    // Now check permissions
    if util.Util.StringSliceContainsSlice(permissionCodes, userPermissionsCodes) {
        return true
    }

    // Not enough permissions
    return false
}
