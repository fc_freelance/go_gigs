/*
Package services - Users module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
)

// groupService: Service private struct
type groupService struct {}

// GroupService: Service global variable (sort of singleton)
var GroupService = groupService{}

/*
All: Gets all models
 */
func (s *groupService) All() (*httpModels.HttpError, *[]userModels.Group) {

    // Return collection or error in repository
    return userRepositories.GroupRepository.All()
}

/*
Get: Gets a model by id
 */
func (s *groupService) Get(id int) (*httpModels.HttpError, *userModels.Group) {

    // Find model or error in repository
    return userRepositories.GroupRepository.Get(id)
}

/*
Find: Finds a model
 */
func (s *groupService) Find(group userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Find model or error in repository
    return userRepositories.GroupRepository.Find(group)
}

/*
Create: Creates a model
 */
func (s *groupService) Create(group userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Return model creation in repo
    return userRepositories.GroupRepository.Create(group)
}

/*
Update: Updates a model
 */
func (s *groupService) Update(id int, group userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Update model or error in repository
    return userRepositories.GroupRepository.Update(id, group)
}

/*
Delete: Deletes a model by id
 */
func (s *groupService) Delete(id int) (*httpModels.HttpError, *userModels.Group) {

    // Delete model or error in repository
    return userRepositories.GroupRepository.Delete(id)
}