package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
    "gitlab.com/fc_freelance/go_gigs/util"
    "time"
)

// userApiTokenService: Service private struct
type userApiTokenService struct {}

// UserApiTokenService: Service global variable (sort of singleton)
var UserApiTokenService = userApiTokenService{}

/*
All: Gets all models
 */
func (s *userApiTokenService) All() (*httpModels.HttpError, *[]userModels.UserApiToken) {

    // Return collection or error in repository
    return userRepositories.UserApiTokenRepository.All()
}

/*
Get: Gets a model by id
 */
func (s *userApiTokenService) Get(id int) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Find model or error in repository
    return userRepositories.UserApiTokenRepository.Get(id)
}

/*
Find: Finds a model
 */
func (s *userApiTokenService) Find(token userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Find model or error in repository
    return userRepositories.UserApiTokenRepository.Find(token)
}

/*
Create: Creates a model
 */
func (s *userApiTokenService) Create(token userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold error from start
    httpErr := &httpModels.HttpError{Message: "Could not generate token"}

    // Calculate expiration
    expiration := time.Now().Add(time.Duration(24) * time.Hour)

    // Assign expiration
    token.ExpiresAt = &expiration

    // Hold final token in var
    var tkn *userModels.UserApiToken

    // While we've an error ... and no surpass attempts limit
    for i := 0; httpErr != nil && i < 10; i++ {

        // Generate a token
        token.Token = util.Crypt.RandomToken(32)

        // Attempt to store the token in DB
        httpErr, tkn = userRepositories.UserApiTokenRepository.Create(token)
    }

    // Return model creation in repo
    return httpErr, tkn
}

/*
Update: Updates a model
 */
func (s *userApiTokenService) Update(id int, token userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Update model or error in repository
    return userRepositories.UserApiTokenRepository.Update(id, token)
}

/*
Delete: Deletes a model by id
 */
func (s *userApiTokenService) Delete(id int) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Delete model or error in repository
    return userRepositories.UserApiTokenRepository.Delete(id)
}