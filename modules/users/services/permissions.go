package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
)

// permissionService: Service private struct
type permissionService struct {}

// PermissionService: Service global variable (sort of singleton)
var PermissionService = permissionService{}

/*
All: Gets all models
 */
func (s *permissionService) All() (*httpModels.HttpError, *[]userModels.Permission) {

    // Return collection or error in repository
    return userRepositories.PermissionRepository.All()
}

/*
Get: Gets a model by id
 */
func (s *permissionService) Get(id int) (*httpModels.HttpError, *userModels.Permission) {

    // Find model or error in repository
    return userRepositories.PermissionRepository.Get(id)
}

/*
Find: Finds a model
 */
func (s *permissionService) Find(permission userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Find model or error in repository
    return userRepositories.PermissionRepository.Find(permission)
}

/*
Create: Creates a model
 */
func (s *permissionService) Create(permission userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Return model creation in repo
    return userRepositories.PermissionRepository.Create(permission)
}

/*
Update: Updates a model
 */
func (s *permissionService) Update(id int, permission userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Update model or error in repository
    return userRepositories.PermissionRepository.Update(id, permission)
}

/*
Delete: Deletes a model by id
 */
func (s *permissionService) Delete(id int) (*httpModels.HttpError, *userModels.Permission) {

    // Delete model or error in repository
    return userRepositories.PermissionRepository.Delete(id)
}