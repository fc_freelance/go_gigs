package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userRepositories "gitlab.com/fc_freelance/go_gigs/modules/users/repositories"
    "gitlab.com/fc_freelance/go_gigs/util"
    "time"
)

// userVerificationTokenService: Service private struct
type userVerificationTokenService struct {}

// UserVerificationTokenService: Service global variable (sort of singleton)
var UserVerificationTokenService = userVerificationTokenService{}

/*
All: Gets all models
 */
func (s *userVerificationTokenService) All() (*httpModels.HttpError, *[]userModels.UserVerificationToken) {

    // Return collection or error in repository
    return userRepositories.UserVerificationTokenRepository.All()
}

/*
Get: Gets a model by id
 */
func (s *userVerificationTokenService) Get(id int) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Find model or error in repository
    return userRepositories.UserVerificationTokenRepository.Get(id)
}

/*
Find: Finds a model
 */
func (s *userVerificationTokenService) Find(token userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Find model or error in repository
    return userRepositories.UserVerificationTokenRepository.Find(token)
}

/*
Create: Creates a model
 */
func (s *userVerificationTokenService) Create(token userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold error from start
    httpErr := &httpModels.HttpError{Message: "Could not generate token"}

    // Calculate expiration
    expiration := time.Now().Add(time.Duration(24) * time.Hour)

    // Assign expiration
    token.ExpiresAt = &expiration

    // Hold final token in var
    var tkn *userModels.UserVerificationToken

    // While we've an error ... and no surpass attempts limit
    for i := 0; httpErr != nil && i < 10; i++ {

        // Generate a token
        token.Token = util.Crypt.RandomToken(8)

        // Attempt to store the token in DB
        httpErr, tkn = userRepositories.UserVerificationTokenRepository.Create(token)
    }

    // Return model creation in repo
    return httpErr, tkn
}

/*
Update: Updates a model
 */
func (s *userVerificationTokenService) Update(id int, token userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Update model or error in repository
    return userRepositories.UserVerificationTokenRepository.Update(id, token)
}

/*
Delete: Deletes a model by id
 */
func (s *userVerificationTokenService) Delete(id int) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Delete model or error in repository
    return userRepositories.UserVerificationTokenRepository.Delete(id)
}