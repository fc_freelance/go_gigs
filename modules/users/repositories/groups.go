/*
Package repositories - Users module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// groupRepository: Repository private struct
type groupRepository struct {}

// GroupRepository: Repository global variable (sort of singleton)
var GroupRepository = groupRepository{}

/*
All: Gets all models
 */
func (r *groupRepository) All() (*httpModels.HttpError, *[]userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    groups := []userModels.Group{}

    // Run query and handle error
    err := db.DB.Find(&groups).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &groups
}

/*
Get: Gets a model by id
 */
func (r *groupRepository) Get(id int) (*httpModels.HttpError, *userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for model
    group := userModels.Group{}

    // Run query and handle error
    err := db.DB.First(&group, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return model
    return nil, &group
}

/*
Find: Finds a model
 */
func (r *groupRepository) Find(grp userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold model
    var group userModels.Group

    // Run query and handle error
    err := db.DB.Where(&grp).First(&group).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return model
    return nil, &group
}

/*
Create: Creates a model
 */
func (r *groupRepository) Create(group userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create group",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(group)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Group already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err := db.DB.Create(&group).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &group
}

/*
Update: Updates a model
 */
func (r *groupRepository) Update(id int, group userModels.Group) (*httpModels.HttpError, *userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update group",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched model
    var grp userModels.Group

    // Create model in DB, handle error
    err := db.DB.First(&grp, id).Updates(group).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &grp
}

/*
Delete: Deletes a model
 */
func (r *groupRepository) Delete(id int) (*httpModels.HttpError, *userModels.Group) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete group",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold model
    var group userModels.Group

    // Create model in DB, handle error
    err := db.DB.First(&group, id).Delete(&group).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &group
}