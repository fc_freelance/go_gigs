package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// permissionRepository: Repository private struct
type permissionRepository struct {}

// PermissionRepository: Repository global variable (sort of singleton)
var PermissionRepository = permissionRepository{}

/*
All: Gets all models
 */
func (r *permissionRepository) All() (*httpModels.HttpError, *[]userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    permissions := []userModels.Permission{}

    // Run query and handle error
    err := db.DB.Find(&permissions).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &permissions
}

/*
Get: Gets a model by id
 */
func (r *permissionRepository) Get(id int) (*httpModels.HttpError, *userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for model
    permission := userModels.Permission{}

    // Run query and handle error
    err := db.DB.First(&permission, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return model
    return nil, &permission
}

/*
Find: Finds a model
 */
func (r *permissionRepository) Find(pms userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold model
    var permission userModels.Permission

    // Run query and handle error
    err := db.DB.Where(&pms).First(&permission).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return model
    return nil, &permission
}

/*
Create: Creates a model
 */
func (r *permissionRepository) Create(permission userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create permission",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(permission)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Permission already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err := db.DB.Create(&permission).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &permission
}

/*
Update: Updates a model
 */
func (r *permissionRepository) Update(id int, permission userModels.Permission) (*httpModels.HttpError, *userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update permission",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched model
    var prm userModels.Permission

    // Create model in DB, handle error
    err := db.DB.First(&prm, id).Updates(permission).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &prm
}

/*
Delete: Deletes a model
 */
func (r *permissionRepository) Delete(id int) (*httpModels.HttpError, *userModels.Permission) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete group",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold model
    var permission userModels.Permission

    // Create model in DB, handle error
    err := db.DB.First(&permission, id).Delete(&permission).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &permission
}