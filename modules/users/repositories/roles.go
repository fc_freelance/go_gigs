package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// roleRepository: Repository private struct
type roleRepository struct {}

// RoleRepository: Repository global variable (sort of singleton)
var RoleRepository = roleRepository{}

/*
All: Gets all models
 */
func (r *roleRepository) All() (*httpModels.HttpError, *[]userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    roles := []userModels.Role{}

    // Run query and handle error
    err := db.DB.Preload("Permissions").Find(&roles).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &roles
}

/*
Get: Gets a model by id
 */
func (r *roleRepository) Get(id int) (*httpModels.HttpError, *userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    role := userModels.Role{}

    // Run query and handle error
    err := db.DB.Preload("Permissions").First(&role, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &role
}

/*
Find: Finds a model
 */
func (r *roleRepository) Find(rl userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold user
    var role userModels.Role

    // Run query and handle error
    err := db.DB.Preload("Permissions").Where(&rl).First(&role).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &role
}

/*
Create: Creates a Role
 */
func (r *roleRepository) Create(role userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create role",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(role)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Role already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create role in DB, handle error
    err := db.DB.Create(&role).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &role
}

/*
Update: Updates a model
 */
func (r *roleRepository) Update(id int, role userModels.Role) (*httpModels.HttpError, *userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update role",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched user
    var rol userModels.Role

    // Create user in DB, handle error
    err := db.DB.Preload("Permissions").First(&rol, id).Updates(role).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &rol
}

/*
Delete: Deletes a model
 */
func (r *roleRepository) Delete(id int) (*httpModels.HttpError, *userModels.Role) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete role",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold user
    var role userModels.Role

    // Create user in DB, handle error
    err := db.DB.First(&role, id).Delete(&role).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &role
}