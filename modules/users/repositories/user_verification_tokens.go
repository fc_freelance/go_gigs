package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// userVerificationTokenRepository: Repository private struct
type userVerificationTokenRepository struct {}

// UserVerificationTokenRepository: Repository global variable (sort of singleton)
var UserVerificationTokenRepository = userVerificationTokenRepository{}

/*
All: Gets all tokens
 */
func (r *userVerificationTokenRepository) All() (*httpModels.HttpError, *[]userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    tokens := []userModels.UserVerificationToken{}

    // Run query and handle error
    err := db.DB.Find(&tokens).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &tokens
}

/*
Get: Gets a token by id
 */
func (r *userVerificationTokenRepository) Get(id int) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    token := userModels.UserVerificationToken{}

    // Run query and handle error
    err := db.DB.First(&token, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &token
}

/*
Find: Finds a token
 */
func (r *userVerificationTokenRepository) Find(tkn userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold user
    var token userModels.UserVerificationToken

    // Run query and handle error
    err := db.DB.Preload("User").Where(&tkn).First(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &token
}

/*
Create: Creates a token
 */
func (r *userVerificationTokenRepository) Create(token userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create token",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(token)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Token already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err := db.DB.Create(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &token
}

/*
Update: Updates a token
 */
func (r *userVerificationTokenRepository) Update(id int, token userModels.UserVerificationToken) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update token",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched user
    var tkn userModels.UserVerificationToken

    // Create user in DB, handle error
    err := db.DB.First(&tkn, id).Updates(token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &tkn
}

/*
Delete: Deletes a token
 */
func (r *userVerificationTokenRepository) Delete(id int) (*httpModels.HttpError, *userModels.UserVerificationToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete token",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold user
    var token userModels.UserVerificationToken

    // Create user in DB, handle error
    err := db.DB.First(&token, id).Delete(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &token
}