package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// userApiTokenRepository: Repository private struct
type userApiTokenRepository struct {}

// UserApiTokenRepository: Repository global variable (sort of singleton)
var UserApiTokenRepository = userApiTokenRepository{}

/*
All: Gets all tokens
 */
func (r *userApiTokenRepository) All() (*httpModels.HttpError, *[]userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    tokens := []userModels.UserApiToken{}

    // Run query and handle error
    err := db.DB.Find(&tokens).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &tokens
}

/*
Get: Gets a token by id
 */
func (r *userApiTokenRepository) Get(id int) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    token := userModels.UserApiToken{}

    // Run query and handle error
    err := db.DB.First(&token, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &token
}

/*
Find: Finds a token
 */
func (r *userApiTokenRepository) Find(tkn userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold user
    var token userModels.UserApiToken

    // Run query and handle error
    err := db.DB.Preload("User").Where(&tkn).First(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &token
}

/*
Create: Creates a token
 */
func (r *userApiTokenRepository) Create(token userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create token",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(token)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Token already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err := db.DB.Create(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &token
}

/*
Update: Updates a token
 */
func (r *userApiTokenRepository) Update(id int, token userModels.UserApiToken) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update token",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched user
    var tkn userModels.UserApiToken

    // Create user in DB, handle error
    err := db.DB.First(&tkn, id).Updates(token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &tkn
}

/*
Delete: Deletes a token
 */
func (r *userApiTokenRepository) Delete(id int) (*httpModels.HttpError, *userModels.UserApiToken) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete token",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold user
    var token userModels.UserApiToken

    // Create user in DB, handle error
    err := db.DB.First(&token, id).Delete(&token).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &token
}