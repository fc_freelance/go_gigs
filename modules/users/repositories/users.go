package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

// userRepository: Repository private struct
type userRepository struct {}

// UserRepository: Repository global variable (sort of singleton)
var UserRepository = userRepository{}

/*
All: Gets all models
 */
func (r *userRepository) All() (*httpModels.HttpError, *[]userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    users := []userModels.User{}

    // Run query and handle error
    err := db.DB.
        Preload("Groups").
        Preload("Roles").
        Preload("Language").
        Preload("Currency").
        Preload("Address").
        Preload("Avatar").
        Preload("SpokenLanguages").
        Preload("BillingAddresses").
        Find(&users).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &users
}

/*
Get: Gets a model by id, specifying if we need its permissions or not
 */
func (r *userRepository) Get(id int, withPermissions bool) (*httpModels.HttpError, *userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    user := userModels.User{}
    
    // Roles preload string
    rolesPreloadStr := "Roles"
    
    // Check if we want permissions, add them if any to string
    if (withPermissions) {
        rolesPreloadStr += ".Permissions"
    }

    // Run query and handle error
    err := db.DB.
        Preload("Groups").
        Preload(rolesPreloadStr).
        Preload("Language").
        Preload("Currency").
        Preload("Address").
        Preload("Avatar").
        Preload("SpokenLanguages").
        Preload("BillingAddresses").
        First(&user, id).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &user
}

/*
Find: Finds a model
 */
func (r *userRepository) Find(usr userModels.User) (*httpModels.HttpError, *userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold user
    var user userModels.User
    log.Println(usr.Email, usr.Password)
    // Run query and handle error
    err := db.DB.
        Preload("Groups").
        Preload("Roles").
        Preload("Language").
        Preload("Currency").
        Preload("Address").
        Preload("Avatar").
        Preload("SpokenLanguages").
        Preload("BillingAddresses").
        Where(&usr).
        First(&user).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &user
}

/*
Create: Creates a model
 */
func (r *userRepository) Create(user userModels.User) (*httpModels.HttpError, *userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create user",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(user)
    if !isNew {
        return &httpModels.HttpError{
            Message: "User already exists?",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Check user existence with that username
    existingErr, existingUser := r.Find(userModels.User{Username: user.Username})
    if existingErr != nil && existingErr.StatusCode != 404 {
        return &defaultError, nil
    }
    if existingUser != nil {
        return &httpModels.HttpError{
            Message: "Username already taken",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Check user existence with that email
    existingErr, existingUser = r.Find(userModels.User{Email: user.Email})
    if existingErr != nil && existingErr.StatusCode != 404 {
        return &defaultError, nil
    }
    if existingUser != nil {
        return &httpModels.HttpError{
            Message: "Email already taken",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err := db.DB.Create(&user).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &user
}

/*
Update: Updates a model
 */
func (r *userRepository) Update(id int, user userModels.User) (*httpModels.HttpError, *userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update user",
        StatusCode: http.StatusInternalServerError,
    }

    // Check user existence with that username
    existingErr, existingUser := r.Find(userModels.User{Username: user.Username})
    if existingErr != nil && existingErr.StatusCode != 404 {
        return &defaultError, nil
    }
    if existingUser != nil && int(existingUser.ID) != id{
        return &httpModels.HttpError{
            Message: "Username already taken",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Check user existence with that email
    existingErr, existingUser = r.Find(userModels.User{Email: user.Email})
    if existingErr != nil && existingErr.StatusCode != 404 {
        return &defaultError, nil
    }
    if existingUser != nil && int(existingUser.ID) != id {
        return &httpModels.HttpError{
            Message: "Email already taken",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Just make sure it's active at this point, not allowed to override that
    // TODO: Change this if we plan to have user administration without directly doing it in DB
    user.Active = true

    // Hold fetched user
    var usr userModels.User

    // Create user in DB, handle error
    err := db.DB.
        Preload("Groups").
        Preload("Roles").
        Preload("Language").
        Preload("Currency").
        Preload("Address").
        Preload("Avatar").
        Preload("SpokenLanguages").
        Preload("BillingAddresses").
        First(&usr, id).
        // FIXME: WTF?
        Omit("Roles").
        Updates(user).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &usr
}

/*
Delete: Deletes a model
 */
func (r *userRepository) Delete(id int) (*httpModels.HttpError, *userModels.User) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete user",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold user
    var user userModels.User

    // Create user in DB, handle error
    err := db.DB.
        Preload("Groups").
        Preload("Roles").
        Preload("Language").
        Preload("Currency").
        Preload("Address").
        Preload("Avatar").
        Preload("SpokenLanguages").
        Preload("BillingAddresses").
        First(&user, id).
        Delete(&user).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &user
}
