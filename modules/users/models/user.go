package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

/*
User: User model global struct
 */
type User struct {
    dbModels.DbBaseModel
    Username string `form:"username" json:"username,omitempty"`
    Email string `form:"email" json:"email,omitempty"`
    Password string `form:"password" json:"-"`
    FirstName string `form:"first_name" json:"first_name,omitempty"`
    LastName string `form:"last_name" json:"last_name,omitempty"`
    Mobile string `form:"mobile" json:"mobile,omitempty"`
    Active bool `json:"-"`
    InVacation bool `form:"in_vacation" json:"in_vacation"`
    Groups []Group `json:"groups,omitempty" gorm:"many2many:user_groups"`
    Roles []Role `json:"roles,omitempty" gorm:"many2many:user_roles"`
    UserVerificationTokens []UserVerificationToken `json:"user_verification_tokens,omitempty"`
    LanguageID *uint `form:"language_id" json:"language_id,omitempty" gorm:"default:NULL"`
    Language *commonModels.Language `json:"language,omitempty"`
    AddressID *uint `form:"address_id" json:"address_id,omitempty" gorm:"default:NULL"`
    Address *commonModels.Address `json:"address,omitempty"`
    AvatarID *uint `form:"avatar_id" json:"avatar_id,omitempty" gorm:"default:NULL"`
    Avatar *commonModels.Resource `json:"avatar,omitempty"`
    CurrencyID *uint `form:"currency_id" json:"currency_id,omitempty" gorm:"default:NULL"`
    Currency *commonModels.Currency `json:"currency,omitempty"`
    SpokenLanguages []commonModels.Language `json:"spoken_languages,omitempty" gorm:"many2many:user_spoken_languages"`
    BillingAddresses []commonModels.Address `json:"billing_addresses,omitempty" gorm:"many2many:user_billing_addresses"`
}
