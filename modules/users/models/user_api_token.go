package models

import (
    "time"
)

/*
UserApiToken: UserApiToken model global struct
 */
type UserApiToken struct {
    ID uint `gorm:"primary_key" json:"-"`
    Token string `json:"token"`
    ExpiresAt *time.Time `json:"expires_at,omitempty"`
    CreatedAt time.Time `json:"-"`
    UserID uint `json:"user_id" binding:"required"`
    User *User `json:"-"`
}
