package models

/*
SignUp: SignUp model global struct
 */
type SignUp struct {
    User
    Username string  `form:"username" json:"username" binding:"required"`
    Email string  `form:"email" json:"email" binding:"required"`
    Password string `form:"password" json:"password" binding:"required"`
    Role string `form:"role" json:"role"`
}
