package models

/*
OptionsConfig: API config options model global struct
 */
type OptionsConfig struct {
    SendSignupVerifyMail bool `json:"send_signup_verify_mail" default:"false"`
    SignupVerifyUrl string `json:"signup_verify_url" default:"http://localhost/user/verify/"`
}
