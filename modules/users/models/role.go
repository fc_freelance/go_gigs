package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Role: Role model global struct
 */
type Role struct {
    dbModels.DbBaseModel
    Code string `form:"code" json:"code" binding:"required"`
    Name *string `form:"name" json:"name,omitempty"`
    Description *string `form:"description" json:"description,omitempty"`
    Permissions []Permission `json:"permissions,omitempty" gorm:"many2many:role_permissions"`
    Groups []Permission `json:"groups,omitempty" gorm:"many2many:group_roles"`
    Users []Permission `json:"users,omitempty" gorm:"many2many:user_roles"`
}
