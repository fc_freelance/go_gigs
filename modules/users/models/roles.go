package models

/*
Role: Roles private struct
 */
type roles struct {
    User string
    Expert string
    Admin string
}

/*
Roles: Roles global variable
 */
var Roles = roles {
    User: "user",
    Expert: "expert",
    Admin: "admin",
}