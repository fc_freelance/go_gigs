/*
Package models - User module models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Group: Group model global struct
 */
type Group struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Description *string  `form:"description" json:"description,omitempty"`
    Users []User `json:"users,omitempty" gorm:"many2many:user_groups"`
    Roles []Role `json:"roles,omitempty" gorm:"many2many:group_roles"`
}
