package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Permission: Permission model global struct
 */
type Permission struct {
    dbModels.DbBaseModel
    Code string `form:"code" json:"code" binding:"required"`
    Name *string `form:"name" json:"name,omitempty"`
    Description *string `form:"description" json:"description,omitempty"`
    Roles []Role `json:"roles,omitempty" gorm:"many2many:role_permissions"`
}
