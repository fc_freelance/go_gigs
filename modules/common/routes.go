/*
Package common - Manages everything related to common entities
 */
package common

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
    commonControllers "gitlab.com/fc_freelance/go_gigs/modules/common/controllers"
)

/*
Routes Global routes variable
 */
var Routes = []httpModels.Route{

    /******************************************************************************************/
    /**************************************** CATEGORIES **************************************/
    /******************************************************************************************/

    /**
     * @apidefine CategoryRequestBody
     *
     * @apiParam (Request body) {String} name Category name.
     * @apiParam (Request body) {String} [description] Category description.
     * @apiParam (Request body) {Number} [parent_id] Parent Category id.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "name": "My awesome category",
     *       "description": "my fancy new category",
     *       "parent_id": 123
     *     }
     */

    /**
     * @apidefine CategoryPayload
     *
     * @apiSuccess (Success response payload) {Number} id Category id.
     * @apiSuccess (Success response payload) {String} name Category name.
     * @apiSuccess (Success response payload) {String} description Category description.
     * @apiSuccess (Success response payload) {Object} parent Parent Category object.
     * @apiSuccess (Success response payload) {Object[]} subcategories Subcategories this category has.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome category",
     *       "description": "my fancy new category",
     *       "parent": {...},
     *       "subcategories": [...]
     *     }
     */

    /**
     * @apidefine CategoryCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Category id.
     * @apiSuccess (Success response payload) {String} name Category name.
     * @apiSuccess (Success response payload) {String} description Category description.
     * @apiSuccess (Success response payload) {Object} parent Parent Category object.
     * @apiSuccess (Success response payload) {Object[]} subcategories Subcategories this category has.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome category",
     *       "description": "my fancy new category",
     *       "parent": {...},
     *       "subcategories": [...]
     *     }
     */

    /**
     * @apidefine CategoriesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Category objects.
     * @apiSuccess (Success response payload) {Number} _.id Category id.
     * @apiSuccess (Success response payload) {String} _.name Category name.
     * @apiSuccess (Success response payload) {String} _.description Category description.
     * @apiSuccess (Success response payload) {Object} _.parent Parent Category object.
     * @apiSuccess (Success response payload) {Object[]} _.subcategories Subcategories this category has.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "My awesome category",
     *         "description": "my fancy new category",
     *         "parent": {...},
     *         "subcategories": [...]
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /categories Get All Categories
     * @apiName GetCategories
     * @apiGroup Categories
     * @apiVersion 1.0.0
     *
     * @apiUse CategoriesPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/categories",
        Permissions: nil,
        Handler: commonControllers.CategoryController.All,
    },

    /**
     * @api {get} /categories/:id Get Category
     * @apiName GetCategory
     * @apiGroup Categories
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Category unique id.
     *
     * @apiUse CategoryPayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/categories/:id",
        Permissions: nil,
        Handler: commonControllers.CategoryController.Get,
    },

    /**
     * @api {post} /categories Create Category
     * @apiName PostCategory
     * @apiGroup Categories
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse CategoryRequestBody
     *
     * @apiUse CategoryCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/categories",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CategoryController.Create,
    },

    /**
     * @api {put} /categories/:id Update Category
     * @apiName PutCategory
     * @apiGroup Categories
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Category unique id.
     *
     * @apiUse CategoryRequestBody
     *
     * @apiUse CategoryPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/categories/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CategoryController.Update,
    },

    /**
     * @api {delete} /categories/:id Delete Category
     * @apiName DeleteCategory
     * @apiGroup Categories
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Category unique id.
     *
     * @apiUse CategoryPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/categories/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CategoryController.Delete,
    },


    /******************************************************************************************/
    /**************************************** LOCATIONS ***************************************/
    /******************************************************************************************/

    /**
     * @apidefine LocationRequestBody
     *
     * @apiParam (Request body) {String} name Location name.
     * @apiParam (Request body) {String} code Location code.
     * @apiParam (Request body) {Number} [lat] Location latitude coordinate.
     * @apiParam (Request body) {Number} [lon] Location longitude coordinate.
     * @apiParam (Request body) {Number} [parent_id] Location parent location id.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "name": "My awesome location",
     *       "code": "LC",
     *       "lat": 30.23,
     *       "lon": -2.001,
     *       "parent_id": 12
     *     }
     */

    /**
     * @apidefine LocationPayload
     *
     * @apiSuccess (Success response payload) {Number} id Location id.
     * @apiSuccess (Success response payload) {String} name Location name.
     * @apiSuccess (Success response payload) {String} code Location code.
     * @apiSuccess (Success response payload) {Number} lat Location latitude coordinate.
     * @apiSuccess (Success response payload) {Number} lon Location longitude coordinate.
     * @apiSuccess (Success response payload) {Object} parent Location this location belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome location",
     *       "code": "LC",
     *       "lat": 30.23,
     *       "lon": -2.001,
     *       "parent": {...}
     *     }
     */

    /**
     * @apidefine LocationCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Location id.
     * @apiSuccess (Success response payload) {String} name Location name.
     * @apiSuccess (Success response payload) {String} code Location code.
     * @apiSuccess (Success response payload) {Number} lat Location latitude coordinate.
     * @apiSuccess (Success response payload) {Number} lon Location longitude coordinate.
     * @apiSuccess (Success response payload) {Object} parent Location this location belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome location",
     *       "code": "LC",
     *       "lat": 30.23,
     *       "lon": -2.001,
     *       "parent": {...}
     *     }
     */

    /**
     * @apidefine LocationsPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Location objects.
     * @apiSuccess (Success response payload) {Number} _.id Location id.
     * @apiSuccess (Success response payload) {String} _.name Location name.
     * @apiSuccess (Success response payload) {String} _.code Location code.
     * @apiSuccess (Success response payload) {Number} _.lat Location latitude coordinate.
     * @apiSuccess (Success response payload) {Number} _.lon Location longitude coordinate.
     * @apiSuccess (Success response payload) {Object} _.parent Location this location belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "My awesome location",
     *         "code": "LC",
     *         "lat": 30.23,
     *         "lon": -2.001,
     *         "parent": {...}
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /locations Get All Locations
     * @apiName GetLocations
     * @apiGroup Locations
     * @apiVersion 1.0.0
     *
     * @apiUse LocationsPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/locations",
        Permissions: nil,
        Handler: commonControllers.LocationController.All,
    },

    /**
     * @api {get} /locations/:id Get Location
     * @apiName GetLocation
     * @apiGroup Locations
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Location unique id.
     *
     * @apiUse LocationPayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/locations/:id",
        Permissions: nil,
        Handler: commonControllers.LocationController.Get,
    },

    /**
     * @api {post} /locations Create Location
     * @apiName PostLocation
     * @apiGroup Locations
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse LocationRequestBody
     *
     * @apiUse LocationCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/locations",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LocationController.Create,
    },

    /**
     * @api {put} /locations/:id Update Location
     * @apiName PutLocation
     * @apiGroup Locations
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Location unique id.
     *
     * @apiUse LocationRequestBody
     *
     * @apiUse LocationPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/locations/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LocationController.Update,
    },

    /**
     * @api {delete} /locations/:id Delete Location
     * @apiName DeleteLocation
     * @apiGroup Locations
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Location unique id.
     *
     * @apiUse LocationPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/locations/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LocationController.Delete,
    },



    /******************************************************************************************/
    /**************************************** CURRENCIES **************************************/
    /******************************************************************************************/

    /**
     * @apidefine CurrencyRequestBody
     *
     * @apiParam (Request body) {String} name Currency name.
     * @apiParam (Request body) {String} code Currency code.
     * @apiParam (Request body) {String} symbol Currency symbol.
     * @apiParam (Request body) {Number} conversion Currency conversion.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "name": "My awesome currency",
     *       "code": "DL",
     *       "symbol": "$",
     *       "conversion": 1.00
     *     }
     */

    /**
     * @apidefine CurrencyPayload
     *
     * @apiSuccess (Success response payload) {Number} id Currency id.
     * @apiSuccess (Success response payload) {String} name Currency name.
     * @apiSuccess (Success response payload) {String} code Currency code.
     * @apiSuccess (Success response payload) {String} symbol Currency symbol.
     * @apiSuccess (Success response payload) {Number} conversion Currency conversion.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome currency",
     *       "code": "DL",
     *       "symbol": "$",
     *       "conversion": 1.00
     *     }
     */

    /**
     * @apidefine CurrencyCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Currency id.
     * @apiSuccess (Success response payload) {String} name Currency name.
     * @apiSuccess (Success response payload) {String} code Currency code.
     * @apiSuccess (Success response payload) {String} symbol Currency symbol.
     * @apiSuccess (Success response payload) {Number} conversion Currency conversion.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "My awesome currency",
     *       "code": "DL",
     *       "symbol": "$",
     *       "conversion": 1.00
     *     }
     */

    /**
     * @apidefine CurrenciesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Currency objects.
     * @apiSuccess (Success response payload) {Number} _.id Currency id.
     * @apiSuccess (Success response payload) {String} _.name Currency name.
     * @apiSuccess (Success response payload) {String} _.code Currency code.
     * @apiSuccess (Success response payload) {String} _.symbol Currency symbol.
     * @apiSuccess (Success response payload) {Number} _.conversion Currency conversion.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "My awesome currency",
     *         "code": "DL",
     *         "symbol": "$",
     *         "conversion": 1.00
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /currencies Get All Currencies
     * @apiName GetCurrencies
     * @apiGroup Currencies
     * @apiVersion 1.0.0
     *
     * @apiUse CurrenciesPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/currencies",
        Permissions: nil,
        Handler: commonControllers.CurrencyController.All,
    },

    /**
     * @api {get} /currencies/:id Get Currency
     * @apiName GetCurrency
     * @apiGroup Currencies
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Currency unique id.
     *
     * @apiUse CurrencyPayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/currencies/:id",
        Permissions: nil,
        Handler: commonControllers.CurrencyController.Get,
    },

    /**
     * @api {post} /currencies Create Currency
     * @apiName PostCurrency
     * @apiGroup Currencies
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse LocationRequestBody
     *
     * @apiUse CurrencyCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/currencies",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CurrencyController.Create,
    },

    /**
     * @api {put} /currencies/:id Update Currency
     * @apiName PutCurrency
     * @apiGroup Currencies
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Currency unique id.
     *
     * @apiUse CurrencyRequestBody
     *
     * @apiUse CurrencyPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/currencies/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CurrencyController.Update,
    },

    /**
     * @api {delete} /currencies/:id Delete Currency
     * @apiName DeleteCurrency
     * @apiGroup Currencies
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Currency unique id.
     *
     * @apiUse CurrencyPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/currencies/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.CurrencyController.Delete,
    },


    /******************************************************************************************/
    /**************************************** ADDRESSES ***************************************/
    /******************************************************************************************/

    /**
     * @apidefine AddressRequestBody
     *
     * @apiParam (Request body) {Number} number Address number.
     * @apiParam (Request body) {String} street Address street.
     * @apiParam (Request body) {String} postal_code Address postal code.
     * @apiParam (Request body) {String} extra_info Address extra information.
     * @apiParam (Request body) {Number} location_id Address location id.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "number": 27,
     *       "street": "Brick Lane",
     *       "postal_code": "E15DY",
     *       "extra_info": "The red door in 2nd floor,
     *       "location_id": 12
     *     }
     */

    /**
     * @apidefine AddressPayload
     *
     * @apiSuccess (Success response payload) {Number} id Address id.
     * @apiSuccess (Success response payload) {Number} number Address number.
     * @apiSuccess (Success response payload) {String} street Address street.
     * @apiSuccess (Success response payload) {String} postal_code Address postal code.
     * @apiSuccess (Success response payload) {String} extra_info Address extra information.
     * @apiSuccess (Success response payload) {Object} location Location this address belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "number": 27,
     *       "street": "Brick Lane",
     *       "postal_code": "E15DY",
     *       "extra_info": "The red door in 2nd floor,
     *       "location": {...}
     *     }
     */

    /**
     * @apidefine AddressCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Address id.
     * @apiSuccess (Success response payload) {Number} number Address number.
     * @apiSuccess (Success response payload) {String} street Address street.
     * @apiSuccess (Success response payload) {String} postal_code Address postal code.
     * @apiSuccess (Success response payload) {String} extra_info Address extra information.
     * @apiSuccess (Success response payload) {Object} location Location this address belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "number": 27,
     *       "street": "Brick Lane",
     *       "postal_code": "E15DY",
     *       "extra_info": "The red door in 2nd floor,
     *       "location": {...}
     *     }
     */

    /**
     * @apidefine AddressesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Address objects.
     * @apiSuccess (Success response payload) {Number} _.id Address id.
     * @apiSuccess (Success response payload) {Number} _.number Address number.
     * @apiSuccess (Success response payload) {String} _.street Address street.
     * @apiSuccess (Success response payload) {String} _.postal_code Address postal code.
     * @apiSuccess (Success response payload) {String} _.extra_info Address extra information.
     * @apiSuccess (Success response payload) {Object} _.location Location this address belongs to.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "number": 27,
     *         "street": "Brick Lane",
     *         "postal_code": "E15DY",
     *         "extra_info": "The red door in 2nd floor,
     *         "location": {...}
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /addresses Get All Addresses
     * @apiName GetAddresses
     * @apiGroup Addresses
     * @apiVersion 1.0.0
     *
     * @apiUse AddressesPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/addresses",
        Permissions: nil,
        Handler: commonControllers.AddressController.All,
    },

    /**
     * @api {get} /addresses/:id Get Address
     * @apiName GetAddress
     * @apiGroup Addresses
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Address unique id.
     *
     * @apiUse AddressPayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/addresses/:id",
        Permissions: nil,
        Handler: commonControllers.AddressController.Get,
    },

    /**
     * @api {post} /addresses Create Address
     * @apiName PostAddress
     * @apiGroup Addresses
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse AddressRequestBody
     *
     * @apiUse AddressCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/addresses",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.AddressController.Create,
    },

    /**
     * @api {put} /addresses/:id Update Address
     * @apiName PutAddress
     * @apiGroup Addresses
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Address unique id.
     *
     * @apiUse AddressRequestBody
     *
     * @apiUse AddressPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/addresses/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.AddressController.Update,
    },

    /**
     * @api {delete} /addresses/:id Delete Address
     * @apiName DeleteAddress
     * @apiGroup Addresses
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Address unique id.
     *
     * @apiUse AddressPayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/addresses/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.AddressController.Delete,
    },


    /******************************************************************************************/
    /**************************************** LANGUAGES ***************************************/
    /******************************************************************************************/

    /**
     * @apidefine LanguageRequestBody
     *
     * @apiParam (Request body) {String} name Language name.
     * @apiParam (Request body) {String} code Language code.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "name": "Deutsch",
     *       "code": "DE"
     *     }
     */

    /**
     * @apidefine LanguagePayload
     *
     * @apiSuccess (Success response payload) {Number} id Language id.
     * @apiSuccess (Success response payload) {String} name Language name.
     * @apiSuccess (Success response payload) {String} code Language code.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "name": "Deutsch",
     *       "code": "DE"
     *     }
     */

    /**
     * @apidefine LanguageCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Language id.
     * @apiSuccess (Success response payload) {String} name Language name.
     * @apiSuccess (Success response payload) {String} code Language code.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "name": "Deutsch",
     *       "code": "DE"
     *     }
     */

    /**
     * @apidefine LanguagesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Language objects.
     * @apiSuccess (Success response payload) {Number} _.id Language id.
     * @apiSuccess (Success response payload) {String} _.name Language name.
     * @apiSuccess (Success response payload) {String} _.code Language code.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "name": "Deutsch",
     *         "code": "DE"
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /languages Get All Languages
     * @apiName GetLanguages
     * @apiGroup Languages
     * @apiVersion 1.0.0
     *
     * @apiUse LanguagesPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/languages",
        Permissions: nil,
        Handler: commonControllers.LanguageController.All,
    },

    /**
     * @api {get} /languages/:id Get Language
     * @apiName GetLanguage
     * @apiGroup Languages
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Language unique id.
     *
     * @apiUse LanguagePayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/languages/:id",
        Permissions: nil,
        Handler: commonControllers.LanguageController.Get,
    },

    /**
     * @api {post} /languages Create Language
     * @apiName PostLanguage
     * @apiGroup Languages
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse LocationRequestBody
     *
     * @apiUse LanguageCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/languages",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LanguageController.Create,
    },

    /**
     * @api {put} /languages/:id Update Language
     * @apiName PutLanguage
     * @apiGroup Languages
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Language unique id.
     *
     * @apiUse LanguageRequestBody
     *
     * @apiUse LanguagePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/languages/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LanguageController.Update,
    },

    /**
     * @api {delete} /languages/:id Delete Language
     * @apiName DeleteLanguage
     * @apiGroup Languages
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Language unique id.
     *
     * @apiUse LanguagePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/languages/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.LanguageController.Delete,
    },


    /******************************************************************************************/
    /**************************************** RESOURCES ***************************************/
    /******************************************************************************************/

    /**
     * @apidefine ResourceRequestBody
     *
     * @apiParam (Request body) {String="image","video","audio"} type Resource type (image, video, etc).
     * @apiParam (Request body) {String} [name] Resource name.
     * @apiParam (Request body) {String} url Resource url.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "type": "image",
     *       "name": "My logo image",
     *       "url": "http://cdn.gig.resource/14567643324/beautifulpic.jpeg"
     *     }
     */

    /**
     * @apidefine ResourcePayload
     *
     * @apiSuccess (Success response payload) {Number} id Resource id.
     * @apiSuccess (Success response payload) {String} type Resource type (image, video, etc).
     * @apiSuccess (Success response payload) {String} name Resource name.
     * @apiSuccess (Success response payload) {String} url Resource url.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 1,
     *       "type": "image",
     *       "name": "My logo image",
     *       "url": "http://cdn.gig.resource/14567643324/beautifulpic.jpeg"
     *     }
     */

    /**
     * @apidefine ResourceCreatePayload
     *
     * @apiSuccess (Success response payload) {Number} id Resource id.
     * @apiSuccess (Success response payload) {String} type Resource type (image, video, etc).
     * @apiSuccess (Success response payload) {String} name Resource name.
     * @apiSuccess (Success response payload) {String} url Resource url.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 201 OK
     *     {
     *       "id": 1,
     *       "type": "image",
     *       "name": "My logo image",
     *       "url": "http://cdn.gig.resource/14567643324/beautifulpic.jpeg"
     *     }
     */

    /**
     * @apidefine ResourcesPayload
     *
     * @apiSuccess (Success response payload) {Object[]} _ List of Resource objects.
     * @apiSuccess (Success response payload) {Number} _.id Resource id.
     * @apiSuccess (Success response payload) {String} _.type Resource type (image, video, etc).
     * @apiSuccess (Success response payload) {String} _.name Resource name.
     * @apiSuccess (Success response payload) {String} _.url Resource url.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *         "id": 1,
     *         "type": "image",
     *         "name": "My logo image",
     *         "url": "http://cdn.gig.resource/14567643324/beautifulpic.jpeg"
     *       },
     *       ...
     *     ]
     */

    /**
     * @api {get} /resources Get All Resources
     * @apiName GetResources
     * @apiGroup Resources
     * @apiVersion 1.0.0
     *
     * @apiUse ResourcesPayload
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/resources",
        Permissions: nil,
        Handler: commonControllers.ResourceController.All,
    },

    /**
     * @api {get} /resources/:id Get Resource
     * @apiName GetResource
     * @apiGroup Resources
     * @apiVersion 1.0.0
     *
     * @apiParam (Parameters) {Number} id Resource unique id.
     *
     * @apiUse ResourcePayload
     *
     * @apiUse BadRequestError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/resources/:id",
        Permissions: nil,
        Handler: commonControllers.ResourceController.Get,
    },

    /**
     * @api {post} /resources Create Resource
     * @apiName PostResource
     * @apiGroup Resources
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiUse ResourceRequestBody
     *
     * @apiUse ResourceCreatePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/resources",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.ResourceController.Create,
    },

    /**
     * @api {put} /resources/:id Update Resource
     * @apiName PutResource
     * @apiGroup Resources
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Resource unique id.
     *
     * @apiUse LanguageRequestBody
     *
     * @apiUse ResourcePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "PUT",
        Path: "/resources/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.ResourceController.Update,
    },

    /**
     * @api {delete} /resources/:id Delete Resource
     * @apiName DeleteResource
     * @apiGroup Resources
     * @apiVersion 1.0.0
     * @apiPermission AdminAuth
     *
     * @apiUse ApiTokenHeader
     *
     * @apiParam (Parameters) {Number} id Resource unique id.
     *
     * @apiUse ResourcePayload
     *
     * @apiUse BadRequestError
     * @apiUse UnauthorizedError
     * @apiUse NotFoundError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "DELETE",
        Path: "/resources/:id",
        Permissions: []string{authModels.Permissions.ManageSystem},
        Handler: commonControllers.ResourceController.Delete,
    },
}
