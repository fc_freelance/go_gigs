/*
Package controllers: Languages module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// languageController: Controller private struct
type languageController struct {}

// LanguageController: Controller global variable (sort of singleton)
var LanguageController = languageController{}

/*
All: Gets all languages
 GET /languages
 */
func (ct *languageController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, languages := commonServices.LanguageService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with languages list
    c.JSON(http.StatusOK, languages)
}

/*
Get: Gets a language
 GET /languages/:id
 */
func (ct *languageController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing language id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, language := commonServices.LanguageService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with language
    c.JSON(http.StatusOK, language)
}

/*
Create: Creates a new language
 POST /languages
 */
func (ct *languageController) Create(c *gin.Context) {

    // Declare Language model
    var language commonModels.Language

    // Error binding request data to model, 400
    if err := c.BindJSON(&language); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdLocation := commonServices.LanguageService.Create(language)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added language
    c.JSON(http.StatusCreated, createdLocation)
}

/*
Update: Updates a language
 PUT /languages/:id
 */
func (ct *languageController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing language id",
        })
        return
    }

    // Declare language model
    var language commonModels.Language

    // Error binding request data to model, 400
    if err := c.BindJSON(&language); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedLocation := commonServices.LanguageService.Update(int(id), language)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified language
    c.JSON(http.StatusOK, updatedLocation)
}

/*
Delete: Deletes a language
 DELETE /languages/:id
 */
func (ct *languageController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing language id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, language := commonServices.LanguageService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, language)
}
