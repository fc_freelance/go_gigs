/*
Package controllers: Addresses module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// addressController: Controller private struct
type addressController struct {}

// AddressController: Controller global variable (sort of singleton)
var AddressController = addressController{}

/*
All: Gets all addresses
 GET /addresses
 */
func (ct *addressController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, addresses := commonServices.AddressService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with addresses list
    c.JSON(http.StatusOK, addresses)
}

/*
Get: Gets a address
 GET /addresses/:id
 */
func (ct *addressController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing address id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, address := commonServices.AddressService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with address
    c.JSON(http.StatusOK, address)
}

/*
Create: Creates a new address
 POST /addresses
 */
func (ct *addressController) Create(c *gin.Context) {

    // Declare Address model
    var address commonModels.Address

    // Error binding request data to model, 400
    if err := c.BindJSON(&address); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Get parent for address
    err, addressLocation := commonServices.LocationService.Get(int(address.LocationID))
    if err != nil || addressLocation == nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Could not get address location info",
        })
        return
    }

    // Assign user to address
    address.Location = addressLocation

    // Call service and throw error if any
    httpErr, createdLocation := commonServices.AddressService.Create(address)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added address
    c.JSON(http.StatusCreated, createdLocation)
}

/*
Update: Updates a address
 PUT /addresses/:id
 */
func (ct *addressController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing address id",
        })
        return
    }

    // Declare address model
    var address commonModels.Address

    // Error binding request data to model, 400
    if err := c.BindJSON(&address); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedLocation := commonServices.AddressService.Update(int(id), address)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified address
    c.JSON(http.StatusOK, updatedLocation)
}

/*
Delete: Deletes a address
 DELETE /addresses/:id
 */
func (ct *addressController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing address id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, address := commonServices.AddressService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, address)
}
