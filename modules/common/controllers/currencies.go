/*
Package controllers: Currencies module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// currencyController: Controller private struct
type currencyController struct {}

// CurrencyController: Controller global variable (sort of singleton)
var CurrencyController = currencyController{}

/*
All: Gets all currencies
 GET /currencies
 */
func (ct *currencyController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, currencies := commonServices.CurrencyService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with currencies list
    c.JSON(http.StatusOK, currencies)
}

/*
Get: Gets a currency
 GET /currencies/:id
 */
func (ct *currencyController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing currency id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, currency := commonServices.CurrencyService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with currency
    c.JSON(http.StatusOK, currency)
}

/*
Create: Creates a new currency
 POST /currencies
 */
func (ct *currencyController) Create(c *gin.Context) {

    // Declare Currency model
    var currency commonModels.Currency

    // Error binding request data to model, 400
    if err := c.BindJSON(&currency); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdLocation := commonServices.CurrencyService.Create(currency)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added currency
    c.JSON(http.StatusCreated, createdLocation)
}

/*
Update: Updates a currency
 PUT /currencies/:id
 */
func (ct *currencyController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing currency id",
        })
        return
    }

    // Declare currency model
    var currency commonModels.Currency

    // Error binding request data to model, 400
    if err := c.BindJSON(&currency); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedLocation := commonServices.CurrencyService.Update(int(id), currency)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified currency
    c.JSON(http.StatusOK, updatedLocation)
}

/*
Delete: Deletes a currency
 DELETE /currencies/:id
 */
func (ct *currencyController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing currency id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, currency := commonServices.CurrencyService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, currency)
}
