/*
Package controllers: Locations module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// locationController: Controller private struct
type locationController struct {}

// LocationController: Controller global variable (sort of singleton)
var LocationController = locationController{}

/*
All: Gets all locations
 GET /locations
 */
func (ct *locationController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, locations := commonServices.LocationService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with locations list
    c.JSON(http.StatusOK, locations)
}

/*
Get: Gets a location
 GET /locations/:id
 */
func (ct *locationController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing location id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, location := commonServices.LocationService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with location
    c.JSON(http.StatusOK, location)
}

/*
Create: Creates a new location
 POST /locations
 */
func (ct *locationController) Create(c *gin.Context) {

    // Declare Location model
    var location commonModels.Location

    // Error binding request data to model, 400
    if err := c.BindJSON(&location); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Check if parent location provided
    if (location.ParentID != nil) {

        // Get parent for location
        err, parentLocation := commonServices.LocationService.Get(int(*location.ParentID))
        if err != nil || parentLocation == nil {
            c.JSON(http.StatusBadRequest, gin.H{
                "error": "Could not get parent location info",
            })
            return
        }

        // Assign user to location
        location.Parent = parentLocation
    }

    // Call service and throw error if any
    httpErr, createdLocation := commonServices.LocationService.Create(location)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added location
    c.JSON(http.StatusCreated, createdLocation)
}

/*
Update: Updates a location
 PUT /locations/:id
 */
func (ct *locationController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing location id",
        })
        return
    }

    // Declare location model
    var location commonModels.Location

    // Error binding request data to model, 400
    if err := c.BindJSON(&location); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedLocation := commonServices.LocationService.Update(int(id), location)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified location
    c.JSON(http.StatusOK, updatedLocation)
}

/*
Delete: Deletes a location
 DELETE /locations/:id
 */
func (ct *locationController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing location id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, location := commonServices.LocationService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, location)
}
