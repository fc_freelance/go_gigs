/*
Package controllers: Resources module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// resourceController: Controller private struct
type resourceController struct {}

// ResourceController: Controller global variable (sort of singleton)
var ResourceController = resourceController{}

/*
All: Gets all resources
 GET /resources
 */
func (ct *resourceController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, resources := commonServices.ResourceService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with resources list
    c.JSON(http.StatusOK, resources)
}

/*
Get: Gets a resource
 GET /resources/:id
 */
func (ct *resourceController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing resource id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, resource := commonServices.ResourceService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with resource
    c.JSON(http.StatusOK, resource)
}

/*
Create: Creates a new resource
 POST /resources
 */
func (ct *resourceController) Create(c *gin.Context) {

    // Declare Resource model
    var resource commonModels.Resource

    // Error binding request data to model, 400
    if err := c.BindJSON(&resource); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdLocation := commonServices.ResourceService.Create(resource)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added resource
    c.JSON(http.StatusCreated, createdLocation)
}

/*
Update: Updates a resource
 PUT /resources/:id
 */
func (ct *resourceController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing resource id",
        })
        return
    }

    // Declare resource model
    var resource commonModels.Resource

    // Error binding request data to model, 400
    if err := c.BindJSON(&resource); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedLocation := commonServices.ResourceService.Update(int(id), resource)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified resource
    c.JSON(http.StatusOK, updatedLocation)
}

/*
Delete: Deletes a resource
 DELETE /resources/:id
 */
func (ct *resourceController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing resource id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, resource := commonServices.ResourceService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, resource)
}
