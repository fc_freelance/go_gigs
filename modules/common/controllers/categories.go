/*
Package controllers: Categories module controllers
 */
package controllers

import (
    "net/http"
    "strconv"

    "github.com/gin-gonic/gin"

    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
)

// categoryController: Controller private struct
type categoryController struct {}

// CategoryController: Controller global variable (sort of singleton)
var CategoryController = categoryController{}

/*
All: Gets all categories
 GET /categories
 */
func (ct *categoryController) All(c *gin.Context) {

    // Call service and throw error if any
    httpErr, categories := commonServices.CategoryService.All()
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with categories list
    c.JSON(http.StatusOK, categories)
}

/*
Get: Gets a category
 GET /categories/:id
 */
func (ct *categoryController) Get(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing category id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, category := commonServices.CategoryService.Get(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with category
    c.JSON(http.StatusOK, category)
}

/*
Create: Creates a new category
 POST /categories
 */
func (ct *categoryController) Create(c *gin.Context) {

    // Declare Category model
    var category commonModels.Category

    // Error binding request data to model, 400
    if err := c.BindJSON(&category); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, createdCategory := commonServices.CategoryService.Create(category)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with added category
    c.JSON(http.StatusCreated, createdCategory)
}

/*
Update: Updates a category
 PUT /categories/:id
 */
func (ct *categoryController) Update(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing category id",
        })
        return
    }

    // Declare category model
    var category commonModels.Category

    // Error binding request data to model, 400
    if err := c.BindJSON(&category); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and throw error if any
    httpErr, updatedCategory := commonServices.CategoryService.Update(int(id), category)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Send response with modified category
    c.JSON(http.StatusOK, updatedCategory)
}

/*
Delete: Deletes a category
 DELETE /categories/:id
 */
func (ct *categoryController) Delete(c *gin.Context) {

    // Get id from parameters
    id, err := strconv.ParseInt(c.Param("id"), 10, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Missing category id",
        })
        return
    }

    // Call service and throw error if any
    httpErr, category := commonServices.CategoryService.Delete(int(id))
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Return response with deleted model
    c.JSON(http.StatusOK, category)
}