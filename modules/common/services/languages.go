/*
Package services: Languages module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// languageService: Service private struct
type languageService struct {}

// LanguageService: Service global variable (sort of singleton)
var LanguageService = languageService{}

/*
All: Gets all languages
 */
func (s *languageService) All() (*httpModels.HttpError, *[]commonModels.Language) {

    // Return collection or error in repository
    return commonRepositories.LanguageRepository.All()
}

/*
Get: Gets a language
 */
func (s *languageService) Get(id int) (*httpModels.HttpError, *commonModels.Language) {

    // Find model or error in repository
    return commonRepositories.LanguageRepository.Get(id)
}

/*
Find: Finds a language
 */
func (s *languageService) Find(language commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Find model or error in repository
    return commonRepositories.LanguageRepository.Find(language)
}

/*
Create: Creates a Language
 */
func (s *languageService) Create(language commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Create model or error in repository
    return commonRepositories.LanguageRepository.Create(language)
}

/*
Update: Updates a Language
 */
func (s *languageService) Update(id int, language commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Update model or error in repository
    return commonRepositories.LanguageRepository.Update(id, language)
}

/*
Delete: Deletes a Language by id
 */
func (s *languageService) Delete(id int) (*httpModels.HttpError, *commonModels.Language) {

    // Delete model or error in repository
    return commonRepositories.LanguageRepository.Delete(id)
}