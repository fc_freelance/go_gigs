/*
Package services: Currencies module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// currencyService: Service private struct
type currencyService struct {}

// CurrencyService: Service global variable (sort of singleton)
var CurrencyService = currencyService{}

/*
All: Gets all currencies
 */
func (s *currencyService) All() (*httpModels.HttpError, *[]commonModels.Currency) {

    // Return collection or error in repository
    return commonRepositories.CurrencyRepository.All()
}

/*
Get: Gets a currency
 */
func (s *currencyService) Get(id int) (*httpModels.HttpError, *commonModels.Currency) {

    // Find model or error in repository
    return commonRepositories.CurrencyRepository.Get(id)
}

/*
Find: Finds a currency
 */
func (s *currencyService) Find(currency commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Find model or error in repository
    return commonRepositories.CurrencyRepository.Find(currency)
}

/*
Create: Creates a Currency
 */
func (s *currencyService) Create(currency commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Create model or error in repository
    return commonRepositories.CurrencyRepository.Create(currency)
}

/*
Update: Updates a Currency
 */
func (s *currencyService) Update(id int, currency commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Update model or error in repository
    return commonRepositories.CurrencyRepository.Update(id, currency)
}

/*
Delete: Deletes a Currency by id
 */
func (s *currencyService) Delete(id int) (*httpModels.HttpError, *commonModels.Currency) {

    // Delete model or error in repository
    return commonRepositories.CurrencyRepository.Delete(id)
}