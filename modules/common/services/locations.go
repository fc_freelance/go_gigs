/*
Package services: Locations module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// locationService: Service private struct
type locationService struct {}

// LocationService: Service global variable (sort of singleton)
var LocationService = locationService{}

/*
All: Gets all locations
 */
func (s *locationService) All() (*httpModels.HttpError, *[]commonModels.Location) {

    // Return collection or error in repository
    return commonRepositories.LocationRepository.All()
}

/*
Get: Gets a location
 */
func (s *locationService) Get(id int) (*httpModels.HttpError, *commonModels.Location) {

    // Find model or error in repository
    return commonRepositories.LocationRepository.Get(id)
}

/*
Find: Finds a location
 */
func (s *locationService) Find(location commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Find model or error in repository
    return commonRepositories.LocationRepository.Find(location)
}

/*
Create: Creates a Location
 */
func (s *locationService) Create(location commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Create model or error in repository
    return commonRepositories.LocationRepository.Create(location)
}

/*
Update: Updates a Location
 */
func (s *locationService) Update(id int, location commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Update model or error in repository
    return commonRepositories.LocationRepository.Update(id, location)
}

/*
Delete: Deletes a Location by id
 */
func (s *locationService) Delete(id int) (*httpModels.HttpError, *commonModels.Location) {

    // Delete model or error in repository
    return commonRepositories.LocationRepository.Delete(id)
}