/*
Package services: Categories module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// categoryService: Service private struct
type categoryService struct {}

// CategoryService: Service global variable (sort of singleton)
var CategoryService = categoryService{}

/*
All: Gets all categories
 */
func (s *categoryService) All() (*httpModels.HttpError, *[]commonModels.Category) {

    // Return collection or error in repository
    return commonRepositories.CategoryRepository.All()
}

/*
Get: Gets a category
 */
func (s *categoryService) Get(id int) (*httpModels.HttpError, *commonModels.Category) {

    // Find model or error in repository
    return commonRepositories.CategoryRepository.Get(id)
}

/*
Find: Finds a category
 */
func (s *categoryService) Find(category commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Find model or error in repository
    return commonRepositories.CategoryRepository.Find(category)
}

/*
Create: Creates a Category
 */
func (s *categoryService) Create(category commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Create model or error in repository
    return commonRepositories.CategoryRepository.Create(category)
}

/*
Update: Updates a Category
 */
func (s *categoryService) Update(id int, category commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Update model or error in repository
    return commonRepositories.CategoryRepository.Update(id, category)
}

/*
Delete: Deletes a Category by id
 */
func (s *categoryService) Delete(id int) (*httpModels.HttpError, *commonModels.Category) {

    // Delete model or error in repository
    return commonRepositories.CategoryRepository.Delete(id)
}