/*
Package services: Addresses module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// addressService: Service private struct
type addressService struct {}

// AddressService: Service global variable (sort of singleton)
var AddressService = addressService{}

/*
All: Gets all addresses
 */
func (s *addressService) All() (*httpModels.HttpError, *[]commonModels.Address) {

    // Return collection or error in repository
    return commonRepositories.AddressRepository.All()
}

/*
Get: Gets a address
 */
func (s *addressService) Get(id int) (*httpModels.HttpError, *commonModels.Address) {

    // Find model or error in repository
    return commonRepositories.AddressRepository.Get(id)
}

/*
Find: Finds a address
 */
func (s *addressService) Find(address commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Find model or error in repository
    return commonRepositories.AddressRepository.Find(address)
}

/*
Create: Creates a Address
 */
func (s *addressService) Create(address commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Create model or error in repository
    return commonRepositories.AddressRepository.Create(address)
}

/*
Update: Updates a Address
 */
func (s *addressService) Update(id int, address commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Update model or error in repository
    return commonRepositories.AddressRepository.Update(id, address)
}

/*
Delete: Deletes a Address by id
 */
func (s *addressService) Delete(id int) (*httpModels.HttpError, *commonModels.Address) {

    // Delete model or error in repository
    return commonRepositories.AddressRepository.Delete(id)
}