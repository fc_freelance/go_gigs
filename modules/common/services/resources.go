/*
Package services: Resources module services
 */
package services

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
    commonRepositories "gitlab.com/fc_freelance/go_gigs/modules/common/repositories"
)

// resourceService: Service private struct
type resourceService struct {}

// ResourceService: Service global variable (sort of singleton)
var ResourceService = resourceService{}

/*
All: Gets all resources
 */
func (s *resourceService) All() (*httpModels.HttpError, *[]commonModels.Resource) {

    // Return collection or error in repository
    return commonRepositories.ResourceRepository.All()
}

/*
Get: Gets a resource
 */
func (s *resourceService) Get(id int) (*httpModels.HttpError, *commonModels.Resource) {

    // Find model or error in repository
    return commonRepositories.ResourceRepository.Get(id)
}

/*
Find: Finds a resource
 */
func (s *resourceService) Find(resource commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Find model or error in repository
    return commonRepositories.ResourceRepository.Find(resource)
}

/*
Create: Creates a Resource
 */
func (s *resourceService) Create(resource commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Create model or error in repository
    return commonRepositories.ResourceRepository.Create(resource)
}

/*
Update: Updates a Resource
 */
func (s *resourceService) Update(id int, resource commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Update model or error in repository
    return commonRepositories.ResourceRepository.Update(id, resource)
}

/*
Delete: Deletes a Resource by id
 */
func (s *resourceService) Delete(id int) (*httpModels.HttpError, *commonModels.Resource) {

    // Delete model or error in repository
    return commonRepositories.ResourceRepository.Delete(id)
}