/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// resourceRepository: Repository private struct
type resourceRepository struct {}

// ResourceRepository: Repository global variable (sort of singleton)
var ResourceRepository = resourceRepository{}

/*
All: Gets all resources
 */
func (r *resourceRepository) All() (*httpModels.HttpError, *[]commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    resources := []commonModels.Resource{}

    // Run query and handle error
    err := db.DB.Find(&resources).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &resources
}

/*
Get: Gets an resource
 */
func (r *resourceRepository) Get(id int) (*httpModels.HttpError, *commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    resource := commonModels.Resource{}

    // Run query and handle error
    err := db.DB.Where("id = ?", id).Find(&resource).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return resource
    return nil, &resource
}

/*
Find: Finds an resource
 */
func (r *resourceRepository) Find(ref commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold resource
    var resource commonModels.Resource

    // Run query and handle error
    err := db.DB.Where(&ref).First(&resource).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return resource
    return nil, &resource
}

/*
Create: Creates an Resource
 */
func (r *resourceRepository) Create(resource commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create resource",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(resource)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Resource already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&resource).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created resource
    return nil, &resource
}

/*
Update: Updates an resource
 */
func (r *resourceRepository) Update(id int, resource commonModels.Resource) (*httpModels.HttpError, *commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update resource",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched resource
    var ref commonModels.Resource

    // Update resource in DB, handle error
    err := db.DB.First(&ref, id).Updates(resource).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &ref
}

/*
Delete: Deletes an resource
 */
func (r *resourceRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Resource) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete resource",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold resource
    var resource commonModels.Resource

    // Delete resource from DB, handle error
    err := db.DB.First(&resource, id).Delete(&resource).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &resource
}
