/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// addressRepository: Repository private struct
type addressRepository struct {}

// AddressRepository: Repository global variable (sort of singleton)
var AddressRepository = addressRepository{}

/*
All: Gets all addresses
 */
func (r *addressRepository) All() (*httpModels.HttpError, *[]commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    addresses := []commonModels.Address{}

    // Run query and handle error
    err := db.DB.Preload("Location").Find(&addresses).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &addresses
}

/*
Get: Gets an address
 */
func (r *addressRepository) Get(id int) (*httpModels.HttpError, *commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    address := commonModels.Address{}

    // Run query and handle error
    err := db.DB.Preload("Location").Where("id = ?", id).Find(&address).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return address
    return nil, &address
}

/*
Find: Finds an address
 */
func (r *addressRepository) Find(ref commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold address
    var address commonModels.Address

    // Run query and handle error
    err := db.DB.Preload("Location").Where(&ref).First(&address).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return address
    return nil, &address
}

/*
Create: Creates an Address
 */
func (r *addressRepository) Create(address commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create address",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(address)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Address already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&address).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created address
    return nil, &address
}

/*
Update: Updates an address
 */
func (r *addressRepository) Update(id int, address commonModels.Address) (*httpModels.HttpError, *commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update address",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched address
    var ref commonModels.Address

    // Update address in DB, handle error
    err := db.DB.Preload("Location").First(&ref, id).Updates(address).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &ref
}

/*
Delete: Deletes an address
 */
func (r *addressRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Address) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete address",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold address
    var address commonModels.Address

    // Delete address from DB, handle error
    err := db.DB.First(&address, id).Delete(&address).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &address
}
