/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// categoryRepository: Repository private struct
type categoryRepository struct {}

// CategoryRepository: Repository global variable (sort of singleton)
var CategoryRepository = categoryRepository{}

/*
All: Gets all categories
 */
func (r *categoryRepository) All() (*httpModels.HttpError, *[]commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    categories := []commonModels.Category{}

    // Run query and handle error
    err := db.DB.Find(&categories).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &categories
}

/*
Get: Gets a category
 */
func (r *categoryRepository) Get(id int) (*httpModels.HttpError, *commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    category := commonModels.Category{}

    // Run query and handle error
    err := db.DB.Where("id = ?", id).Find(&category).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return category
    return nil, &category
}

/*
Find: Finds a category
 */
func (r *categoryRepository) Find(gg commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold category
    var category commonModels.Category

    // Run query and handle error
    err := db.DB.Preload("Parent").Preload("SubCategories").Where(&gg).First(&category).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return category
    return nil, &category
}

/*
Create: Creates a Category
 */
func (r *categoryRepository) Create(category commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create category",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(category)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Category already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    log.Println(category)
    err2 := db.DB.Create(&category).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created category
    return nil, &category
}

/*
Update: Updates a category
 */
func (r *categoryRepository) Update(id int, category commonModels.Category) (*httpModels.HttpError, *commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update category",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched category
    var gg commonModels.Category

    // Update category in DB, handle error
    err := db.DB.Preload("Parent").Preload("SubCategories").First(&gg, id).Updates(category).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &gg
}

/*
Delete: Deletes a category
 */
func (r *categoryRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Category) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete category",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold category
    var category commonModels.Category

    // Delete category from DB, handle error
    err := db.DB.First(&category, id).Delete(&category).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &category
}