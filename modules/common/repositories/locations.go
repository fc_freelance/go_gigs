/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// locationRepository: Repository private struct
type locationRepository struct {}

// LocationRepository: Repository global variable (sort of singleton)
var LocationRepository = locationRepository{}

/*
All: Gets all locations
 */
func (r *locationRepository) All() (*httpModels.HttpError, *[]commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    locations := []commonModels.Location{}

    // Run query and handle error
    err := db.DB.Preload("Parent").Find(&locations).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &locations
}

/*
Get: Gets a location
 */
func (r *locationRepository) Get(id int) (*httpModels.HttpError, *commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    location := commonModels.Location{}

    // Run query and handle error
    err := db.DB.Preload("Parent").Where("id = ?", id).Find(&location).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return location
    return nil, &location
}

/*
Find: Finds a location
 */
func (r *locationRepository) Find(ref commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold location
    var location commonModels.Location

    // Run query and handle error
    err := db.DB.Preload("Parent").Where(&ref).First(&location).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return location
    return nil, &location
}

/*
Create: Creates a Location
 */
func (r *locationRepository) Create(location commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create location",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(location)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Location already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&location).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created location
    return nil, &location
}

/*
Update: Updates a location
 */
func (r *locationRepository) Update(id int, location commonModels.Location) (*httpModels.HttpError, *commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update location",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched location
    var ref commonModels.Location

    // Update location in DB, handle error
    err := db.DB.Preload("Parent").First(&ref, id).Updates(location).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &ref
}

/*
Delete: Deletes a location
 */
func (r *locationRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Location) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete location",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold location
    var location commonModels.Location

    // Delete location from DB, handle error
    err := db.DB.First(&location, id).Delete(&location).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &location
}
