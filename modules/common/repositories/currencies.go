/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// currencyRepository: Repository private struct
type currencyRepository struct {}

// CurrencyRepository: Repository global variable (sort of singleton)
var CurrencyRepository = currencyRepository{}

/*
All: Gets all currencies
 */
func (r *currencyRepository) All() (*httpModels.HttpError, *[]commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    currencies := []commonModels.Currency{}

    // Run query and handle error
    err := db.DB.Find(&currencies).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &currencies
}

/*
Get: Gets an currency
 */
func (r *currencyRepository) Get(id int) (*httpModels.HttpError, *commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    currency := commonModels.Currency{}

    // Run query and handle error
    err := db.DB.Where("id = ?", id).Find(&currency).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return currency
    return nil, &currency
}

/*
Find: Finds an currency
 */
func (r *currencyRepository) Find(ref commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold currency
    var currency commonModels.Currency

    // Run query and handle error
    err := db.DB.Where(&ref).First(&currency).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return currency
    return nil, &currency
}

/*
Create: Creates an Currency
 */
func (r *currencyRepository) Create(currency commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create currency",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(currency)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Currency already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&currency).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created currency
    return nil, &currency
}

/*
Update: Updates an currency
 */
func (r *currencyRepository) Update(id int, currency commonModels.Currency) (*httpModels.HttpError, *commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update currency",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched currency
    var ref commonModels.Currency

    // Update currency in DB, handle error
    err := db.DB.First(&ref, id).Updates(currency).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &ref
}

/*
Delete: Deletes an currency
 */
func (r *currencyRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Currency) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete currency",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold currency
    var currency commonModels.Currency

    // Delete currency from DB, handle error
    err := db.DB.First(&currency, id).Delete(&currency).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &currency
}
