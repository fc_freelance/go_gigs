/*
Package repositories: Categories module repositories
 */
package repositories

import (
    "net/http"
    "log"

    "gitlab.com/fc_freelance/go_gigs/db"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
)

// languageRepository: Repository private struct
type languageRepository struct {}

// LanguageRepository: Repository global variable (sort of singleton)
var LanguageRepository = languageRepository{}

/*
All: Gets all languages
 */
func (r *languageRepository) All() (*httpModels.HttpError, *[]commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    languages := []commonModels.Language{}

    // Run query and handle error
    err := db.DB.Find(&languages).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return collection
    return nil, &languages
}

/*
Get: Gets an language
 */
func (r *languageRepository) Get(id int) (*httpModels.HttpError, *commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Define holder for collection
    language := commonModels.Language{}

    // Run query and handle error
    err := db.DB.Where("id = ?", id).Find(&language).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return language
    return nil, &language
}

/*
Find: Finds an language
 */
func (r *languageRepository) Find(ref commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Not found",
        StatusCode: http.StatusNotFound,
    }

    // Hold language
    var language commonModels.Language

    // Run query and handle error
    err := db.DB.Where(&ref).First(&language).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return language
    return nil, &language
}

/*
Create: Creates an Language
 */
func (r *languageRepository) Create(language commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not create language",
        StatusCode: http.StatusInternalServerError,
    }

    // Check we don't have any id
    isNew := db.DB.NewRecord(language)
    if !isNew {
        return &httpModels.HttpError{
            Message: "Language already exists",
            StatusCode: http.StatusBadRequest,
        }, nil
    }

    // Create user in DB, handle error
    err2 := db.DB.Create(&language).Error
    if err2 != nil {
        log.Println(err2)
        return &defaultError, nil
    }

    // Return created language
    return nil, &language
}

/*
Update: Updates an language
 */
func (r *languageRepository) Update(id int, language commonModels.Language) (*httpModels.HttpError, *commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not update language",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold fetched language
    var ref commonModels.Language

    // Update language in DB, handle error
    err := db.DB.First(&ref, id).Updates(language).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &ref
}

/*
Delete: Deletes an language
 */
func (r *languageRepository) Delete(id int) (*httpModels.HttpError, *commonModels.Language) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Could not delete language",
        StatusCode: http.StatusInternalServerError,
    }

    // Hold language
    var language commonModels.Language

    // Delete language from DB, handle error
    err := db.DB.First(&language, id).Delete(&language).Error
    if err != nil {
        log.Println(err)
        return &defaultError, nil
    }

    // Return data
    return nil, &language
}
