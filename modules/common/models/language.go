/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Language: Public Language model struct
 */
type Language struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Code string  `form:"code" json:"code" binding:"required"`
}
