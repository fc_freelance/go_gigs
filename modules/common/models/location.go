/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Location: Public Location model struct
 */
type Location struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Code string  `form:"code" json:"code" binding:"required"`
    Lat *float64  `form:"lat" json:"lat,omitempty"`
    Lon *float64  `form:"lon" json:"lon,omitempty"`
    ParentID *uint `form:"parent_id" json:"parent_id,omitempty" gorm:"default:NULL"`
    Parent *Location `json:"parent,omitempty"`
    SubLocations []Location `json:"sublocations,omitempty" gorm:"ForeignKey:ParentID"`
}
