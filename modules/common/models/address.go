/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Address: Public Address model struct
 */
type Address struct {
    dbModels.DbBaseModel
    Number uint `form:"number" json:"number" binding:"required"`
    Street string `form:"street" json:"street" binding:"required"`
    PostalCode string `form:"postal_code" json:"postal_code" binding:"required"`
    ExtraInfo *string `form:"extra_info" json:"extra_info,omitempty"`
    LocationID uint `form:"location_id" json:"location_id" binding:"required"`
    Location *Location `json:"location"`
}
