/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Currency: Public Currency model struct
 */
type Currency struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Code string  `form:"code" json:"code" binding:"required"`
    Symbol string  `form:"symbol" json:"symbol" binding:"required"`
    Conversion float64  `form:"conversion" json:"conversion" binding:"required"`
}
