/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Resource: Public Resource model struct
 */
type Resource struct {
    dbModels.DbBaseModel
    Type string  `form:"type" json:"type" binding:"required"`
    Name *string  `form:"name" json:"name,omitempty"`
    Url string  `form:"url" json:"url" binding:"required"`
}
