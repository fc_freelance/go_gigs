/*
Package models - Common models
 */
package models

import (
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
)

/*
Category: Public Category model struct
 */
type Category struct {
    dbModels.DbBaseModel
    Name string  `form:"name" json:"name" binding:"required"`
    Description *string `form:"description" json:"description,omitempty"`
    ParentID *uint `form:"parent_id" json:"parent_id,omitempty" gorm:"default:NULL"`
    Parent *Category `json:"parent,omitempty"`
    SubCategories []Category `json:"subcategories,omitempty" gorm:"ForeignKey:ParentID"`
}
