/*
Package controllers - Auth controllers
 */
package controllers

import (
    "net/http"

    "github.com/gin-gonic/gin"

    cfg "gitlab.com/fc_freelance/go_gigs/config"
    authServices "gitlab.com/fc_freelance/go_gigs/modules/auth/services"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
    "time"
)

// authController: Private controller struct
type authController struct {}

// AuthController: Global controller variable
var AuthController = authController{}

/*
CheckSession: Checks the auth status
 GET /auth
 */
func (ct *authController) CheckSession(c *gin.Context) {

    // Call service and return error if any
    httpErr, session := authServices.AuthService.GetSession(c)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Get user by id from session
    err, _ := userServices.UserService.Get(int(session.UserId), false)
    if err != nil {
        
        // Ensure cookie deletion here, it doesn't work from DELETE request
        // Try to get cookie from request
        cookie, err := c.Request.Cookie(cfg.Config.Http.AuthCookie)

        // Checks if error getting the cookie, then we must be already logged out
        if err == nil {

            // Set expiration to now
            cookie.Expires = time.Now()

            // Set the cookie to expired
            http.SetCookie(c.Writer, cookie)
        }
        
        // Return 401
        c.JSON(http.StatusUnauthorized, gin.H{
            "error": "Unauthorized",
        })
        return
    }

    // Return Authorized/Authenticated with session data
    c.JSON(http.StatusOK, session)
}

/*
Login: Login a user
 POST /login
 */
func (ct *authController) Login(c *gin.Context) {

    // Declare login model
    var login authModels.Login

    // Error binding request data to model, 400
    if err := c.BindJSON(&login); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Bad form data",
        })
        return
    }

    // Call service and return error if any
    httpErr, cookie, session := authServices.AuthService.Login(login)
    if httpErr != nil {
        c.JSON(httpErr.StatusCode, gin.H{
            "error": httpErr.Message,
        })
        return
    }

    // Set the cookie
    http.SetCookie(c.Writer, cookie)

    // Send response with cookie
    c.JSON(http.StatusOK, session)
}

/*
Logout: Logout a user
 POST /logout
 */
func (ct *authController) Logout(c *gin.Context) {

    // Try to get token from request
    token := c.Request.Header.Get((cfg.Config.Http.AuthToken))

    // Check if not empty
    if token != "" {

        // Get Token from service
        _, tkn := userServices.UserApiTokenService.Find(userModels.UserApiToken{Token: token})

        // Check if token, and remove it if so
        if tkn != nil {
            userServices.UserApiTokenService.Delete(int(tkn.ID))
        }
    }

    // Try to get cookie from request
    cookie, err := c.Request.Cookie(cfg.Config.Http.AuthCookie)

    // Checks if error getting the cookie, then we must be already logged out
    if err != nil {
        c.JSON(http.StatusOK, gin.H{
            "status": "OK",
        })
        return
    }

    // Set expiration to now
    cookie.Expires = time.Now()

    // Set the cookie
    http.SetCookie(c.Writer, cookie)

    // Send response with cookie
    c.JSON(http.StatusOK, gin.H{
        "status": "OK",
    })
}