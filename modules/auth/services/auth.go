/*
Package services: Auth services
 */
package services

import (
    "net/http"
    "encoding/json"
    "encoding/hex"
    "time"
    "log"

    "github.com/gin-gonic/gin"

    cfg "gitlab.com/fc_freelance/go_gigs/config"
    "gitlab.com/fc_freelance/go_gigs/util"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
    userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
    userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
)

// authService: Private service struct
type authService struct {}

// AuthService: Service global variable (sort of singleton)
var AuthService = authService{}

/*
GetSession: Tries to get the session given a http context
 */
func (s *authService) GetSession(c *gin.Context) (*httpModels.HttpError, *authModels.Session) {

    // Try to get cookie from request
    cookie, err := c.Request.Cookie(cfg.Config.Http.AuthCookie)

    // Try to get token from request
    token := c.Request.Header.Get((cfg.Config.Http.AuthToken))

    // Checks if error getting the cookie, so 401 then
    if err != nil && token == "" {
        return &httpModels.HttpError{
            Message: "Unauthorized",
            StatusCode: http.StatusUnauthorized,
        }, nil
    }

    // Call service and return error if any
    return s.CheckSession(cookie, token)
}

/*
CheckSession: Checks the auth status
 */
func (s *authService) CheckSession(cookie *http.Cookie, token string) (*httpModels.HttpError, *authModels.Session) {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Unauthorized",
        StatusCode: http.StatusUnauthorized,
    }

    // Declare session model
    var session authModels.Session

    // Proceed with token if set
    if token != "" {

        // Get Token from service
        httpErr, tkn := userServices.UserApiTokenService.Find(userModels.UserApiToken{Token: token})
        if httpErr != nil {
            return &defaultError, nil
        }

        // Check if token expired, deleting it if any
        if tkn.ExpiresAt != nil && tkn.ExpiresAt.Before(time.Now()) {
            userServices.UserApiTokenService.Delete(int(tkn.ID))
            return &httpModels.HttpError{
                Message: "Token expired",
                StatusCode: http.StatusUnauthorized,
            }, nil
        }

        // Set data into session
        session.UserId = tkn.UserID
        session.ExpiresAt = tkn.ExpiresAt.Unix()

    // Proceed with cookie if set and no token
    } else if cookie != nil {

        // Decrypt cookie
        err, tkn := util.Crypt.Decrypt(cookie.Value)
        if err != nil {
            return &httpModels.HttpError{
                Message: "Server error while decrypting token",
                StatusCode: http.StatusInternalServerError,
            }, nil
        }

        // Error decoding = 401
        jsonToken, err := hex.DecodeString(tkn)
        if err != nil {
            return &defaultError, nil
        }

        // Try to recreate session from token
        if json.Unmarshal(jsonToken, &session) != nil {
            return &defaultError, nil
        }

        // Get expiration time for session (double security)
        expiresAt := time.Unix(session.ExpiresAt, 0)

        // Check proper user/role, 401 otherwise
        if session.UserId == 0 || expiresAt.Before(time.Now()) {
            return &defaultError, nil
        }

    // No token and no cookie!
    } else {
        return &defaultError, nil
    }

    // Return Authorized/Authenticated with session data
    return nil, &session
}

/*
GetSessionUser: Gets user from session
 */
func (s *authService) GetSessionUser(c *gin.Context) (*httpModels.HttpError, *userModels.User) {

    // Get session
    err, session := s.GetSession(c)
    if err != nil {
        return err, nil
    }

    // Get user by id from session
    err, user := userServices.UserService.Get(int(session.UserId), false)
    if err != nil {
        return err, nil
    }

    // Return user
    return nil, user
}

/*
CheckPermission: Checks session permissions
 */
func (s *authService) CheckPermission(session *authModels.Session, permissions []string) *httpModels.HttpError {

    // Hold general error here for reusing it ...
    defaultError := httpModels.HttpError{
        Message: "Unauthorized",
        StatusCode: http.StatusUnauthorized,
    }

    // Get user by id from session
    err, user := userServices.UserService.Get(int(session.UserId), true)
    if err != nil {
        log.Println(err)
        return &defaultError
    }

    // If user is not activated, no permissions then
    if !user.Active {
        return &defaultError
    }

    // Check permission codes
    hasPermissions := userServices.UserService.HasPermissions(*user, permissions)
    if !hasPermissions {
        return &defaultError
    }

    // Return success
    return nil
}

/*
Login: Login a user
 */
func (s *authService) Login(login authModels.Login) (*httpModels.HttpError, *http.Cookie, *authModels.Session) {

    // Holder for user
    usr := userModels.User {
        Email: login.Email,
        Password: util.Crypt.GetMD5Hash(login.Password),
    }

    // Try to get the user, fail otherwise
    err, user := userServices.UserService.Find(usr)
    if err != nil {
        return &httpModels.HttpError{
            Message: "Incorrect email and/or password",
            StatusCode: http.StatusUnauthorized,
        }, nil, nil
    }

    // Add multiplier for 'remember me' thing on login
    timeMultiplier := 1
    if login.Remember {
        timeMultiplier = int(cfg.Config.Http.AuthTimeRememberMultiplier)
    }

    // Try to parse time from settings
    sessionTime := int(cfg.Config.Http.AuthTime)

    // Add expiration
    expiration := time.Now().Add(time.Duration(timeMultiplier * sessionTime) * time.Hour)

    // Create session object
    session := authModels.Session{
        UserId: user.ID,
        ExpiresAt: expiration.Unix(),
    }

    // Generate api token
    apiToken := userModels.UserApiToken{
        UserID: user.ID,
        ExpiresAt: &expiration,
    }

    // Save it
    err, apiCreatedToken := userServices.UserApiTokenService.Create(apiToken)
    if err !=  nil {
        return &httpModels.HttpError{
            Message: "Server error while creating API token",
            StatusCode: http.StatusInternalServerError,
        }, nil, nil
    }

    // Add token to session
    session.Token = &apiCreatedToken.Token

    // Encode to bytes the relevant data for auth token, 401 otherwise
    // Important: Don't store in cookie session the API token!
    jsonBytes, err3 := json.Marshal(authModels.Session{
        UserId: user.ID,
        ExpiresAt: expiration.Unix(),
    })
    if err3 != nil {
        return &httpModels.HttpError{
            Message: "Unauthorized",
            StatusCode: http.StatusUnauthorized,
        }, nil, nil
    }

    // Encode to string for the cookie
    jsonHash := hex.EncodeToString(jsonBytes)

    // Create auth token, encrypt it
    tErr, token := util.Crypt.Encrypt(jsonHash)
    if tErr != nil {
        return &httpModels.HttpError{
            Message: "Server error while encrypting token",
            StatusCode: http.StatusInternalServerError,
        }, nil, nil
    }

    // Build cookie
    cookie := http.Cookie{
        Name: cfg.Config.Http.AuthCookie,
        Value: token,
        Expires: expiration,
    }

    // Return with cookie and session data
    return nil, &cookie, &session
}
