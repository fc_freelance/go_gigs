package models

/*
permissions: Private permissions struct
 */
type permissions struct {
    ListUsers string
    ManageGigs string
    ManageSystem string
}

/*
Permissions: Global permissions variable
 */
var Permissions = permissions {
    ListUsers: "list_users",
    ManageGigs: "manage_gigs",
    ManageSystem: "manage_system",
}