package models

/*
Session: Global session model struct
 */
type Session struct {
    UserId uint `json:"user_id"`
    ExpiresAt int64 `json:"expires_at"`
    Token *string `json:"token,omitempty" `
}
