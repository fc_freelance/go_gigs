/*
Package models - Auth models
 */
package models

/*
Login: Login model global struct
 */
type Login struct {
    Email string  `form:"email" json:"email" binding:"required"`
    Password string `form:"password" json:"password" binding:"required"`
    Remember bool `form:"remember" json:"remember"`
}
