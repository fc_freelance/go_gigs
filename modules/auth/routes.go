/*
Package auth - Auth module

Manages authentication/authorization for API
 */
package auth

import (
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    authControllers "gitlab.com/fc_freelance/go_gigs/modules/auth/controllers"
)

/*
Routes Global variable holding all auth routes for API
 */
var Routes = []httpModels.Route{

    /**
     * @apiDefine LoginRequestBody
     *
     * @apiParam (Request body) {String} email User email.
     * @apiParam (Request body) {String} password User password.
     * @apiParam (Request body) {Boolean} [remember] Remember session for a longer time.
     *
     * @apiParamExample {json} Request body example
     *     {
     *       "email": "whoever@gmail.com",
     *       "password": "mypass",
     *       "remember": false
     *     }
     */

    /**
     * @apiDefine LoginResponsePayload
     *
     * @apiSuccess (Success response payload) {Number} user_id User Id.
     * @apiSuccess (Success response payload) {Number} expires_at Session expiration timestamp.
     * @apiSuccess (Success response payload) {String} token API auth token.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "user_id": 1,
     *       "expires_at": 1459157340,
     *       "token": "an83jkfd34FDDF233ere2an83jkfd34FDDF233ere2"
     *     }
     */

    /**
     * @apiDefine SessionPayload
     *
     * @apiSuccess (Success response payload) {Number} user_id User Id.
     * @apiSuccess (Success response payload) {Number} expires_at Session expiration timestamp.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "user_id": 1,
     *       "expires_at": 1459157340
     *     }
     */


    /**
     * @api {get} /auth Get session status
     * @apiName GetAuth
     * @apiGroup Auth
     * @apiVersion 1.0.0
     *
     * @apiUse ApiTokenHeader
     *
     * @apiUse SessionPayload
     *
     * @apiUse UnauthorizedError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "GET",
        Path: "/auth",
        Permissions: nil,
        Handler: authControllers.AuthController.CheckSession,
    },

    /**
     * @api {post} /login Login into the API
     * @apiName PostLogin
     * @apiGroup Auth
     * @apiVersion 1.0.0
     *
     * @apiUse JsonHeader
     *
     * @apiUse LoginRequestBody
     *
     * @apiUse LoginResponsePayload
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/login",
        Permissions: nil,
        Handler: authControllers.AuthController.Login,
    },

    /**
     * @api {post} /logout Logout from the API
     * @apiName PostLogout
     * @apiGroup Auth
     * @apiVersion 1.0.0
     *
     * @apiUse ApiTokenHeader
     *
     * @apiSuccess (Success response payload) {String} status Operation status.
     *
     * @apiSuccessExample {json} Success response example
     *     HTTP/1.1 200 OK
     *     {
     *       "status": "OK",
     *     }
     *
     * @apiUse InternalServerError
     */
    httpModels.Route{
        Method: "POST",
        Path: "/logout",
        Permissions: nil,
        Handler: authControllers.AuthController.Logout,
    },
}
