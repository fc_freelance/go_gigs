/*
Package controllers: Gig Requests module controllers
*/
package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	//commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
	authModels "gitlab.com/fc_freelance/go_gigs/modules/auth/models"
	gigRequestModels "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/models"

	//commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
	"log"

	authServices "gitlab.com/fc_freelance/go_gigs/modules/auth/services"
	gigRequestServices "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/services"
	userServices "gitlab.com/fc_freelance/go_gigs/modules/users/services"
)

// gigRequestController: Controller private struct
type gigRequestController struct{}

// GigRequestController: Controller global variable (sort of singleton)
var GigRequestController = gigRequestController{}

/*
All: Gets all gigRequests
 GET /gigRequests
*/
func (ct *gigRequestController) All(c *gin.Context) {

	// Call service and throw error if any
	httpErr, gigRequests := gigRequestServices.GigRequestService.All()
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Return response with gigRequests list
	c.JSON(http.StatusOK, gigRequests)
}

/*
Get: Gets a gigRequest
 GET /gigRequests/:id
*/
func (ct *gigRequestController) Get(c *gin.Context) {

	// Get id from parameters
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Missing gigRequest id",
		})
		return
	}

	// Call service and throw error if any
	httpErr, gigRequest := gigRequestServices.GigRequestService.Get(int(id))
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Return response with gigRequests list
	c.JSON(http.StatusOK, gigRequest)
}

/*
Create: Creates a new gigRequest
 POST /gigRequests
*/
func (ct *gigRequestController) Create(c *gin.Context) {

	// Declare gigRequest model
	var gigRequest gigRequestModels.GigRequest

	// Error binding request data to model, 400
	if err := c.BindJSON(&gigRequest); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Bad form data",
		})
		return
	}

	// Call service for session and return error if any
	httpErr, session := authServices.AuthService.GetSession(c)
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Get user (expert) for gigRequest
	err, user := userServices.UserService.Get(int(session.UserId), false)
	if err != nil || user == nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Could not get user info",
		})
		return
	}

	// Assign user to gigRequest
	gigRequest.UserID = session.UserId

	// Call service and throw error if any
	httpErr, createdgigRequest := gigRequestServices.GigRequestService.Create(gigRequest)
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Send response with added gigRequest
	c.JSON(http.StatusCreated, createdgigRequest)
}

/*
Update: Updates a gigRequest
 PUT /gigRequests/:id
*/
func (ct *gigRequestController) Update(c *gin.Context) {

	// Get id from parameters
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Missing gigRequest id",
		})
		return
	}

	// Call service to get gigRequest and throw error if any
	err1, gigRequestRead := gigRequestServices.GigRequestService.Get(int(id))
	if err1 != nil {
		c.JSON(err1.StatusCode, gin.H{
			"error": err1.Message,
		})
		return
	}

	// Check it is same user or an admin one
	err2, usr := authServices.AuthService.GetSessionUser(c)
	if err2 != nil || (usr.ID != gigRequestRead.UserID && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error": "Unauthorized",
		})
		return
	}

	// Declare gigRequest model
	var gigRequest gigRequestModels.GigRequest

	// Error binding request data to model, 400
	if err := c.BindJSON(&gigRequest); err != nil {
		log.Println(err, gigRequest)
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Bad form data",
		})
		return
	}

	// Call service and throw error if any
	httpErr, updatedgigRequest := gigRequestServices.GigRequestService.Update(int(id), gigRequest)
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Send response with modified gigRequest
	c.JSON(http.StatusOK, updatedgigRequest)
}

/*
Delete: Deletes a gigRequest
 DELETE /gigRequests/:id
*/
func (ct *gigRequestController) Delete(c *gin.Context) {

	// Get id from parameters
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Missing gigRequest id",
		})
		return
	}

	// Call service to get gigRequest and throw error if any
	err1, gigRequestRead := gigRequestServices.GigRequestService.Get(int(id))
	if err1 != nil {
		c.JSON(err1.StatusCode, gin.H{
			"error": err1.Message,
		})
		return
	}

	// Check it is same user or an admin one
	err2, usr := authServices.AuthService.GetSessionUser(c)
	if err2 != nil || (usr.ID != gigRequestRead.UserID && !userServices.UserService.HasPermissions(*usr, []string{authModels.Permissions.ManageSystem})) {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error": "Unauthorized",
		})
		return
	}

	// Call service and throw error if any
	httpErr, gigRequest := gigRequestServices.GigRequestService.Delete(int(id))
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, gin.H{
			"error": httpErr.Message,
		})
		return
	}

	// Return response with deleted model
	c.JSON(http.StatusOK, gigRequest)
}
