/*
Package services: Gig Requests module services
*/
package services

import (
	"net/http"

	httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
	commonServices "gitlab.com/fc_freelance/go_gigs/modules/common/services"
	gigRequestModels "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/models"
	gigRequestRepositories "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/repositories"
)

// gigRequestService: Service private struct
type gigRequestService struct{}

// GigRequestService: Service global variable (sort of singleton)
var GigRequestService = gigRequestService{}

/*
All: Gets all gigRequests
*/
func (s *gigRequestService) All() (*httpModels.HttpError, *[]gigRequestModels.GigRequest) {

	// Return collection or error in repository
	return gigRequestRepositories.GigRequestRepository.All()
}

/*
Get: Gets a gigRequest
*/
func (s *gigRequestService) Get(id int) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Find model or error in repository
	return gigRequestRepositories.GigRequestRepository.Get(id)
}

/*
Find: Finds a gigRequest
*/
func (s *gigRequestService) Find(gigRequest gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Find model or error in repository
	return gigRequestRepositories.GigRequestRepository.Find(gigRequest)
}

/*
Create: Creates a gigRequest
*/
func (s *gigRequestService) Create(gigRequest gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Handle relations first
	if err, _ := s.handleRelations(&gigRequest); err != nil {
		return err, nil
	}

	// Create model or error in repository
	err, createdgigRequest := gigRequestRepositories.GigRequestRepository.Create(gigRequest)
	if err != nil {
		return err, nil
	}

	// Finally return created gigRequest from DB
	return s.Get(int(createdgigRequest.ID))
}

/*
Update: Updates a gigRequest
*/
func (s *gigRequestService) Update(id int, gigRequest gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Handle relations first
	if err, _ := s.handleRelations(&gigRequest); err != nil {
		return err, nil
	}

	// Update model or error in repository
	if err, _ := gigRequestRepositories.GigRequestRepository.Update(id, gigRequest); err != nil {
		return err, nil
	}

	// Finally return updated gigRequest from DB
	return s.Get(id)
}

/*
Delete: Deletes a gigRequest by id
*/
func (s *gigRequestService) Delete(id int) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Delete model or error in repository
	return gigRequestRepositories.GigRequestRepository.Delete(id)
}

/*
HandleRelations: Handles model relations (validate, assigns, etc.) and returns the modified? model
*/
func (s *gigRequestService) handleRelations(gigRequest *gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Ensure no category object
	gigRequest.Category = nil

	// Check proper category ID
	if gigRequest.CategoryID > 0 {

		// Get category data
		err, category := commonServices.CategoryService.Get(int(gigRequest.CategoryID))

		// If error, throw it
		if err != nil {
			return err, nil
		}

		// Properly retrieved, set it to model
		gigRequest.Category = category

		// No proper category id, 400
	} else {
		return &httpModels.HttpError{
			Message:    "Invalid category ID",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// Ensure no currency object
	gigRequest.Currency = nil

	// Check proper currency ID
	if gigRequest.CurrencyID > 0 {

		// Get currency data
		err, currency := commonServices.CurrencyService.Get(int(gigRequest.CurrencyID))

		// If error, throw it
		if err != nil {
			return err, nil
		}

		// Properly retrieved, set it to model
		gigRequest.Currency = currency

		// No proper currency id, 400
	} else {
		return &httpModels.HttpError{
			Message:    "Invalid currency ID",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// Ensure no location object
	gigRequest.Location = nil

	// Check proper location ID
	if gigRequest.LocationID > 0 {

		// Get location data
		err, location := commonServices.LocationService.Get(int(gigRequest.LocationID))

		// If error, throw it
		if err != nil {
			return err, nil
		}

		// Properly retrieved, set it to model
		gigRequest.Location = location

		// No proper location id, 400
	} else {
		return &httpModels.HttpError{
			Message:    "Invalid location ID",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// Return success with modified model
	return nil, gigRequest
}
