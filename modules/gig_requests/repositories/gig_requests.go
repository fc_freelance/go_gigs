/*
Package repositories: Gig Requests module repositories
*/
package repositories

import (
	"log"
	"net/http"

	"gitlab.com/fc_freelance/go_gigs/db"
	httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
	gigRequestModels "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/models"
)

// gigRequestRepository: Repository private struct
type gigRequestRepository struct{}

// GigRequestRepository: Repository global variable (sort of singleton)
var GigRequestRepository = gigRequestRepository{}

/*
All: Gets all gigRequests
*/
func (r *gigRequestRepository) All() (*httpModels.HttpError, *[]gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Not found",
		StatusCode: http.StatusNotFound,
	}

	// Define holder for collection
	gigRequests := []gigRequestModels.GigRequest{}

	// Run query and handle error
	err := db.DB.
		Preload("gigRequestResources").
		Preload("Category.Parent").
		Preload("Currency").
		Preload("Location.Parent").
		Find(&gigRequests).Error
	if err != nil {
		log.Println(err)
		return &defaultError, nil
	}

	// Return collection
	return nil, &gigRequests
}

/*
Get: Gets a gigRequest
*/
func (r *gigRequestRepository) Get(id int) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Not found",
		StatusCode: http.StatusNotFound,
	}

	// Define holder for collection
	gigRequest := gigRequestModels.GigRequest{}

	// Run query and handle error
	err := db.DB.
		Preload("gigRequestResources").
		Preload("Category.Parent").
		Preload("Currency").
		Preload("Location.Parent").
		Where("id = ?", id).
		Find(&gigRequest).Error
	if err != nil {
		log.Println(err)
		return &defaultError, nil
	}

	// Return model
	return nil, &gigRequest
}

/*
Find: Finds a gigRequest
*/
func (r *gigRequestRepository) Find(gg gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Not found",
		StatusCode: http.StatusNotFound,
	}

	// Hold gigRequest
	var gigRequest gigRequestModels.GigRequest

	// Run query and handle error
	err := db.DB.
		Preload("gigRequestResources").
		Preload("Category.Parent").
		Preload("Currency").
		Preload("Location.Parent").
		Where(&gg).
		First(&gigRequest).Error
	if err != nil {
		log.Println(err)
		return &defaultError, nil
	}

	// Return collection
	return nil, &gigRequest
}

/*
Create: Creates a gigRequest
*/
func (r *gigRequestRepository) Create(gigRequest gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Could not create gigRequest",
		StatusCode: http.StatusInternalServerError,
	}

	// Check we don't have any id
	isNew := db.DB.NewRecord(gigRequest)
	if !isNew {
		return &httpModels.HttpError{
			Message:    "gigRequest already exists",
			StatusCode: http.StatusBadRequest,
		}, nil
	}

	// Create user in DB, handle error
	err2 := db.DB.Create(&gigRequest).Error
	if err2 != nil {
		log.Println(err2)
		return &defaultError, nil
	}

	// Return created model
	return nil, &gigRequest
}

/*
Update: Updates a gigRequest
*/
func (r *gigRequestRepository) Update(id int, gigRequest gigRequestModels.GigRequest) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Could not update gigRequest",
		StatusCode: http.StatusInternalServerError,
	}

	// Hold fetched gigRequest
	var gg gigRequestModels.GigRequest

	// Update gigRequest in DB, handle error
	err := db.DB.
		Preload("gigRequestResources").
		Preload("Category.Parent").
		Preload("Currency").
		Preload("Location.Parent").
		First(&gg, id).
		Updates(gigRequest).Error
	if err != nil {
		log.Println(err)
		return &defaultError, nil
	}

	// Return data
	return nil, &gg
}

/*
Delete: Deletes a gigRequest
*/
func (r *gigRequestRepository) Delete(id int) (*httpModels.HttpError, *gigRequestModels.GigRequest) {

	// Hold general error here for reusing it ...
	defaultError := httpModels.HttpError{
		Message:    "Could not delete gigRequest",
		StatusCode: http.StatusInternalServerError,
	}

	// Hold gigRequest
	var gigRequest gigRequestModels.GigRequest

	// Delete gigRequest from DB, handle error
	err := db.DB.
		Preload("gigRequestResources").
		Preload("Category.Parent").
		Preload("Currency").
		Preload("Location.Parent").
		First(&gigRequest, id).
		Delete(&gigRequest).Error
	if err != nil {
		log.Println(err)
		return &defaultError, nil
	}

	// Return data
	return nil, &gigRequest
}
