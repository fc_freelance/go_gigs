/*
Package models - Gig requests related models
*/
package models

import (
	dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
	commonModels "gitlab.com/fc_freelance/go_gigs/modules/common/models"
	userModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

/*
Gig: Public Gig Request model struct
*/
type GigRequest struct {
	dbModels.DbBaseModel
	Name                string                  `form:"name" json:"name" binding:"required"`
	Description         string                  `form:"description" json:"description" binding:"required"`
	Date                string                  `form:"date" json:"date,omitempty"`
	Time                *string                 `form:"time" json:"time,omitempty"`
	Budget              float64                 `form:"price" json:"price" binding:"required"`
	UserID              uint                    `json:"user_id"`
	User                *userModels.User        `json:"-"`
	CurrencyID          uint                    `form:"currency_id" json:"currency_id" binding:"required"`
	Currency            *commonModels.Currency  `json:"currency,omitempty"`
	CategoryID          uint                    `form:"category_id" json:"category_id" binding:"required"`
	Category            *commonModels.Category  `json:"category,omitempty"`
	LocationID          uint                    `form:"location_id" json:"location_id" binding:"required"`
	Location            *commonModels.Location  `json:"location,omitempty"`
	GigRequestResources []commonModels.Resource `json:"gig_request_resources,omitempty" gorm:"many2many:gig_request_resources"`
}
