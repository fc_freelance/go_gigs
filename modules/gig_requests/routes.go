/*
Package gig_requests - Manages everything related to Gig Requests
*/
package gig_requests

import (
	httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
	gigRequestControllers "gitlab.com/fc_freelance/go_gigs/modules/gig_requests/controllers"
)

/*
Routes Global routes variable
*/
var Routes = []httpModels.Route{

	/******************************************************************************************/
	/*************************************** Gig Requests *************************************/
	/******************************************************************************************/

	/**
	 * @apidefine GigRequestCreateRequestBody
	 *
	 * @apiParam (Request body) {String} name Gig Request name.
	 * @apiParam (Request body) {String} description Gig Request description.
	 * @apiParam (Request body) {String} date Day service needs to be delivered on, must be in date format like 2016-08-01.
	 * @apiParam (Request body) {String} [time] Time service needs to be delivered on. Must be formatted like 18:00
	 * @apiParam (Request body) {Number} budget Gig Request budget.
	 * @apiParam (Request body) {Number} category_id The id of the category. It should be taken from existing ones.
	 * @apiParam (Request body) {Number} currency_id The id of the currency. It should be taken from existing ones.
	 * @apiParam (Request body) {Number} location_id The id of the location. It should be taken from existing ones.
	 * @apiParam (Request body) {Object[]} [gig_request_resources] Media resources associated to Gig Request (videos, images, etc.)
	 * @apiParam (Request body) {String="image","video","audio"} gig_request_resources.type Type of resource (video, audio, etc.).
	 * @apiParam (Request body) {String} [gig_request_resources.name] Name for the resource.
	 * @apiParam (Request body) {String} gig_request_resources.url Url where resource is located.

	 *
	 * @apiParamExample {json} Request body example
	 * {
	 *     "name": "Some Gig Request",
	 *     "description": "My fancy Gig Request",
	 *     "date": "2016-08-03",
	 *     "time": "16:30",
	 *     "budget": 40,
	 *     "category_id": 101,
	 *     "currency_id": 7,
	 *     "location_id": 123
	 *     "gig_request_resources": [
	 *         {
	 *             "type": "video",
	 *             "name": "my request video",
	 *             "url": "http://myvideourl.ftw"
	 *         }
	 *     ]
	 * }
	 */

	/**
	 * @apidefine GigRequestUpdateRequestBody
	 *
	 * @apiParam (Request body) {String} name Gig Request name.
	 * @apiParam (Request body) {String} description Gig Request description.
	 * @apiParam (Request body) {String} date Day service needs to be delivered on, must be in date format like 2016-08-01.
	 * @apiParam (Request body) {String} [time] Time service needs to be delivered on. Must be formatted like 18:00
	 * @apiParam (Request body) {Number} budget Gig Request budget.
	 * @apiParam (Request body) {Number} category_id The id of the category. It should be taken from existing ones.
	 * @apiParam (Request body) {Number} currency_id The id of the currency. It should be taken from existing ones.
	 * @apiParam (Request body) {Number} location_id The id of the location. It should be taken from existing ones.
	 * @apiParam (Request body) {Object[]} [gig_request_resources] Media resources associated to Gig Request (videos, images, etc.)
	 * @apiParam (Request body) {Number} [gig_request_resources.id] Existing resource id for updating it. If not provided, a new resource will be created.
	 * @apiParam (Request body) {String="image","video","audio"} gig_request_resources.type Type of resource (video, audio, etc.).
	 * @apiParam (Request body) {String} [gig_request_resources.name] Name for the resource.
	 * @apiParam (Request body) {String} gig_request_resources.url Url where resource is located.
	 *
	 * @apiParamExample {json} Success response payload example
	 * {
	 *     "name": "The same Gig Request",
	 *     "description": "My fancy Gig Request reloaded",
	 *     "date": "2016-08-04",
	 *     "time": "17:30",
	 *     "budget": 50,
	 *     "category_id": 101,
	 *     "currency_id": 7,
	 *     "location_id": 122
	 *     "gig_request_resources": [
	 *         {
	 *             "id": 1,
	 *             "type": "video",
	 *             "name": "my request video modified",
	 *             "url": "http://myvideourl2.ftw"
	 *         }
	 *     ]
	 * }
	 */

	/**
	 * @apidefine GigRequestPayload
	 *
	 * @apiSuccess (Success response payload) {Number} id Gig Request id.
	 * @apiSuccess (Success response payload) {String} name Gig Request name.
	 * @apiSuccess (Success response payload) {String} description Gig Request description.
	 * @apiSuccess (Success response payload) {String} date Day service needs to be delivered on.
	 * @apiSuccess (Success response payload) {String} time Time service needs to be delivered on.
	 * @apiSuccess (Success response payload) {Number} budget Gig Request budget.
	 * @apiSuccess (Success response payload) {Number} user_id User id this Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} category Category Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} currency Currency Gig Request uses.
	 * @apiSuccess (Success response payload) {Object} location Location Gig Request is for.
	 * @apiSuccess (Success response payload) {Object[]} gig_request_resources Media resources associated to Gig Request (videos, images, etc.)
	 *
	 * @apiSuccessExample {json} Success response example
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "id": 1,
	 *       "name": "The same Gig Request",
	 *       "description": "My fancy Gig Request reloaded",
	 *       "date": "2016-08-04",
	 *       "time": "17:30",
	 *       "budget": 50,
	 *       "user_id": 2,
	 *       "category": {
	 *           "id": 101,
	 *           "name": "Garden",
	 *           "description": "Garden cleaning related services",
	 *           "parent": {
	 *               "id": 1,
	 *               "name": "Cleaning",
	 *               "description": "Cleaning related services"
	 *           }
	 *       },
	 *       "currency": {
	 *           "id": 7,
	 *           "code": "DL",
	 *           "symbol": "$",
	 *           "conversion": 1.000
	 *       },
	 *       "location": {
	 *           "id": 2,
	 *           "name": "Berlin",
	 *           "code": "DE-BE-01",
	 *           "lat": 52.5244,
	 *           "lon": 13.4105,
	 *           "parent": {
	 *               "id": 1,
	 *               "name": "Deutschland",
	 *               "code": "DE",
	 *               "lat": 51.5,
	 *               "lon": 10.5
	 *           }
	 *       },
	 *       "gigRequest_resources": [
	 *           {
	 *               "id": 1,
	 *               "type": "video",
	 *               "name": "my video",
	 *               "url": "http://myvideourl2.ftw"
	 *           }
	 *       ]
	 *     }
	 */

	/**
	 * @apidefine GigRequestCreatePayload
	 *
	 * @apiSuccess (Success response payload) {Number} id Gig Request id.
	 * @apiSuccess (Success response payload) {String} name Gig Request name.
	 * @apiSuccess (Success response payload) {String} description Gig Request description.
	 * @apiSuccess (Success response payload) {String} date Day service needs to be delivered on.
	 * @apiSuccess (Success response payload) {String} time Time service needs to be delivered on.
	 * @apiSuccess (Success response payload) {Number} budget Gig Request budget.
	 * @apiSuccess (Success response payload) {Number} user_id User id this Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} category Category Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} currency Currency Gig Request uses.
	 * @apiSuccess (Success response payload) {Object} location Location Gig Request is for.
	 * @apiSuccess (Success response payload) {Object[]} gig_request_resources Media resources associated to Gig Request (videos, images, etc.)
	 *
	 * @apiSuccessExample {json} Success response example
	 *     HTTP/1.1 201 OK
	 *     {
	 *       "id": 1,
	 *       "name": "The same Gig Request",
	 *       "description": "My fancy Gig Request reloaded",
	 *       "date": "2016-08-04",
	 *       "time": "17:30",
	 *       "budget": 50,
	 *       "user_id": 2,
	 *       "category": {
	 *           "id": 101,
	 *           "name": "Garden",
	 *           "description": "Garden cleaning related services",
	 *           "parent": {
	 *               "id": 1,
	 *               "name": "Cleaning",
	 *               "description": "Cleaning related services"
	 *           }
	 *       },
	 *       "currency": {
	 *           "id": 7,
	 *           "code": "DL",
	 *           "symbol": "$",
	 *           "conversion": 1.000
	 *       },
	 *       "location": {
	 *           "id": 2,
	 *           "name": "Berlin",
	 *           "code": "DE-BE-01",
	 *           "lat": 52.5244,
	 *           "lon": 13.4105,
	 *           "parent": {
	 *               "id": 1,
	 *               "name": "Deutschland",
	 *               "code": "DE",
	 *               "lat": 51.5,
	 *               "lon": 10.5
	 *           }
	 *       },
	 *       "gigRequest_resources": [
	 *           {
	 *               "id": 1,
	 *               "type": "video",
	 *               "name": "my video",
	 *               "url": "http://myvideourl2.ftw"
	 *           }
	 *       ]
	 *     }
	 */

	/**
	 * @apidefine GigRequestsPayload
	 *
	 * @apiSuccess (Success response payload) {Object[]} _ List of Gig Request objects.
	 * @apiSuccess (Success response payload) {Number} _.id gigRequest id.
	 * @apiSuccess (Success response payload) {String} _.name Gig Request name.
	 * @apiSuccess (Success response payload) {String} _.description Gig Request description.
	 * @apiSuccess (Success response payload) {String} _.date Day service needs to be delivered on.
	 * @apiSuccess (Success response payload) {String} _.time Time service needs to be delivered on.
	 * @apiSuccess (Success response payload) {Number} _.budget Gig Request budget.
	 * @apiSuccess (Success response payload) {Number} _.user_id User id this Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} _.category Category Gig Request belongs to.
	 * @apiSuccess (Success response payload) {Object} _.currency Currency Gig Request uses.
	 * @apiSuccess (Success response payload) {Object} _.location Location Gig Request is for.
	 * @apiSuccess (Success response payload) {Object[]} _.gig_request_resources Media resources associated to Gig Request (videos, images, etc.)
	 *
	 * @apiSuccessExample {json} Success response example
	 *     HTTP/1.1 200 OK
	 *     [
	 *       {
	 *          "id": 1,
	 *          "name": "The same Gig Request",
	 *          "description": "My fancy Gig Request reloaded",
	 *          "date": "2016-08-04",
	 *          "time": "17:30",
	 *          "budget": 50,
	 *          "user_id": 2,
	 *          "category": {
	 *              "id": 101,
	 *              "name": "Garden",
	 *              "description": "Garden cleaning related services",
	 *              "parent": {
	 *                  "id": 1,
	 *                  "name": "Cleaning",
	 *                  "description": "Cleaning related services"
	 *              }
	 *          },
	 *          "currency": {
	 *              "id": 7,
	 *              "code": "DL",
	 *              "symbol": "$",
	 *              "conversion": 1.000
	 *          },
	 *          "location": {
	 *              "id": 2,
	 *              "name": "Berlin",
	 *              "code": "DE-BE-01",
	 *              "lat": 52.5244,
	 *              "lon": 13.4105,
	 *              "parent": {
	 *                  "id": 1,
	 *                  "name": "Deutschland",
	 *                  "code": "DE",
	 *                  "lat": 51.5,
	 *                  "lon": 10.5
	 *              }
	 *          },
	 *          "gigRequest_resources": [
	 *              {
	 *                  "id": 1,
	 *                  "type": "video",
	 *                  "name": "my video",
	 *                  "url": "http://myvideourl2.ftw"
	 *              }
	 *          ]
	 *       },
	 *       ...
	 *     ]
	 */

	/**
	 * @api {get} /gig_requests Get All Gig Requests
	 * @apiName GetGigRequests
	 * @apiGroup GigRequests
	 * @apiVersion 1.0.0
	 * @apiPermission UserAuth
	 *
     * @apiUse ApiTokenHeader
     *
	 * @apiUse GigRequestsPayload
	 *
	 * @apiUse UnauthorizedError
	 * @apiUse InternalServerError
	 */
	httpModels.Route{
		Method:      "GET",
		Path:        "/gig_requests",
		Permissions: nil,
		Handler:     gigRequestControllers.GigRequestController.All,
	},

	/**
	 * @api {get} /gig_requests/:id Get Gig Request
	 * @apiName GetGigRequest
	 * @apiGroup GigRequests
	 * @apiVersion 1.0.0
	 * @apiPermission UserAuth
	 *
     * @apiUse ApiTokenHeader
     *
	 * @apiParam (Parameters) {Number} id Gig Request unique id.
	 *
	 * @apiUse GigRequestPayload
	 *
	 * @apiUse BadRequestError
	 * @apiUse UnauthorizedError
	 * @apiUse NotFoundError
	 * @apiUse InternalServerError
	 */
	httpModels.Route{
		Method:      "GET",
		Path:        "/gig_requests/:id",
		Permissions: nil,
		Handler:     gigRequestControllers.GigRequestController.Get,
	},

	/**
	 * @api {post} /gig_requests Create Gig Request
	 * @apiName PostGigRequest
	 * @apiGroup GigRequests
	 * @apiVersion 1.0.0
	 * @apiPermission UserAuth
	 *
	 * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
	 * @apiUse GigRequestCreateRequestBody
	 *
	 * @apiUse GigRequestCreatePayload
	 *
	 * @apiUse BadRequestError
	 * @apiUse InternalServerError
	 */
	httpModels.Route{
		Method:      "POST",
		Path:        "/gig_requests",
		Permissions: nil,
		Handler:     gigRequestControllers.GigRequestController.Create,
	},

	/**
	 * @api {put} /gig_requests/:id Update Gig Request
	 * @apiName PutGigRequest
	 * @apiGroup GigRequests
	 * @apiVersion 1.0.0
	 * @apiPermission UserAuth
	 *
	 * @apiUse JsonHeader
     * @apiUse ApiTokenHeader
     *
	 * @apiParam (Parameters) {Number} id Gig Request unique id.
	 *
	 * @apiUse GigRequestUpdateRequestBody
	 *
	 * @apiUse GigRequestPayload
	 *
	 * @apiUse BadRequestError
	 * @apiUse UnauthorizedError
	 * @apiUse NotFoundError
	 * @apiUse InternalServerError
	 */
	httpModels.Route{
		Method:      "PUT",
		Path:        "/gig_requests/:id",
		Permissions: nil,
		Handler:     gigRequestControllers.GigRequestController.Update,
	},

	/**
	 * @api {delete} /gig_requests/:id Delete Gig Request
	 * @apiName DeleteGigRequest
	 * @apiGroup GigRequests
	 * @apiVersion 1.0.0
	 * @apiPermission UserAuth
	 *
     * @apiUse ApiTokenHeader
     *
	 * @apiParam (Parameters) {Number} id Gig Request unique id.
	 *
	 * @apiUse GigRequestPayload
	 *
	 * @apiUse BadRequestError
	 * @apiUse UnauthorizedError
	 * @apiUse NotFoundError
	 * @apiUse InternalServerError
	 */
	httpModels.Route{
		Method:      "DELETE",
		Path:        "/gig_requests/:id",
		Permissions: nil,
		Handler:     gigRequestControllers.GigRequestController.Delete,
	},
}
