/*
Package config - GoGigs API configuration package
 */
package config

import (
    "os"
    "log"

    "github.com/jinzhu/configor"

    configModels "gitlab.com/fc_freelance/go_gigs/config/models"
)

/*
Config: Go Gigs API configuration global variable
 */
var Config configModels.Config

/*
init: Loads configuration and fills global variable
 */
func init() {

    if err := configor.Load(&Config, "conf/config.json"); err != nil {
        log.Panicf("Error parsing config file: %s", err.Error())
        os.Exit(1)
    }
}
