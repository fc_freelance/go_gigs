/*
Package models - GoGigs API configuration models package
 */
package models

import(
    dbModels "gitlab.com/fc_freelance/go_gigs/db/models"
    httpModels "gitlab.com/fc_freelance/go_gigs/http/models"
    utilModels "gitlab.com/fc_freelance/go_gigs/util/models"
    usersModels "gitlab.com/fc_freelance/go_gigs/modules/users/models"
)

/*
Config: Go Gigs API configuration struct
 */
type Config struct {
    Version string `json:"version" default:"0.0.0"`
    Http httpModels.Config `json:"http"`
    Db dbModels.Config `json:"db"`
    Smtp utilModels.MailerConfig `json:"smtp"`
    Crypto utilModels.CryptConfig `json:"crypto"`
    Options struct {
        Users usersModels.OptionsConfig `json:"users"`
    } `json:"options"`
    Assets utilModels.AssetsConfig `json:"assets"`
}
